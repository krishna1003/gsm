<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactWithGroup extends Model
{
    protected $table = "contact_with_group";
}
