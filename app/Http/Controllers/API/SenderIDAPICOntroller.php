<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\SenderID_Details;
use Illuminate\Http\Request;
use Str;
use Validator;

class SenderIDAPICOntroller extends Controller {
	/**
	 * Request
	 * @param UserID  [admin , user]
	 * ifnull default get all records
	 */
	public static function getSenderIDS($request, $userID = null) {
		// dd("Get all", $userID, $request->all());
		//check validation first here
		try {
			if ($userID == null) {
				$data = SenderID_Details::latest()->get();
			} else {
				$data = SenderID_Details::where('user_uuid', $userID)->latest()->get();
			}

		} catch (\Throwable $th) {
			//throw $th;
			return response()->json(['status' => 'error', 'data' => [], 'msg' => 'error occured API', 'code' => 201]);
		}

		return response()->json(['status' => 'success', 'data' => $data, 'msg' => 'get data successfully', 'code' => 200]);

	}

	/**
	 * Save and Update Sender ID
	 * @param
	 */
	public static function storeSenderIDs(Request $request) {

		// dd($request->all(), "oooo");

		if ($request->isMethod('post')) {

			// dd($request->all());
			$validator = Validator::make($request->all(), [
				'senderid' => 'required|alpha|max:6',
				'type' => 'required',
				'purpose' => 'required',
			]);

			if ($validator->fails()) {
				return response()->json(['status' => 'error', 'data' => [], 'msg' => $validator->errors(), 'code' => 201]);
			}

			$data = self::saveSenderIDS($request);

			return response()->json(['status' => 'success', 'data' => $data, 'msg' => 'apply for Sender ID Success', 'code' => 200]);
		} else {
			return response()->json(['status' => 'error', 'data' => [], 'msg' => 'invalid Method for this operation', 'code' => 201]);
		}

	}

	public static function UserstoreSenderIDs(Request $request) {

		// dd($request->all(), "oooo");

		if ($request->isMethod('post')) {

			//dd($request->all());
			$validator = Validator::make($request->all(), [
				'senderid' => 'required|alpha|max:6',

			]);

			if ($validator->fails()) {
				return response()->json(['status' => 'error', 'data' => [], 'msg' => $validator->errors(), 'code' => 201]);
			}

			$data = self::saveSenderIDS($request);

			return response()->json(['status' => 'success', 'data' => $data, 'msg' => 'apply for Sender ID Success', 'code' => 200]);
		} else {
			return response()->json(['status' => 'error', 'data' => [], 'msg' => 'invalid Method for this operation', 'code' => 201]);
		}

	}

	/**
	 * Remember part of storeSenderIDS Function
	 * @param $request object from storeSenderIDS function
	 * Table Record Add and Update
	 */
	public static function saveSenderIDS($request) {
		// echo "Sender Fun";
		// dd($request->all());

		$senderID = SenderID_Details::where('sender_uuid', ($request->sender_uuid ?? null))->first();

		if ($senderID == null) {
			$senderID = new SenderID_Details;
			$senderID->sender_uuid = Str::uuid();
		}

		$senderID->user_uuid = $request->user_id;
		$senderID->user_type = $request->user_type;
		$senderID->senderid = $request->senderid;
		$senderID->status = $request->status;
		$senderID->type = $request->type;
		$senderID->purpose = $request->purpose;
		// $senderID->approved_by        =   $request->user_type;

		$senderID->save();
	}

	/**
	 * get Single Record Sender ID
	 * @param sender_uuid
	 */
	public function editSenderID($uuid) {
		$validator = Validator::make($request->all(), [
			'senderid' => 'required|exists:senderid_details,sender_uuid',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => 'error', 'data' => [], 'msg' => $validator->errors(), 'code' => 201]);
		}

		if ($uuid) {
			$data = SenderID_Details::where('sender_uuid', $uuid)->first();
			// return redirect()->back()->with('succes', 'Removed Sender ID');
			return response()->json(['status' => 'success', 'data' => $data, 'msg' => 'apply for Sender ID Success', 'code' => 200]);
		} else {
			return response()->json(['status' => 'error', 'data' => [], 'msg' => 'invalid Method for this operation', 'code' => 201]);
		}
	}

	/**
	 * Remove Sender ID
	 * @param senderid
	 */
	public function removeSenderID($uuid = null) {
		$validator = Validator::make($request->all(), [
			'senderid' => 'required|exists:senderid_details,sender_uuid',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => 'error', 'data' => [], 'msg' => $validator->errors(), 'code' => 201]);
		}

		if ($uuid) {
			$senderID = SenderID_Details::where('sender_uuid', $uuid)->delete();
			return redirect()->back()->with('succes', 'Removed Sender ID');
		} else {
			return view('layouts.error-404');
		}
	}

}
