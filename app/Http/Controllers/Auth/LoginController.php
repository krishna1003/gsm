<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/dashboard';

    protected function redirectTo()
    {
       if(Auth::user()->roleid == "1")
       {
        if(Auth::user()->status == "1")
         {
             return 'dashboard';
         }else
         {
            Auth::logout();
            return 'login';
         }
        
        }else
        {
          if(Auth::user()->status == "1")
             {
                 return 'userpanel';
             }else
             {
                Auth::logout();
                return 'login';
             }  
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    

    protected function credentials(Request $request)
        {
            if(is_numeric($request->get('email'))){
              return ['contactno'=>$request->get('email'),'password'=>$request->get('password')];
            }
            elseif (filter_var($request->get('email'))) {
              return ['email' => $request->get('email'), 'password'=>$request->get('password')];
            }
              return $request->only($this->email(), 'password');
        }

    public function loggedOut()
    {
        //Session::flush();
        Auth::logout();
        return redirect('/login'); 
    }
}
