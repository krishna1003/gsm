<?php

namespace App\Http\Controllers;

use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class ChangePasswordController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		//return view('changePassword');

		$activeTab = "changepassword";
		return view('users.changePassword', compact('activeTab'));

	}

	public function userindex() {
		//return view('changePassword');

		$activeTab = "changepassword";
		$user = \Auth::user();

		//dd($activeTab);
		return view('users.userprofile', compact('activeTab', 'user'));

	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function store(Request $request) {
		$request->validate([
			'current_password' => ['required', new MatchOldPassword],
			'new_password' => ['required|min:6'],
			'new_confirm_password' => ['same:new_password'],
		]);

		User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

		// dd('Password change successfully.');

		return redirect()->route('changepassword')->with('success', 'password change successfully.');
	}

	public function userstore(Request $request) {

		// dd('Password change successfully.');
		/*$test =  $request->validate([
	            'current_password' => ['required', new MatchOldPassword],
	            'new_password' => ['required'],
	            'new_confirm_password' => ['same:new_password'],
*/
		$activeTab = "changepassword";

		$validator = Validator::make($request->all(), [

			'current_password' => ['required', new MatchOldPassword],
			'new_password' => ['required', 'min:6'],
			'new_confirm_password' => ['same:new_password'],

		]);

		if ($validator->fails()) {

			// return redirect($user->user_name . '/settings')->withErrors($validator)->withInput();
			return redirect()->action('ChangePasswordController@userindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();

		} else {

			User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
			$activeTab = "changepassword";

			return redirect()->action('ChangePasswordController@userindex')->with('activeTab', $activeTab)->with('success', 'password change successfully.');

		}

		/*User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
			        $activeTab = "changepassword";

		*/

	}
}