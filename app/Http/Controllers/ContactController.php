<?php

namespace App\Http\Controllers;

use App\ContactWithGroup;
use App\Groups;
use App\Models\Contact;
use App\Models\ContactGroup;
use Auth;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Str;
use Validator;

class ContactController extends Controller {
	/** Personal Contact **/

	public function showContact(Request $request) {

		if ($request->ajax()) {

			$data = Contact::latest()->get();

			return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function ($row) {

					$btn = '<a href="' . route('contact.create', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>
                                <a href="' . route('contact.remove', $row->id) . '" class="remove btn btn-danger btn-sm">Remove</a>
                                ';

					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
		}
		return view('contacts.show-contacts');
	}

	public function createContact(Request $request, $uuid = null) {

		if ($request->isMethod('get') && $uuid != null) {
			$contact = Contact::find($uuid);
			return view('contacts.create-contact')->with('contact', $contact);
		}

		return view('contacts.create-contact');
	}

	public function storeData(Request $request, $uuid = null) {

		if ($request->isMethod('post')) {

			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'email' => 'required|email',
				'contact' => 'required|min:10',
			]);

			if ($validator->fails()) {
				return redirect()->back()
					->withErrors($validator)
					->withInput();
			}

			$user = Auth::user();
			$user_type = 'user';

			if ($uuid != null) {
				$request->request->add(['id' => $uuid]);
			}

			$request->request->add(['user_type' => $user_type, 'user_id' => $user->id]);
			$this->saveContact($request);

			return \redirect()->back()->with('success', 'Save Contact Success');
		} else {
			return view('layouts.error-404');
		}
	}

	public function usercontactstoreData(Request $request, $uuid = null) {

		$activeTab = "address-book";

		if ($request->isMethod('post')) {

			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'contact' => 'required|min:10',
			]);

			if ($validator->fails()) {

				return redirect()->action('UserController@addressindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();
			}

			$user = Auth::user();
			$user_type = 'user';

			if ($uuid != null) {
				$request->request->add(['id' => $uuid]);
			}

			$request->request->add(['user_type' => $user_type, 'user_id' => $user->id, 'email' => ""]);
			$this->saveContact($request);

			return redirect()->action('UserController@addressindex')->with('activeTab', $activeTab)->with('success_single_contact_add', 'New Contact saved successfully');
		} else {
			//return view('layouts.error-404');
		}
	}

	public function usermultiplecontactstoreData(Request $request, $uuid = null) {

		$activeTab = "address-book";
		$tab = "multi";

		if ($request->isMethod('post')) {

			$validator = Validator::make($request->all(), [
				'textarea' => 'required',

			]);

			if ($validator->fails()) {

				return redirect()->route('addressindex', ['tab' => 'multi'])->with('activeTab', $activeTab)->with('tab', $tab)->withErrors($validator)->withInput();
			}

			$user = Auth::user();
			$user_type = 'user';

			if ($uuid != null) {
				$request->request->add(['id' => $uuid]);
			}

			$addmult = nl2br($_REQUEST['textarea']);
			$data1 = explode('<br />', $addmult);
			foreach ($data1 as $dataline) {

				$line = explode(',', $dataline);
				$name1 = $line[0];
				$name = str_replace(' ', '', $name1);
				$contactno1 = $line[1];
				$contactno = str_replace(' ', '', $contactno1);

				if (preg_match('/^\d{10}$/', $contactno)) // phone number is valid
				{
					$phoneNumber = '0' . $contactno;

					// your other code here
				} else // phone number is not valid
				{
					return redirect()->action('UserController@addressindex', ['tab' => 'multi'])->with('activeTab', $activeTab)->with('tab', $tab)->with('error_multiple_contact_add', 'Invalid Contact Number');
				}

				//dd($dataline);

				$contact = new Contact;
				$contact->id = Str::uuid();
				$contact->name = $name;
				$contact->contact = $contactno;
				$contact->user_id = $user->id;
				$contact->user_type = $user_type;
				$contact->save();
				if (!$contact->save()) {
					//Log::error("Product gallery image not uploaded.");
				}

			}

			return redirect()->action('UserController@addressindex', ['tab' => 'multi'])->with('activeTab', $activeTab)->with('success_multi_contact_add', 'New Contact saved successfully');
		} else {
			//return view('layouts.error-404');
		}
	}

	public function saveContact($request) {

		$contact = Contact::where('id', ($request->id ?? null))->first();

		if ($contact == null) {
			$contact = new Contact;
		}

		$contact->id = Str::uuid();

		$contact->name = $request->name;
		$contact->email = $request->email;
		$contact->contact = $request->contact;
		$contact->user_id = $request->user_id;
		$contact->user_type = $request->user_type;

		$contact->save();
	}

	public function usercontact_edit($id) {
		$activeTab = 'address-book';
		$tab = '';
		$smstypetab = "singlesmstab";
		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		$contact = Contact::find($id);

		$contact_update_id = $id;

		//dd($contact);
		return View('userpanel', compact('contact', 'activeTab', 'contacts', 'contact_update_id', 'tab', 'smstypetab', 'groups'));
	}

	public function usercontact_update(Request $request, $id) {
		try {
			$date = date('Y-m-d h:i:s');
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'contact' => 'required|min:10',
			]);

			if ($validator->fails()) {

				return redirect()->action('UserController@addressindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();
			} else {

				$contact = Contact::find($id);
				$contact->name = $request->input('name');
				$contact->contact = $request->input('contact');
				$contact->updated_at = $date;

				if ($contact->save()) {
					//Redirect success
					return redirect()->action('UserController@addressindex')->with('success_single_contact_add', 'Contact updated successfully.');
				}

				//Redirect error
				return redirect()->action('UserController@addressindex')->with('error_single_contact_add', 'Failed to update contact.');

			}

		} catch (Exception $e) {
			Log::error("Failed to update contact.");
		}
	}

	public function usercontact_delete($id) {
		$activeTab = 'address-book';

		// $contact = Contact::find($id)->delete();
		$contact = DB::table("contacts")->where('id', '=', $id)->delete();

		$group = DB::table("contact_with_group")->where('contactid', '=', $id)->delete();
		// dd($contact);
		// $contact->delete();
		return redirect()->route('addressindex')->with('success_single_contact_add', 'Contact Deleted Successfully.');
	}

	public function usergroup_delete($id) {
		$activeTab = 'address-book';

		// $contact = Contact::find($id)->delete();
		$contact = DB::table("groups")->where('id', '=', $id)->delete();

		$group = DB::table("contact_with_group")->where('groupid', '=', $id)->delete();
		// dd($contact);
		// $contact->delete();
		return redirect()->route('addressindex')->with('success_single_contact_add', 'Group Deleted Successfully.');
	}

	public function deleteContact(Request $request, $uuid) {
		Contact::find($uuid)->delete();
		return redirect()->back()->with('success', 'Contact Removed Success');
	}

	/**
	 * Contact Group
	 */
	public function showGroupContact(Request $request) {

		// $data   =   ContactGroup::with('userGroup')->orderBy('created_at', 'DESC')->get();

		// dd( $data );

		if ($request->ajax()) {

			$data = ContactGroup::with('userGroup')->orderBy('created_at', 'DESC')->get();

			return Datatables::of($data)
				->addIndexColumn()
				->addColumn('created_by', function ($row) {
					return $row->userGroup->name;
				})
				->addColumn('action', function ($row) {

					$btn = '<a href="' . route('contact.group.create', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>
                                <a href="' . route('contact.group.remove', $row->id) . '" class="remove btn btn-danger btn-sm">Remove</a>
                                ';

					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
		}
		return view('contacts.show-group-contacts');
	}

	public function createGroupContact(Request $request, $uuid = null) {

		$contactGroup = ContactGroup::where('id', $uuid ?? '')->first();

		if ($contactGroup != null) {
			if ($contactGroup == null) {
				return redirect()->route('error500');
			}
			return view('contacts.create-group-contact')->with('contactGroup', $contactGroup);
		}

		return view('contacts.create-group-contact');
	}

	public function groupStoreData(Request $request, $uuid = null) {

		$request->validate([-
			'name' => 'required',
		]);

		$contactGroup = ContactGroup::where('id', $uuid ?? '')->first();

		if ($uuid == null) {
			$contactGroup = new ContactGroup;
			$contactGroup->id = uuid();
		}

		$contactGroup->name = $request->name;
		$contactGroup->created_by = $request->user;
		$contactGroup->save();

		return redirect()->route('contact.group.show');

	}

	public function createusergroup(Request $request) {

		$user = Auth::user();
		$userid = $user->id;
		$actiontype = $request->grouptype;
		if ($actiontype == 'u') {
			$groupid = $request->g_group;

			ContactWithGroup::where('groupid', $groupid)->delete();
			$contactGroup = Groups::where('id', $groupid ?? '')->first();
			$date = date('Y-m-d h:i:s');

			$contactGroup->name = $request->groupname;
			$contactGroup->updated_at = $date;
			$contactGroup->save();

			$comp_proSplit = explode(",", $request->g_contactid);

			$cnt = count($comp_proSplit);

			for ($i = 0; $i < $cnt; $i++) {
				$ContactWithGroup = new ContactWithGroup;
				$ContactWithGroup->groupid = $groupid;
				$ContactWithGroup->contactid = $comp_proSplit[$i];
				$ContactWithGroup->user_id = $userid;
				$ContactWithGroup->created_at = $date;

				if (!$ContactWithGroup->save()) {
					Log::error("Contact not added in Group.");
				}
			}

			return redirect()->route('addressindex')->with('success_group_add', 'Group Updated successfully.');

		} else {
			$contactGroup = new Groups;
			$date = date('Y-m-d h:i:s');

			$contactGroup->name = $request->groupname;
			$contactGroup->userid = $userid;
			$contactGroup->created_at = $date;
			$contactGroup->save();
			$groupid = $contactGroup->id;

			$comp_proSplit = explode(",", $request->g_contactid);

			$cnt = count($comp_proSplit);

			for ($i = 0; $i < $cnt; $i++) {
				$ContactWithGroup = new ContactWithGroup;
				$ContactWithGroup->groupid = $groupid;
				$ContactWithGroup->contactid = $comp_proSplit[$i];
				$ContactWithGroup->user_id = $userid;
				$ContactWithGroup->created_at = $date;

				if (!$ContactWithGroup->save()) {
					Log::error("Contact not added in Group.");
				}
			}

			return redirect()->route('addressindex')->with('success_group_add', 'Group Created successfully.');

		}

	}

	public function EditGroupContact(Request $request) {

		$user = Auth::user();
		$userid = $user->id;

		$output = "";
		/*$contacts=DB::table('contacts')
			                  ->where('user_id','=',$userid)
			                  ->orwhere('name', $request->search)
			                  ->orwhere('contact', $request->search)
		*/

		$contacts = DB::select(DB::raw(" SELECT cw.*,c.contact,c.name,g.name as groupname FROM contact_with_group cw , contacts c , groups g WHERE cw.user_id = '$userid' AND cw.groupid = '$request->groupid' AND cw.contactid = c.id AND cw.groupid = g.id "));

		return json_encode($contacts);

	}

	public function deleteGroupContact(Request $request, $uuid = null) {
		if ($uuid == null) {
			return redirect()->route('error500');
		}

		ContactGroup::where('id', $uuid)->delete();

		return redirect()->route('contact.group.show');
	}

	public function smsgroupid(Request $request) {

		$user = Auth::user();
		$userid = $user->id;

		$output = "";
		$contacts = DB::select(DB::raw(" SELECT cwg.*,c.* FROM contact_with_group cwg , contacts c WHERE cwg.user_id = '$userid' AND  cwg.groupid = '$request->id' AND cwg.contactid = c.id "));

		// dd ($contacts);

		if ($contacts) {
			$delete_img = asset('front2/images/delete.svg');
			$output .= '<table class="table" id="t123">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Mobile Number</th>
                                                    <th>action</th>
                                                </tr>
                                            </thead>
                                            <tbody >';
			$prefix = $contactList = '';

			foreach ($contacts as $key => $contact) {

				$output .= '<tr>';
				$output .= '<td>' . $contact->name . '</td>';
				$output .= '<td>' . $contact->contact . '</td>';
				$output .= '<td class="cancelDeletegroupsmsContact" id="c_' . $contact->contact . '"><img src="' . $delete_img . '"></td>';

				$output .= '</tr>';
				$contactList .= $prefix . '' . $contact->contact . '';
				$prefix = ', ';

			}
			$output .= '<input type="hidden" name="contact" id="smsgroupcontact" value="' . $contactList . '" />';
			$output .= '</tbody> </table>';
			return Response($output);
		}

	}

	public function searchcontact(Request $request) {

		$user = Auth::user();
		$userid = $user->id;

		$output = "";
		/*$contacts=DB::table('contacts')
			              ->where('user_id','=',$userid)
			              ->orwhere('name', $request->search)
			              ->orwhere('contact', $request->search)
		*/

		$contacts = DB::select(DB::raw(" SELECT * FROM contacts WHERE user_id = '$userid' AND ( name LIKE '%$request->search%' or  contact LIKE '%$request->search%' )"));

		// dd ($contacts);

		if ($contacts) {

			foreach ($contacts as $key => $contact) {
				$contact_f_name = ucfirst($contact->name[0]);
				$contact_name = $contact->name;
				$contact_no = $contact->contact;
				$onclick = "selectContact('" . $contact_no . "','" . $contact_name . "','" . $contact->id . "')";
				$edit = route('user.contacts.edit', $contact->id);
				$delete = route('user.contacts.delete', $contact->id);
				$edit_img = asset('front2/images/edit.svg');
				$delete_img = asset('front2/images/delete.svg');
				$chk_img = asset('front2/images/iconmonstr-check-mark-16.svg');
				$output .= '<li class="media" onclick="' . $onclick . '" style="cursor: pointer;">
                                    <span class="mr-3 cfl_" id="cfl_' . $contact_no . '">' . $contact_f_name . '</span>
                                    <span class="mr-3 check_" id="check_' . $contact_no . '" style="display: none;"><img style="padding: 4px;"   src="' . $chk_img . '" /></span>
                                    <div class="media-body row no-gutter align-items-center">
                                        <div class="col-10">
                                            <h5 class="mt-0 mb-1">' . $contact_name . '</h5>
                                            ' . $contact_no . '
                                        </div>
                                        <div class="col-2 d-flex justify-content-between action-btn active">
                                            <a href="' . $edit . '"><img src="' . $edit_img . '" /></a>
                                            <a href="' . $delete . '"><img src="' . $delete_img . '" /></a>
                                        </div>
                                    </div>
                                </li>';
			}
			return Response($output);
		}

	}

	public function searchcontactgroup(Request $request) {

		$user = Auth::user();
		$userid = $user->id;

		$output = "";
		/*$contacts=DB::table('contacts')
			              ->where('user_id','=',$userid)
			              ->orwhere('name', $request->search)
			              ->orwhere('contact', $request->search)
		*/

		$contacts = DB::select(DB::raw(" SELECT * FROM groups WHERE userid = '$userid' AND  name LIKE '%$request->search%'  "));

		// dd ($contacts);

		if ($contacts) {

			foreach ($contacts as $key => $contact) {
				$contact_f_name = ucfirst($contact->name[0]);
				$contact_name = $contact->name;
				$edit = route('user.contacts.edit', $contact->id);
				$delete = route('user.groups.delete', $contact->id);
				$edit_img = asset('front2/images/edit.svg');
				$delete_img = asset('front2/images/delete.svg');
				$output .= '<li class="media">
                                    <span class="mr-3">' . $contact_f_name . '</span>
                                    <div class="media-body row no-gutter align-items-center">
                                        <div class="col-10">
                                            <h5 class="mt-0 mb-1">' . $contact_name . '</h5>
                                            9925498917
                                        </div>
                                        <div class="col-2 d-flex justify-content-between action-btn active">
                                            <a href="#" onclick="Editgroupcontact(' . $contact->id . ');"><img src="' . $edit_img . '" /></a>
                                            <a href="' . $delete . '"><img src="' . $delete_img . '" /></a>
                                        </div>
                                    </div>
                                </li>';
			}
			return Response($output);
		}

	}

}
