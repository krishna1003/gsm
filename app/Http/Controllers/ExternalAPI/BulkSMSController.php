<?php

namespace App\Http\Controllers\ExternalAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SMSDeliveries;
use Excel;
use Auth;
use App\User;
use App\Models\APIDetails;
use App\Models\Bulkfile;
use App\Models\ImportBulks; 
use Log;
use Str;

class BulkSMSController extends Controller
{

    /**
     * Render BULK SMS Request function
     *
     * @param Type $file
     * @param Name bulkFile
     * @return void
    */
    public static function renderBulkSms(Request $request)
    {
        // dd($request->all());
        try {

            $fileHeader = [];

            $fileName    =   request()->file('bulkFile')->getClientOriginalName();
            $import = new ImportBulks();
            \Excel::import($import, request()->file('bulkFile'));

            foreach($import->rowData as $key => $row) {
                if($key == 0) {
                    foreach($row as $title => $value) {
                        array_push($fileHeader, $title);
                    }
                }
            }

            // Upload File
            $file               =   $request->file('bulkFile');
            $newFileName        =   time().'-'.$file->getClientOriginalName();
            $destinationPath    =   public_path('bulk-files/');
            $file->move($destinationPath,$newFileName);

            $bulkFile   =   new Bulkfile;
            $bulkFile->bulkfile_uuid   =   Str::uuid();
            $bulkFile->user_uuid       =   $request->user_id;
            $bulkFile->filename        =   $newFileName;

            $bulkFile->save();

            $sheetData = array(
                'fileuuid'  => $bulkFile->bulkfile_uuid,
                'filename'  => $fileName,
                'header'    => $fileHeader,
                'body'      => $import->rowData
            );

            // dd( $fileHeader, $sheetData);

            if($sheetData) {
                Log::info('\n Render Files Data SUCCESS RESPONSE ');
                return response()->json(['status' => 'success', 'data' => $sheetData, 'msg' => 'Bulk render data success', 'code' => 200]);
            }else{
                Log::info('\n Render File has No Data Found RESPONSE ');
                return response()->json(['status' => 'success', 'data' => [], 'msg' => 'Bulk Rendering Data not found' , 'code' => 201]);
            }


        } catch (\Throwable $th) {
            $msg = "File SMS not Rendering Response Please contact admin";
            Log::info('\n '.$msg);

            return response()->json(['status' => 'error', 'data' => null, 'msg' => $msg, 'code' => 501]);
        }


    }

    /**
     * Send BULK SMS Request function
     *
     * @param Type $file
     * @param Name bulkFile
     * @return void
    */
    public static function sendBulkSMS(Request $request)
    {
        $bulkFileData = Bulkfile::where('bulkfile_uuid', $request->fileuuid)->first();

        $user = User::where('id', $bulkFileData['user_uuid'])->first();

        $ApiCredential  =   APIDetails::where('uuid', $user['gateway_uuid'])->first();

        $numbersArray = [];
        $msgArray = [];

        $requestParameters = new \stdClass;
        $requestParameters->type = 'bulk';
        $requestParameters->method  =   $ApiCredential['method'];
        $requestParameters->userid  =   $user['id'];
        $requestParameters->customer_uuid = $user['id'];
        $requestParameters->api_uuid = $ApiCredential['uuid'];
        $requestParameters->senderid = $request->senderid;
        $requestParameters->customer_id  = $ApiCredential['customer_id'];
        $requestParameters->campaign = $request->campaign;
        $requestParameters->filedata =  json_decode($request->filedata);
        $requestParameters->fileuuid = $request->fileuuid;


        $columnsNo  = collect($requestParameters->filedata[0])->count();

        //dd($columnsNo);



        if($columnsNo == 2) {
            self::saveBulkFile($requestParameters);

            foreach($requestParameters->filedata as $num) {
                array_push($numbersArray, $num->number);
            }

            $requestParameters->message =   $request->message;
            $requestParameters->contact =   $numbersArray;


            if($ApiCredential->method == "GET") {
                $apiUrl = $ApiCredential->api_url;

                $tags       =   arrayToString($ApiCredential->tags);
                $set_tag    =   arrayToString($ApiCredential->set_tags);

                $baseAPIUrl = str_replace($tags, $set_tag, $apiUrl);

                $newAPIUrl = str_replace(['#NUMBERS', '#MESSAGE'], [implode(',', $numbersArray), urlencode($request->message)], $baseAPIUrl);

                $requestParameters->url     =   $newAPIUrl;
                $response   =   sendGetSMS($requestParameters);

                // dd($request->all(), $requestParameters);
                if(($response->getData()->status == "success") && ($response->getData()->code == 200)) {
                    if(is_array($response->getData()->data)) {
                        $apiResponse    =   $response->getData()->data;
                    }elseif(is_string($response->getData()->data)) {
                        if(isJSON($response->getData()->data) == true) {
                            $apiResponse    =   json_decode($response->getData()->data);
                        }else{
                            $apiResponse    =   $response->getData()->data;
                        }
                    }

                    if($ApiCredential->response_rules == "PREFIX") {

                        $responseString =  checkString($apiResponse, $ApiCredential->success);

                        if($responseString == true) {
                            Log::info(' API FINAL SUCCESS RESPONSE :: '. $ApiCredential);
                            self::saveDelivery($requestParameters, 'send');
                        }elseif($responseString == false) {
                            Log::info(' API FINAL FAILED RESPONSE :: '. $ApiCredential);
                            self::saveDelivery($requestParameters, 'reject');
                        }else{
                            Log::info(' API FINAL ERROR Response Unknown not match in Our Database');
                            self::saveDelivery($requestParameters, 'reject');
                        }
                    }else {

                        $checkExist = property_exists($apiResponse, $ApiCredential->response_rules);

                        if($checkExist == true) {

                        $objVal = $apiResponse->{$ApiCredential->response_rules};

                            if($objVal == $ApiCredential->success) {
                                Log::info('\n API FINAL SUCCESS RESPONSE :: '. $ApiCredential->success);
                                self::saveDelivery($requestParameters, 'send');
                            }elseif($objVal == $ApiCredential->error) {
                                Log::info('\n API FINAL FAILED RESPONSE :: '. $ApiCredential->error);
                                self::saveDelivery($requestParameters, 'reject');
                            }else{
                                Log::info('\n API FINAL ERROR Response Unknown not match in Our Database');
                                self::saveDelivery($requestParameters, 'reject');
                            }

                        }else{
                            $msg = "API Rules Not Match Error Response Please contact admin";
                            Log::info('\n '.$msg);
                            self::saveDelivery($requestParameters, 'reject');
                        }
                    }
                }
            }elseif($ApiCredential->method == "POST") {
                // dd($request->all(), $requestParameters);
                    
                $requestParameters->url     =   $ApiCredential->api_url;
                $tags       =   arrayToString($ApiCredential->tags);
                $set_tag    =   arrayToString($ApiCredential->set_tags);

                $postUrl    =   "key=#APIKEY&routeid=#ROUTEID&type=#TYPE&contacts=#CONTACTS&senderid=#SENDERID&msg=#MESSAGE";
                $postFields = str_replace($tags, $set_tag, $postUrl);
                $postParameters = str_replace(['#CONTACTS', '#MESSAGE'], [implode(',', $numbersArray), urlencode($request->message)], $postFields);

                $requestParameters->postParameters     =   $postParameters;
                $response   =   sendPostSMS($requestParameters);

                if(($response->getData()->status == "success") && ($response->getData()->code == 200)) {
                    if(is_array($response->getData()->data)) {
                        $apiResponse    =   $response->getData()->data;
                    }elseif(is_string($response->getData()->data)) {
                        if(isJSON($response->getData()->data) == true) {
                            $apiResponse    =   json_decode($response->getData()->data);
                        }else{
                            $apiResponse    =   $response->getData()->data;
                        }
                    }

                    if($ApiCredential->response_rules == "PREFIX") {

                        $responseString =  checkString($apiResponse, $ApiCredential->success);

                        if($responseString == true) {
                            Log::info(' API FINAL SUCCESS RESPONSE :: '. $ApiCredential);
                            self::saveDelivery($requestParameters, 'send');
                        }elseif($responseString == false) {
                            Log::info(' API FINAL FAILED RESPONSE :: '. $ApiCredential);
                            self::saveDelivery($requestParameters, 'reject');
                        }else{
                            Log::info(' API FINAL ERROR Response Unknown not match in Our Database');
                            self::saveDelivery($requestParameters, 'reject');
                        }
                    }else {

                        $checkExist = property_exists($apiResponse, $ApiCredential->response_rules);

                        if($checkExist == true) {

                        $objVal = $apiResponse->{$ApiCredential->response_rules};

                            if($objVal == $ApiCredential->success) {
                                Log::info('\n API FINAL SUCCESS RESPONSE :: '. $ApiCredential->success);
                                self::saveDelivery($requestParameters, 'send');
                            }elseif($objVal == $ApiCredential->error) {
                                Log::info('\n API FINAL FAILED RESPONSE :: '. $ApiCredential->error);
                                self::saveDelivery($requestParameters, 'reject');
                            }else{
                                Log::info('\n API FINAL ERROR Response Unknown not match in Our Database');
                                self::saveDelivery($requestParameters, 'reject');
                            }

                        }else{
                            $msg = "API Rules Not Match Error Response Please contact admin";
                            Log::info('\n '.$msg);
                            self::saveDelivery($requestParameters, 'reject');
                        }
                    }
                }
            }

        }else if($columnsNo >= 3) {
            self::saveBulkFile($requestParameters);

            foreach($requestParameters->filedata as $key => $rowdata) {

                $requestParameters->contact = $rowdata->number;
                $requestParameters->message = $rowdata->message;

                if($ApiCredential->method == "GET") {
                    $apiUrl = $ApiCredential->api_url;

                    $tags       =   arrayToString($ApiCredential->tags);
                    $set_tag    =   arrayToString($ApiCredential->set_tags);

                    $baseAPIUrl = str_replace($tags, $set_tag, $apiUrl);

                    // dd( $bulkNumbers, $bulkMsg);
                    $newAPIUrl = str_replace(['#NUMBERS', '#MESSAGE'], [$rowdata->number, urlencode($rowdata->message)], $baseAPIUrl);
                    $requestParameters->url     =   $newAPIUrl;
                    $response   =   sendGetSMS($requestParameters);
                    // dd($request->all(), $requestParameters);

                    if(($response->getData()->status == "success") && ($response->getData()->code == 200)) {
                        if(is_array($response->getData()->data)) {
                            $apiResponse    =   $response->getData()->data;
                        }elseif(is_string($response->getData()->data)) {
                            if(isJSON($response->getData()->data) == true) {
                                $apiResponse    =   json_decode($response->getData()->data);
                            }else{
                                $apiResponse    =   $response->getData()->data;
                            }
                        }

                        if($ApiCredential->response_rules == "PREFIX") {

                            $responseString =  checkString($apiResponse, $ApiCredential->success);

                            if($responseString == true) {
                                Log::info(' API FINAL SUCCESS RESPONSE :: '. $ApiCredential);
                                self::saveDelivery($requestParameters, 'send');
                            }elseif($responseString == false) {
                                Log::info(' API FINAL FAILED RESPONSE :: '. $ApiCredential);
                                self::saveDelivery($requestParameters, 'reject');
                            }else{
                                Log::info(' API FINAL ERROR Response Unknown not match in Our Database');
                                self::saveDelivery($requestParameters, 'reject');
                            }
                        }else {

                            $checkExist = property_exists($apiResponse, $ApiCredential->response_rules);

                            if($checkExist == true) {

                            $objVal = $apiResponse->{$ApiCredential->response_rules};

                                if($objVal == $ApiCredential->success) {
                                    Log::info('\n API FINAL SUCCESS RESPONSE :: '. $ApiCredential->success);
                                    self::saveDelivery($requestParameters, 'send');
                                }elseif($objVal == $ApiCredential->error) {
                                    Log::info('\n API FINAL FAILED RESPONSE :: '. $ApiCredential->error);
                                    self::saveDelivery($requestParameters, 'reject');
                                }else{
                                    Log::info('\n API FINAL ERROR Response Unknown not match in Our Database');
                                    self::saveDelivery($requestParameters, 'reject');
                                }

                            }else{
                                $msg = "API Rules Not Match Error Response Please contact admin";
                                Log::info('\n '.$msg);
                                self::saveDelivery($requestParameters, 'reject');
                            }
                        }
                    }
                }else if($ApiCredential->method == "POST") {

                    $requestParameters->url     =   $ApiCredential->api_url;
                    $tags       =   arrayToString($ApiCredential->tags);
                    $set_tag    =   arrayToString($ApiCredential->set_tags);

                    $postUrl    =   "key=#APIKEY&routeid=#ROUTEID&type=#TYPE&contacts=#CONTACTS&senderid=#SENDERID&msg=#MESSAGE";
                    $postFields = str_replace($tags, $set_tag, $postUrl);
                    $postParameters = str_replace(['#CONTACTS', '#MESSAGE'], [implode(',', $numbersArray), urlencode($request->message)], $postFields);

                    $requestParameters->postParameters     =   $postParameters;
                    $response   =   sendPostSMS($requestParameters);

                    if(($response->getData()->status == "success") && ($response->getData()->code == 200)) {
                        if(is_array($response->getData()->data)) {
                            $apiResponse    =   $response->getData()->data;
                        }elseif(is_string($response->getData()->data)) {
                            if(isJSON($response->getData()->data) == true) {
                                $apiResponse    =   json_decode($response->getData()->data);
                            }else{
                                $apiResponse    =   $response->getData()->data;
                            }
                        }

                        if($ApiCredential->response_rules == "PREFIX") {

                            $responseString =  checkString($apiResponse, $ApiCredential->success);

                            if($responseString == true) {
                                Log::info(' API FINAL SUCCESS RESPONSE :: '. $ApiCredential);
                                self::saveDelivery($requestParameters, 'send');
                            }elseif($responseString == false) {
                                Log::info(' API FINAL FAILED RESPONSE :: '. $ApiCredential);
                                self::saveDelivery($requestParameters, 'reject');
                            }else{
                                Log::info(' API FINAL ERROR Response Unknown not match in Our Database');
                                self::saveDelivery($requestParameters, 'reject');
                            }
                        }else {

                            $checkExist = property_exists($apiResponse, $ApiCredential->response_rules);

                            if($checkExist == true) {

                            $objVal = $apiResponse->{$ApiCredential->response_rules};

                                if($objVal == $ApiCredential->success) {
                                    Log::info('\n API FINAL SUCCESS RESPONSE :: '. $ApiCredential->success);
                                    self::saveDelivery($requestParameters, 'send');
                                }elseif($objVal == $ApiCredential->error) {
                                    Log::info('\n API FINAL FAILED RESPONSE :: '. $ApiCredential->error);
                                    self::saveDelivery($requestParameters, 'reject');
                                }else{
                                    Log::info('\n API FINAL ERROR Response Unknown not match in Our Database');
                                    self::saveDelivery($requestParameters, 'reject');
                                }

                            }else{
                                $msg = "API Rules Not Match Error Response Please contact admin";
                                Log::info('\n '.$msg);
                                self::saveDelivery($requestParameters, 'reject');
                            }
                        }
                    }
                }
            }
        }


        return response()->json(['status' => 'success', 'data' => [], 'msg' => "send bulk sheet success", 'code' => 200]);

        // dd($request->all(), $requestParameters, $ApiCredential);

    }

    public static function saveBulkFile($request)
    {
        // dd($request->fileuuid);
        $data = [
            'company_uuid'    =>   $request->customer_id,
            'api_uuid'        =>   $request->api_uuid,
            'client_uuid'     =>   $request->userid
        ];

        $bulkFile = Bulkfile::where('bulkfile_uuid', $request->fileuuid)
                            ->update($data);
    }

    public static function saveDelivery($request, $status=null) {

        // dd($request);

        $smsDelivery = SMSDeliveries::where('deliveries_uuid', $request->deliveries_uuid ?? null)->first();

        if($smsDelivery == null) {
            $smsDelivery                    =    new SMSDeliveries;
            $smsDelivery->deliveries_uuid   =   Str::uuid();
        }

        $smsDelivery->user_uuid         =   $request->userid;
        $smsDelivery->company_uuid      =   $request->customer_uuid;
        $smsDelivery->api_uuid          =   $request->api_uuid;
        $smsDelivery->client_uuid       =   Auth::user()->id;

        $smsDelivery->number            =   (is_array($request->contact)) ? (implode(',', $request->contact)) : $request->contact;
        $smsDelivery->message           =   $request->message;

        if($status == 'waiting') {
            $smsDelivery->delivery_status   =   'waiting';
        }elseif($status == 'send') {
            $smsDelivery->delivery_status   =   'send';
        }elseif($status == 'reject'){
            $smsDelivery->delivery_status   =   'reject';
        }else{
            $smsDelivery->delivery_status   =   'waiting';
        }

        $smsDelivery->save();

        return $smsDelivery;

    }
}
