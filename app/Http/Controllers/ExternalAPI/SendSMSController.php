<?php

namespace App\Http\Controllers\ExternalAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SMSDelivery;
use Excel;
use App\Models\ImportBulks;
use Auth;
use DB;
use Validator;
use App\Models\Bulkfile;


class SendSMSController extends Controller
{
    /**
     * Send Single Number SMS function
     *
     * @param Request $request
     * @return void
     */
    public function sendSingleSMSForm()
    {
        return view('external-apis.sms.single-response');
    }

    /**
     * Send Single SMS Request function
     *
     * @return void
     */
    public function sendSingleSMSRequest(Request $request)
    {

        $request->validate([
            'contact'   =>  'required|max:11',
            'campaign'   =>  'required',
            'senderid'   =>  'required',
            'message'   =>  'required',
        ]);

        $gatewayResponse = SingleSMSController::testSMS($request);

        $response = $gatewayResponse->getData();

        if(($response->status == "success") && ($response->code == 200)) {
            return redirect()->back()->with('success', 'Send SMS success');
        }else{
            return redirect()->back()->with('error', 'failed Please contact your administrative ');
        }
    }

    public function sendUserSingleSMSRequest(Request $request)
    {

        

        $validator = Validator::make($request->all(), [
                'contact'   =>  'required|max:11',
            'campaign'   =>  'required',
            'senderid'   =>  'required',
            'message'   =>  'required',
            ]);

        $activeTab = "sms-send";

        if ($validator->fails()) {
                

              return redirect()->action('UserController@singlesmsindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();
            }

        $gatewayResponse = SingleSMSController::testSMS($request);

        $response = $gatewayResponse->getData();
        $smstypetab = "singlesmstab";



        if(($response->status == "success") && ($response->code == 200)) {
           // return redirect()->back()->with('smstypetab', $smstypetab)->with('success', 'Send SMS success');
            return  redirect()->action('UserController@singlesmsindex')->with('success_singlesmssend', 'Send SMS success');
        }else{
            //return redirect()->back()->with('smstypetab', $smstypetab)->with('error', 'failed Please contact your administrative ');

            return  redirect()->action('UserController@singlesmsindex')->with('error_singlesmssend', 'failed Please contact your administrative ');
        }
    }

    /**
     * Send Multi Number SMS Send function
     *
     * @param Type $var
     * @return void
     */
    public function sendMultiSMS()
    {
        return view('external-apis.sms.multi-response');
    }

    /**
     * Send Multiple Request SMS function
     *
     * @return void
     */
    public function SendMultiSMSRequest(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'contact'   =>  'required',
            'campaign'   =>  'required',
            'senderid'   =>  'required',
            'message'   =>  'required',
        ]);

        $gatewayResponse = SingleSMSController::testSMS($request);

        $response = $gatewayResponse->getData();

        if(($response->status == "success") && ($response->code == 200)) {
            return redirect()->back()->with('success', 'Send SMS success');
        }else{
            return redirect()->back()->with('error', 'failed Please contact your administrative ');
        }
    }

    public function SendUserMultiSMSRequest(Request $request)
    {
        // dd($request->all());
        
        $validator = Validator::make($request->all(), [
                'contact'   =>  'required',
                'campaign'   =>  'required',
                'senderid'   =>  'required',
                'message'   =>  'required',
            ]);

        $activeTab = "sms-send";
        $tab = "single";
        $smstypetab = "multismstab";
        $user = Auth::user();
        $userid = $user->id;
        $contacts = DB::table("contacts")->where('user_id','=',$userid)->get();

            if ($validator->fails()) {
                

              return redirect()->action('UserController@multismsindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();
            }

        $gatewayResponse = SingleSMSController::testSMS($request);

        $response = $gatewayResponse->getData();

        $smstypetab = "multismstab";


        //dd($contacts);
        //return view('userpanel',compact('activeTab','contacts','tab','smstypetab'));

        if(($response->status == "success") && ($response->code == 200)) {
          return  redirect()->action('UserController@multismsindex')->with('success_multismssend', 'Send SMS success');

          
        }else{
          return  redirect()->action('UserController@multismsindex')->with('error_multismssend', 'failed Please contact your administrative ');
        }
    }

    public function SendUserGroupSMSRequest(Request $request)
    {
        // dd($request->all());
        
        $validator = Validator::make($request->all(), [
                'contact'   =>  'required',
                'campaign'   =>  'required',
                'senderid'   =>  'required',
                'message'   =>  'required',
            ]);

        $activeTab = "sms-send";
        $tab = "single";
        $smstypetab = "groupsmstab";
        $user = Auth::user();
        $userid = $user->id;
        $contacts = DB::table("contacts")->where('user_id','=',$userid)->get();

            if ($validator->fails()) {
                

              return redirect()->action('UserController@groupsmsindex')->with('activeTab', $activeTab)->withErrors($validator)->withInput();
            }

        $gatewayResponse = SingleSMSController::testSMS($request);

        $response = $gatewayResponse->getData();

        $smstypetab = "groupsmstab";


        //dd($contacts);
        //return view('userpanel',compact('activeTab','contacts','tab','smstypetab'));

        if(($response->status == "success") && ($response->code == 200)) {
          return  redirect()->action('UserController@groupsmsindex')->with('success_groupsmssend', 'Send SMS success');

          
        }else{
          return  redirect()->action('UserController@groupsmsindex')->with('error_groupsmssend', 'failed Please contact your administrative ');
        }
    }

    /**
     * Send BULK SMS function
     *
     * @param Type $var
     * @return void
     */
    public function sendBulkSMS()
    {
        return view('external-apis.sms.bulk-response');
    }

    /**
     * Render BULK SMS Request function Preview
     *
     * @param Type $var
     * @return void
    */
    public function renderBulkSMSRequest(Request $request)
    {
        // dd($request->all());
        $rules = array('bulkFile' => 'required|mimes:xlx,xlsx,csv|max:2048');
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->errors()->all(), 'data' => null, 'code' => 201]);
        }

        $request->request->add(['user_id' => Auth::user()->id]);

        $bulkResponse    =   BulkSMSController::renderBulkSms($request);
        $response = $bulkResponse->getData();

        // dd($response);
        // $fileData = Bulkfile::where('bulkfile_uuid', $response->data->fileuuid)->first();

        // $path = asset('bulk-files/').$fileData->filename;

        $responseData = new \stdClass;
        $responseData->bulkdata     =   $response->data;

        $title      = "Render Uploaded Bulk Sheet Data";
        $fileuuid   = $response->data->fileuuid;
        $header     = $response->data->header;
        $body       = $response->data->body;
        $fileName   = $response->data->filename;
        // dd($fileName, $header, $body);

        $view = view("partials.data-table-reponse")
                    ->with('title', $title)
                    ->with('fileuuid', $fileuuid)
                    ->with('fileName', $fileName)
                    ->with('header', $header)
                    ->with('body', $body)
                    ->render();

        $responseData->htmlview     =   $view;

        if(($response->status == "success") && ($response->code == 200)) {
            return response()->json(['status' => 'success', 'msg' => 'File Data Rendering Successfully', 'data' => $responseData, 'code' => 200]);
        }else{
            return response()->json(['status' => 'error', 'msg' => 'invalid file Upload', 'data' => null, 'code' => 201]);
        }

    }

    /**
     * Send BULK SMS Request function
     *
     * @param Type $var
     * @return void
    */
    public function SendBulkSMSRequest(Request $request)
    {
        // dd("SMS SEND FINAL ", $request->all());

        $bulkResponse    =   BulkSMSController::sendBulkSMS($request);

        // dd($bulkResponse);
        $response = $bulkResponse->getData();

        if(($response->status == "success") && ($response->code == 200)) {
            return response()->json(['status' => 'success', 'msg' => 'File Data Rendering Successfully', 'data' => $response->data, 'code' => 200]);
        }else{
            return response()->json(['status' => 'error', 'msg' => 'invalid file Upload', 'data' => null, 'code' => 201]);
        }

    }

}
