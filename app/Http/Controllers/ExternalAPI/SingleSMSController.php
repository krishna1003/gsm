<?php

namespace App\Http\Controllers\ExternalAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Str;
use GuzzleHttp\Client;
use App\Models\SMSDeliveries;
use App\Models\APIDetails;
use Auth;
use Log;

class SingleSMSController extends Controller
{
    // public static function SendSms(Request $request)
    // {
    //     echo "Yes Send SMS";
    //     try {

    //     } catch (\Throwable $th) {

    //         return response()->json(['status' => 'fail', 'error' => $th, 'code' => 201]);
    //     }
    //     return response()->json(['status' => 'success', 'data' => null, 'code' => 200]);
    //     // dd($request->all());
    // }

    public static function testSMS(Request $request)
    {
        try {

            $ApiCredential  =   APIDetails::where('uuid', Auth::user()->gateway_uuid)->first();

                $requestParameters = new \stdClass;
                $requestParameters->api_uuid = $ApiCredential->uuid;
                $requestParameters->customer_uuid = $ApiCredential->customer_id;



                $requestParameters->contact = $request->contact; 
                $requestParameters->message = $request->message;
                $requestParameters->senderid = $request->senderid;
                $requestParameters->campaign = $request->campaign;

                $requestParameters->userid = Auth::user()->id;
                $requestParameters->type = 'single';
                $requestParameters->gateway = Auth::user()->gateway_uuid;
                $requestParameters->method  =   $ApiCredential->method;

                /**
                 * Set in delivery
                 */
                self::saveDelivery($requestParameters, 'waiting');

            if($ApiCredential->method == "GET") {

                $apiUrl = $ApiCredential->api_url;

                $tags       =   arrayToString($ApiCredential->tags);
                $set_tag    =   arrayToString($ApiCredential->set_tags);

                $baseAPIUrl = str_replace($tags, $set_tag, $apiUrl);
                $newAPIUrl = str_replace(['#NUMBERS', '#MESSAGE'], [$requestParameters->contact, urlencode($requestParameters->message)], $baseAPIUrl);

                $requestParameters->url     =   $newAPIUrl;
                $response   =   sendGetSMS($requestParameters);

            }elseif($ApiCredential->method == "POST") {
                $requestParameters->url     =   $ApiCredential->api_url;
                $tags       =   arrayToString($ApiCredential->tags);
                $set_tag    =   arrayToString($ApiCredential->set_tags);

                $postUrl    =   "key=#APIKEY&routeid=#ROUTEID&type=#TYPE&contacts=#CONTACTS&senderid=#SENDERID&msg=#MESSAGE";
                $postFields = str_replace($tags, $set_tag, $postUrl);
                $postParameters = str_replace(['#CONTACTS', '#MESSAGE'], [$requestParameters->contact, urlencode($requestParameters->message)], $postFields);

                $requestParameters->postParameters     =   $postParameters;
                $response   =   sendPostSMS($requestParameters);

                // dd($response);
            }

            // dd($response);
            // dd($requestParameters, $ApiCredential, $request->all(), $response);

            if(($response->getData()->status == "success") && ($response->getData()->code == 200)) {

                // echo $ApiCredential->method;
                // echo "<br>".$ApiCredential->response_rules;
                // echo "<br>".$ApiCredential->success;
                // echo "<br>".$ApiCredential->error;

                if(is_array($response->getData()->data)) {
                    $apiResponse    =   $response->getData()->data;
                }elseif(is_string($response->getData()->data)) {
                    if(isJSON($response->getData()->data) == true) {
                        $apiResponse    =   json_decode($response->getData()->data);
                    }else{
                        $apiResponse    =   $response->getData()->data;
                    }
                }

                // /**
                //  * Save in Delivery
                //  */
                // if($requestParameters->type == 'single') {
                //     $data = self::saveDelivery($requestParameters);
                // }else{
                //     $data = self::saveDelivery($requestParameters);
                // }

                if($ApiCredential->response_rules == "PREFIX") {

                    $responseString =  checkString($apiResponse, $ApiCredential->success);

                    if($responseString == true) {
                        Log::info(' API FINAL SUCCESS RESPONSE :: '. $ApiCredential);
                        self::saveDelivery($requestParameters, 'send');
                        return response()->json(['status' => 'success', 'data' => $apiResponse, 'msg' => 'send sms successfully', 'code' => 200]);
                    }elseif($responseString == false) {
                        Log::info(' API FINAL FAILED RESPONSE :: '. $ApiCredential);
                        self::saveDelivery($requestParameters, 'reject');
                        return response()->json(['status' => 'fail', 'data' => $apiResponse, 'msg' => 'API Error occured please contact service provider', 'code' => 201]);
                    }else{
                        Log::info(' API FINAL ERROR Response Unknown not match in Our Database');
                        self::saveDelivery($requestParameters, 'reject');
                        return response()->json(['status' => 'error', 'data' => null, 'msg' => 'Unknown Error Response Please contact admin', 'code' => 500]);
                    }
                }else {

                    $checkExist = property_exists($apiResponse, $ApiCredential->response_rules);

                    // dd($checkExist, $apiResponse, $ApiCredential->response_rules, $ApiCredential->success);
                    if($checkExist == true) {

                       $objVal = $apiResponse->{$ApiCredential->response_rules};

                        if($objVal == $ApiCredential->success) {
                            // $ApiCredential->success;
                            Log::info('\n API FINAL SUCCESS RESPONSE :: '. $ApiCredential->success);
                            self::saveDelivery($requestParameters, 'send');
                            return response()->json(['status' => 'success', 'data' => $apiResponse->MessageData, 'msg' => $apiResponse, 'code' => 200]);
                        }elseif($objVal == $ApiCredential->error) {
                            // $ApiCredential->error
                            Log::info('\n API FINAL FAILED RESPONSE :: '. $ApiCredential->error);
                            self::saveDelivery($requestParameters, 'reject');
                            return response()->json(['status' => 'fail', 'data' => null, 'msg' => $apiResponse->ErrorMessage, 'code' => 201]);
                        }else{
                            Log::info('\n API FINAL ERROR Response Unknown not match in Our Database');
                            self::saveDelivery($requestParameters, 'reject');
                            return response()->json(['status' => 'error', 'data' => null, 'msg' => 'Unknown Error Response Please contact admin', 'code' => 500]);
                        }

                    }else{
                        $msg = "API Rules Not Match Error Response Please contact admin";
                        Log::info('\n '.$msg);
                        self::saveDelivery($requestParameters, 'reject');
                        return response()->json(['status' => 'error', 'data' => null, 'msg' => $msg, 'code' => 501]);
                    }
                }

            }
        } catch (Exception $e) {
            // $e->getMessage()
            self::saveDelivery($requestParameters, 'reject');
            return response()->json(['status' => 'fail', 'data' => null, 'msg' => "API Response Failed Please Contact Administrative", 'code' => 200]);
        }

    }

    public static function saveDelivery($request, $status=null) {

        $smsDelivery = SMSDeliveries::where('deliveries_uuid', $request->deliveries_uuid ?? null)->first();

        if($smsDelivery == null) {
            $smsDelivery                    =    new SMSDeliveries;
            $smsDelivery->deliveries_uuid   =   Str::uuid();
        }

        $smsDelivery->user_uuid         =   $request->userid;
        $smsDelivery->company_uuid      =   $request->customer_uuid;
        $smsDelivery->api_uuid          =   $request->api_uuid;
        $smsDelivery->client_uuid       =   Auth::user()->id;
        $smsDelivery->number            =   $request->contact;
        $smsDelivery->message           =   $request->message;

        if($status == 'waiting') {
            $smsDelivery->delivery_status   =   'waiting';
        }elseif($status == 'send') {
            $smsDelivery->delivery_status   =   'send';
        }elseif($status == 'reject'){
            $smsDelivery->delivery_status   =   'reject';
        }else{
            $smsDelivery->delivery_status   =   'waiting';
        }

        $smsDelivery->save();

        return $smsDelivery;

    }



}
