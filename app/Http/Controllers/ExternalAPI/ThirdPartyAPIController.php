<?php

namespace App\Http\Controllers\ExternalAPI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExternalAPIs;
use App\Models\APIDetails;
use DataTables;

class ThirdPartyAPIController extends Controller
{
    public function showAllAPIS(Request $request)
    {

        if ($request->ajax()) {

            $data   =   ExternalAPIs::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                        $btn = '<a href="'. route('externalapi.chnage.status',$row->uuid) .'" class="remove btn btn-info btn-sm">Status</a>
                                <a href="'. route('contact.group.create',$row->uuid) .'" class="edit btn btn-primary btn-sm">Edit</a>
                                <a href="'. route('externalapi.remove',$row->uuid) .'" class="remove btn btn-danger btn-sm">Remove</a>
                                ';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('external-apis.show-apis');
    }

    public function addNewAPIS(Request $request)
    {
        return view('external-apis.import-new-api');
    }

    public function importNewAPIS(Request $request)
    {


        $request->validate([
            'comapny_name'  =>  'required',
            'email'  =>  'required',
            'contact'  =>  'required',
            'title'  =>  'required',
            'api_url'  =>  'required',
            'purchase'  =>  'required',
            'expired'  =>  'required',
            'status'  =>  'required',
        ]);

        $extApi     =   new ExternalAPIs;

        $extApi->uuid                   =   uuid();
        $extApi->comapny_name           =   $request->comapny_name;
        $extApi->contact           =   $request->contact;
        $extApi->email           =   $request->email;
        $extApi->location           =   $request->location;


        if($extApi->save())
        {
            $apiDetails  =   new APIDetails;
            $apiDetails->uuid           =   uuid();

            $apiDetails->customer_id    =   $extApi->uuid;
            $apiDetails->title          =   $request->title;
            $apiDetails->api_url        =   $request->api_url;
            $apiDetails->api_key        =   $request->api_key;
            $apiDetails->api_token      =   $request->api_token;
            $apiDetails->api_password   =   $request->api_password;
            $apiDetails->purchase       =   $request->purchase;
            $apiDetails->expired        =   $request->expired;
            $apiDetails->status         =   $request->status;
            $apiDetails->description    =   $request->description;

            $apiDetails->save();
        }

       return redirect()->route('list.external.apis')->with('success', 'API Imported Successfully');
    }

    public function removeAPIS(Request $request, $uuid=null)
    {
        if($uuid == null) {
            return redirect()->route('error404')->with('error', 'Invalid Direction Please Contact Administrative');
        }

        ExternalAPIs::where('uuid', $uuid)->delete();
        APIDetails::where('customer_id', $uuid)->delete();

        return redirect()->route('list.external.apis')->with('success', 'API Removed Successfully');
    }

    public function changeExternalAPIStatus(Request $request, $uuid=null)
    {
        if($uuid == null) {
            return redirect()->route('error404')->with('error', 'Invalid Direction Please Contact Administrative');
        }
        $api = APIDetails::where('customer_id', $uuid)->first();

        if($api->status == 1) {
            $status = 'true';
        }else{
            $status = 'false';
        }


        APIDetails::where('customer_id', $uuid)->update(['status' => $status]);

        return redirect()->route('list.external.apis')->with('success', 'API Status Updated Successfully');
    }


}
