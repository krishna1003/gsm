<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SenderID_Details;
use Validator;
use Auth;
use DataTables;
use Str;
use App\Http\Controllers\API\SenderIDAPICOntroller;

class ManageSenderIDs extends Controller
{
    /**
     * Show All sender IDS
     */
    public function showSendersId(Request $request)
    {
        if ($request->ajax()) {

            $apiResponse = SenderIDAPICOntroller::getSenderIDS($request);
            $response = $apiResponse->getData();
            
            if(($response->status == "success") && ($response->code == 200)) {
                $data = $response->data;
            }
            // $data = SenderID_Details::latest()->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('user_name', function($row){
                    $userName = geUser($row->user_uuid); 
                    return $userName;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('change.sender.status',[$row->sender_uuid, 'approve']) .'" class=" btn btn-info btn-sm">Approve</a>
                            <a href="'. route('change.sender.status',[$row->sender_uuid, 'rejected']) .'" class=" btn btn-danger btn-sm">Reject</a>
                            <a href="'. route('apply.new.senderids',$row->sender_uuid) .'" class="edit btn btn-primary btn-sm">Edit</a>
                            <a href="'. route('create.sender.remove',$row->sender_uuid) .'" class="remove btn btn-danger btn-sm">Remove</a>
                            ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('sender-ids.show-ids');
    }

    public function applyForSenderID(Request $request, $senderid=null)
    {
        if($request->isMethod('get') && $senderid != null) {
            $senderID = SenderID_Details::where('sender_uuid', $senderid)->first();
            
            if($senderID == null) {
                return redirect()->back()->with('error', 'Invalid Route Way');
            }

            return view('sender-ids.create-senderid')->with('senderData', $senderID);
        }

        return view('sender-ids.create-senderid');
    }

    public function storeSenderIDS(Request $request, $uuid=null) {

        if($request->isMethod('post')) {

            // dd($request->all());
            $user = Auth::user();
            $user_type = 'user';
            $status = 'apply';
            $request->request->add(['user_type' => $user_type, 'user_id' => $user->id, 'status' => $status]);
            
            if($uuid != null) {
                $request->request->add(['sender_uuid' => $uuid]);
            }

            $apiResponse = SenderIDAPICOntroller::storeSenderIDs($request);

            // dd($response);
            $response = $apiResponse->getData();
            
            if(($response->status == "success") && ($response->code == 200)) {
                $data = $response->data;
                return \redirect()->back()->with('success', 'apply for Sender ID Success');
            }else{
                return \redirect()->back()->with('error', 'Invalid Form Details');
            }
            
        }else{
            return view('layouts.error-404');
        }

    }

    

    public function removeSenderID(Request $request, $uuid=null)
    {
        if($uuid) {
            
            $senderID = SenderID_Details::where('sender_uuid', $uuid)->delete();
            return redirect()->back()->with('succes', 'Removed Sender ID');
        }else{
            return view('layouts.error-404');
        }
    }

    public function changeSenderIDStatus(Request $request, $uuid=null, $status=null)
    {
        // dd($request->all(), $uuid, $status);

        if($uuid != null && $status != null) {
            
            if ($status == "apply") {
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'apply']);
            }elseif ($status == 'pending') {
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'pending']);
            }elseif ($status == 'process') {
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'process']);
            }elseif ($status == 'approve') {
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'approve']);
            }elseif ($status == 'rejected') {
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'rejected']);
            }else{
                SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'rejected']);
            }
            
            return redirect()->back()->with('succes', 'Update Status Sender ID');
        }else{
            return view('layouts.error-404');
        }
    }
}
