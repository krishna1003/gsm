<?php
namespace App\Http\Controllers;

use App\UserSmsPackage;
use App\Payment;
use App\Wallet;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Support\Facades\Log;
use Razorpay\Api\Api;

class MembersController extends Controller {

public $razor_key = 'rzp_test_jmSRPUSopYJQPQ';
public $razor_secret = 'sZJqpHFumbKViSqUr1SDnZ08';


public function index()
    {
        $activeTab = 'manage_usersmspackage';
        $smspackages = DB::table("sms_package_list")->where('status', '=',"1")->get();
        return View('wallet',compact('activeTab','smspackages'));
    }

    public function transaction_history(Request $request)
    {

        $activeTab = "transaction-history";
        $user = Auth::user();
        $user_id = $user->id;

        $trahisdata = Payment::WHERE('userid','=',$user_id)->get();
        //dd($contacts);
        return view('transaction-history',compact('activeTab','trahisdata'));
    }


public function wallet(Request $request) {
        $user = Auth::user();

        $user_id = $user->id;


 if ($request->isMethod('post')) {
        $validatedData = $this->validate($request,[
            'wallet_amount' => 'required|numeric|min:10'
        ]);

        $UsersWallet = new Wallet();
        $where = array();
        $where[] = array('userid','=',$user_id);
        $wallet_row = $UsersWallet->where($where)->first();

        $wallet_amount = $request->amount;
        $api = new Api($this->razor_key, $this->razor_secret);
        $arr = array(
            'receipt'         => 'order_rcptid_' . $user_id,
            'amount'          => $wallet_amount, // amount in the smallest currency unit
            'currency'        => 'INR',// <a href="https://razorpay.freshdesk.com/support/solutions/articles/11000065530-what-currencies-does-razorpay-support" target="_blank">See the list of supported currencies</a>.)
            'payment_capture' =>  '0'
        );
        $orderId = $api->Order->create($arr);

        return view('members.paymentform',compact('rows','wallet_row', 'user', 'wallet_amount','orderId')); 



    }else {

        $UsersWallet = new UsersWallet();
        $where = array();
        $where[] = array('user_id','=',$user_id);
        $wallet_row = $UsersWallet->where($where)->first();

        return view('members.wallet',compact('rows','wallet_row')); 
    }

}

public function storepremiumRegister (Request $request){ 
        
        $data =$request->all();
        $success = false;
     
       $api = new Api( 'rzp_test_jmSRPUSopYJQPQ',  'sZJqpHFumbKViSqUr1SDnZ08');
        
        $payment = $api->payment->fetch($request->payment_id);
       $error = '';
        if(!empty($request)&& !empty($request->payment_id)){
            try {
                $response = $api->payment->fetch($request->payment_id)->capture(array('amount'=>$request->netamount*100));
                
                 if (isset($response['error']) === false) {
                            $success = true;
                        }else {
                            $success = false;
                            if (!empty($response['error']['code'])) {
                                $error = $response['error']['code'].':'.$response['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$response;
                            }
                        }
                        
        if($success ==true)
        {
                    
                    $date = date('Y-m-d h:i:s');
                    $user = Auth::user();
                    $userid = $user->id;

                    $Payment = new Payment;
                    $Payment->userid = $userid;
                    $Payment->amount = $request->input('amount');
                    $Payment->type   = 'online';
                    $Payment->remark = 'razorpay';
                    $Payment->status = 'paid';
                    $Payment->payment_id = $request->payment_id;
                    $Payment->created_at = $date;
                    $Payment->save();
                    

                    $payment_id = $Payment->id;
                    $UserSmsPackage = new UserSmsPackage;
                    $UserSmsPackage->userid = $userid;
                    $UserSmsPackage->smspackageid = $request->input('smspackageid');
                    $UserSmsPackage->amount = $request->input('amount'); 
                    $UserSmsPackage->no_of_msg = $request->input('no_of_msg');
                    $UserSmsPackage->payment_id = $payment_id;
                    $UserSmsPackage->status = 1;
                    $UserSmsPackage->created_at = $date;
                    $UserSmsPackage->save();

                    $nomsg = DB::table('wallet')->where('userid','=',$userid)->get();
                   if($nomsg -> count() > 0)
                    {
                        $nomsg1 = DB::table('wallet')->where('userid','=',$userid)->first();
                       $totalmsg = $nomsg1->no_of_msg + $request->input('no_of_msg');
                    }else
                    {
                       $totalmsg =  $request->input('no_of_msg');
                    }
                    
                    //dd($totalmsg);

                    $wallet = Wallet::updateOrCreate([
                                'userid' => $userid
                            ], [
                                'no_of_msg' => $totalmsg,
                                
                            ]);
                   
                   
                     
                    
                     
                      
                      /*end  Send welcome mail */
                    $arr = array('msg' => 'Recharge done successfully.', 'status' => true);
                    return Response()->json($arr);
        }
        else{
                $arr = array('msg' => $error, 'status' => false);
        return Response()->json($arr);          
            }
                
//print_r($response); exit; 
                //$arr = array('msg' => 'Payment successfully credited', 'status' => true,'tempcode'=> $tempcode);
                //return Response()->json($arr);
            } catch (\Exception $e) {
                $arr = array('msg' => $e->getMessage(), 'status' => false);
                return Response()->json($arr);
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            }

            // Do something here for store payment details in database...
        }
     
    }


//payment_success method is called when payemtn ge success 

function payment_success(Request $request) {
    $user = Auth::user();
    $user_id = $user->id;

        $api = new Api($this->razor_key, $this->razor_secret);

        $payment = $api->payment->fetch($request->razorpay_payment_id);


        if(!empty($request->razorpay_payment_id)) {
            try {
                $response = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => $payment['amount'])); 

                //below parameters are received from razor pay. you can save it a database table.   
                $response['id'];
                $response['entity'];
                $response['amount'];
                $response['currency'];
                $response['status'];
                $response['order_id'];
                $response['invoice_id'];
                $response['international'];
                $response['method'];
                $response['amount_refunded'];
                $response['refund_status'];
                $response['captured'];
                $response['description'];
                $response['card_id'];
                $response['bank'];
                $response['wallet'];
                $response['vpa'];
                $response['email'];
                $response['contact'];
                $response['fee'];
                $response['tax'];
                $response['error_code'];
                $response['error_description'];




                return redirect('/wallet')->with('success','Amount added successfull!');



            } catch (\Exception $e) {
                return  $e->getMessage();
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            }

            // Do something here for store payment details in database...
        }

        return redirect()->back();
    }

}