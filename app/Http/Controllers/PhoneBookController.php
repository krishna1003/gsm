<?php

namespace App\Http\Controllers;

use App\PhoneBook;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Support\Facades\Log;

class PhoneBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_phonebook';
        $PhoneBookData = PhoneBook::all();
        return View('phone_book/show-contacts',compact('PhoneBookData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_phonebook';
        return View('phone_book/create-contact',compact('activeTab')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $user = Auth::user();
        $PhoneBook = new PhoneBook;
        $PhoneBook->name = $request->input('name');
        $PhoneBook->mobile_number = $request->input('mobile_number');
        $PhoneBook->userid = $user->id;
        $PhoneBook->status = $request->input('status') != "" ? $request->input('status') : 0;
        $PhoneBook->created_at = $date;

        

        if(!$PhoneBook->save()) {
            //Redirect error
            return redirect()->route('phonebook')->with('error','Failed to update PhoneBook details.');
        }

        //Redirect success
        return redirect()->route('phonebook')->with('success','Contact added successfully.');
    
    }

    public function changePhoneBookStatus(Request $request) {
        $PhoneBook_id = $request->PhoneBook_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($brand_id)) {
            $PhoneBook = PhoneBook::where('id',$PhoneBook_id)
                                ->update(['status' => $status]);

            if($PhoneBook) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_phonebook'; 
        $PhoneBook = PhoneBook::find($id);
        return View('phone_book/create-contact',compact('PhoneBook','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $PhoneBook = PhoneBook::find($id);
            $PhoneBook->name = $request->input('name');
            $PhoneBook->mobile_number = $request->input('mobile_number');
            $PhoneBook->status = $request->input('status') != "" ? $request->input('status') : 0;
            $PhoneBook->updated_at = $date;

            

            if($PhoneBook->save()) {
                //Redirect success
                return redirect()->route('phonebook')->with('success','Contact updated successfully.');
            }

            //Redirect error
            return redirect()->route('phonebook')->with('error','Failed to update contact.');


        } catch (Exception $e) {
            Log::error("Failed to update contact.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

     public function changephonebookStatus_datatable(Request $request, $id) {
        $phonebook = PhoneBook::find($id);

        if($phonebook->status == 1) {
            $phonebook->status = 0;
        }elseif($phonebook->status == 0) {
            $phonebook->status = 1;
        }
        $phonebook->save();

        return redirect()->route('phonebook')->with('success', 'PhoneBook Status Updated');

         
        

    }

    public function allPhoneBook(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2 =>'number',
                            3=> 'status',
                        );
  
        $totalData = PhoneBook::count();
        $user = Auth::user();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = PhoneBook::Where('userid', $user->id)
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = PhoneBook::where('name','LIKE',"%{$search}%")
                         ->orWhere('mobile_number', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->andWhere('userid', $user->id)
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = PhoneBook::where('name','LIKE',"%{$search}%")
                         ->orWhere('mobile_number', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->andWhere('userid', $user->id)
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('phonebook.edit',$post->id); 
                $edit = route('phonebook.edit', $post->id);
                $delete = route('phonebook.delete', $post->id);
                
                $st = route('change.phonebook_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['number'] = $post->mobile_number;
                
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause Phonebook"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeBrandStatus" data-toggle="tooltip" title="Activate Phonebook"> <i class="fa fa-play"></i> </a>&nbsp;';
                }
                 
                
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit Phonebook"><i class="fa fa-edit"></i></a>&nbsp;';

                 $options .= '<a href="'.$delete.'" class="btn btn-rounded btn-sm btn-danger" data-toggle="tooltip" title="Delete Phonebook"><i class="fa fa-trash"></i></a>';
                 

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }

    public function delete1($id)
    {
        $activeTab = 'manage_phonebook';
        $PhoneBook = PhoneBook::find($id)->delete();
         return redirect()->route('phonebook')->with('success','Contact Deleted Successfully.');
    }
}
