<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\User;

use DB;
use Crypt;
use Razorpay\Api\Api;
use Redirect,Response;
use Illuminate\Support\Str;
use Auth;
use Mail;
use PDF;



class PremiumsignupController extends Controller
{
	
	
	 public function index($token = null){

       $user = array();
	   
	    $referal_code = '';
		$reference_name = '';
	    if($token != ''){
			
			
			$refrence_user = User::where('referal_code',$token)->whereIn('roleid', [1, 2, 3, 4, 5])->first();
			if($refrence_user != ''){
				$referal_code = $refrence_user->referal_code;
				$reference_name = $refrence_user->name;
			}
			
	    } 
	
           
        //$membershiptypes = MembershipType::where('status',1)->orderBy('id','asc')->get();
		
		if (Auth::check()) 
		{
				$user = Auth::user();
				$user = User::join('country', 'users.country', '=' ,'country.id')->join('state', 'users.state', '=' ,'state.id')->join('city', 'users.city', '=' ,'city.id')->select(DB::raw("users.*,country.name as countyname,state.name as statename,city.name as cityname"))->where('users.id',$user->id)->first();
			if($user->roleid != 6){
				return redirect('dashboard');
			}	
		}

		return view('auth.premium-register',compact('user','referal_code','reference_name'));
        
    }
	
	public function storepremiumRegister (Request $request){ 
		
		$data =$request->all();
		$success = false;
     
	   $api = new Api( env('RAZORPAY_KEY'),  env('RAZORPAY_SECRET'));
        
        $payment = $api->payment->fetch($request->payment_id);
       $error = '';
        if(!empty($request)&& !empty($request->payment_id)){
            try {
                $response = $api->payment->fetch($request->payment_id)->capture(array('amount'=>$request->netamount*100));
				
				 if (isset($response['error']) === false) {
                            $success = true;
                        }else {
                            $success = false;
                            if (!empty($response['error']['code'])) {
                                $error = $response['error']['code'].':'.$response['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$response;
                            }
                        }
						
		if($success ==true){
							if ($data['tnc_accepts'] == ""){
            $data['tnc_accepts'] = 0;
        }
        else
        {
           $data['tnc_accepts'] = 1; 
        }
		$createddate = date('Y-m-d h:i:s');
		$startdate = date('Y-m-d');
		
		$fname= trim($data['firstname']);
		$lname= trim($data['lastname']);
		$f=  substr(strtoupper($fname), 0, 1);
		$l=  substr(strtoupper($lname), 0, 1);

		$membership_code= 'PM'.$f.$l.date('Y').generateCode(5);
		$dob =date("Y-m-d", strtotime($request->dob));
		$whatsappno = ($request->IsWhatsapp != "" ? $request->contact : ($request->whatsappno != "" ? $request->whatsappno : ""));
	 $referal_user = User::where('referal_code','=',$data['reference_code'])->first();

	   if (Auth::check()) {
		   
		   $user = Auth::user();
		  $userupdate = User::where('id',$user->id)->update(['roleid' => 7,'referal_code'=>$membership_code, 'pincode' => $data['pincode'], 'locality' => $data['locality'],'dob' =>$dob,'whatsappno' => $whatsappno,'is_verified'=>1, 'verified_at'=>$createddate, 'parentid' =>$referal_user->id]);
		   $userid =  $user->id; 
		
	   }else{
		 $user = new User();
		   $userd = array(
		    'username' => $data['admin_usr'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'name' => $data['firstname'].' '.$data['lastname'],
            'email' => $data['email'],
            'email_verified_at' => date('Y-m-d h:i:s'),
            'password' => Crypt::encrypt($data['password']),
            'contactno' => $data['contact'],
            'ageGroup' => $data['ageGroup'],
            'address' => $data['address'],
            'country' => $data['country'],
            'state' => $data['state'],
            'city' => $data['city'],
            'tnc_accept' => $data['tnc_accepts'],
            'referal_code' => $membership_code,
            'pincode' => $data['pincode'],
            'locality' => $data['locality'],
			'dob' => $dob,
            'activation_code' => Str::random(20),
            'roleid' => 7,
            'status' => 1,
            'created_at' => $createddate,
			'whatsappno' => $whatsappno,
			'is_verified'=>1,
			'verified_at'=>$createddate,
			'parentid' =>$referal_user->id
			 
           
		);
		$user->fill( $userd );
		$user->save();
		$doc  =   new UserDocuments;
        $doc->userid  =   $user->id;
        $doc->doctypeid =   $user->id;
        $doc->save();
		$userid = $user->id; 
		   
	   }
	   
	   $membershiptype = MembershipType::where('id',$data['membershipid'])->first();
		if($membershiptype->is_package == 1){
			$amount = $membershiptype->amount;
			$person = 0;
		}else{
			$amount = $membershiptype->amount * $data['business_person'];
			$person = $data['business_person'];
		}
		
		
		$usermembership = new Usermembership();
		
		 $usermembershipd = array(
		    'userid' => $userid,
            'membershiptypeid' => $data['membershipid'],
            'membership_code' => $membership_code,
            'amount' => $amount,
            'person' => $person,
            'referal_code' => $data['reference_code'],
			'startdate' => $startdate,
            'payment_type' => $data['payment_type'],
            'tnc_accept' => $data['tnc_accepts'],
            'status' => 1,           
		);
		
		//print_r($usermembershipd); exit;
		$usermembership->fill( $usermembershipd );
		$usermembership->save();
		$usermembershipid = $usermembership->id;
		
		$transection_data = json_encode($response);

			$subscription = new Subscription();
			//echo $request->payment_id; exit;
			//echo $response; exit;
		 $trans_id = $user->username.'_trans_1';
		 $subscriptiond = array(
		    'membershipid' => $usermembershipid,
            'membershiptypeid' => $data['membershipid'],
            'trans_id' => $trans_id,
            'convenience_fee' => $data['fees'],
            'trans_id' => $trans_id,
            'transection_id' => $data['payment_id'],
            'date' => date('Y-m-d'),
            'amount' => $amount,
            'transection_data' => $transection_data,
            'payment_type' => $data['payment_type'],
			'status' => 1
         );
		 $subscription->fill( $subscriptiond );
		 $subscription->save();
		 
		
		 
		  /*  Send welcome mail */
		  
		  $mail_data=array(
		   'to'=>$user->email,
		   'name'=>$user->name,
		   'contactno'=>$user->contactno,
		   'address'=>$user->address,
		   'userid'=>$user->id,
		   'membership_id'=>$usermembershipid,
		   'membership_number'=>$usermembership->membership_code,
		   'membership_type'=>$membershiptype->name,
		   'registration_amount'=>$usermembership->amount,
		   'registration_date'=> date('d/m/Y', strtotime($usermembership->startdate)),
		   
		  );
		  $this->send_premium_member_mail($mail_data);
		  /*end  Send welcome mail */
		$arr = array('msg' => 'Your Premium membership registration done successfully.', 'status' => true);
		return Response()->json($arr);
		}
		else{
				$arr = array('msg' => $error, 'status' => false);
		return Response()->json($arr);			
			}
				
//print_r($response); exit; 
				//$arr = array('msg' => 'Payment successfully credited', 'status' => true,'tempcode'=> $tempcode);
				//return Response()->json($arr);
            } catch (\Exception $e) {
                $arr = array('msg' => $e->getMessage(), 'status' => false);
                return Response()->json($arr);
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            }

            // Do something here for store payment details in database...
        }
	 
	}
	public function store(Request $request){

	   $data =$request->all();
      
	   if ($data['tnc_accepts'] == ""){
            $data['tnc_accepts'] = 0;
        }
        else
        {
           $data['tnc_accepts'] = 1; 
        }
		$createddate = date('Y-m-d h:i:s');
		
		
		$fname= trim($data['firstname']);
		$lname= trim($data['lastname']);
		
		
		$f=  substr(strtoupper($fname), 0, 1);
		$l=  substr(strtoupper($lname), 0, 1);

		//$membership_code= 'PM'.$f.$l.date('Y').generateCode(5);
		$membership_code= 'PM'.$f.$l.date('Y').generateCode(5);
		$dob =date("Y-m-d", strtotime($request->dob));
		$whatsappno = ($request->IsWhatsapp != "" ? $request->contact : ($request->whatsappno != "" ? $request->whatsappno : ""));
		 $referal_user = User::where('referal_code','=',$data['reference_code'])->first();
	   if(Auth::check()){
		  
		 $user = Auth::user();
		 $userupdate = User::where('id',$user->id)->update(['roleid' => 7,'referal_code'=>$membership_code, 'pincode' => $data['pincode'], 'locality' => $data['locality'],'dob' => $dob,'whatsappno'=>$whatsappno, 'parentid' =>$referal_user->id]);
		 $userid =  $user->id; 
		
	   }else{
		  $user = new User();
		   $userd = array(
		    'username' => $data['admin_usr'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'name' => $data['firstname'].' '.$data['lastname'],
            'email' => $data['email'],
            'email_verified_at' => date('Y-m-d h:i:s'),
            'password' => Crypt::encrypt($data['password']),
            'contactno' => $data['contact'],
            'ageGroup' => $data['ageGroup'],
            'address' => $data['address'],
            'country' => $data['country'],
            'state' => $data['state'],
            'city' => $data['city'],
			'pincode' => $data['pincode'],
            'locality' => $data['locality'],
			'dob' => $dob,
            'tnc_accept' => $data['tnc_accepts'],
            'referal_code' => $membership_code,
            'activation_code' => Str::random(20),
            'roleid' => 7,
            'status' => 1,
            'created_at' => $createddate,
			'whatsappno'=>$whatsappno,
			'parentid' =>$referal_user->id
		);
		$user->fill( $userd );
		$user->save();
		$doc  =   new UserDocuments;
        $doc->userid     =   $user->id;
        $doc->doctypeid     =   $user->id;
        $doc->save();
		$userid = $user->id; 
		   
	   }
	   
	   $membershiptype = MembershipType::where('id',$data['membershipid'])->first();
		if($membershiptype->is_package == 1){
			$amount = $membershiptype->amount;
			$person = 0;
		}else{
			$amount = $membershiptype->amount * $data['business_person'];
			$person = $data['business_person'];
		}
		$usermembership = new Usermembership();
		//$membership_code= generateCode(8);
		
		 $usermembershipd = array(
		    'userid' => $userid,
            'membershiptypeid' => $data['membershipid'],
			'membership_code' => $membership_code,
            'amount' => $amount,
            'person' => $person,
            'referal_code' => $data['reference_code'],
            'payment_type' => $data['payment_type'],
            //'payment_file' => $data['ageGroup'],
            'payment_remark' => $data['remark'],
            'tnc_accept' => $data['tnc_accepts'],
            'status' => 0,
           
		);
		
		//print_r($usermembershipd); exit;
		
		
		if($request->hasFile('image')) {
                $file1 = $request->file('image');
                $filename1 = str_random(2).time().$file1->getClientOriginalName();
                $file1->move(public_path().'/uploads/membeship_images/', $filename1);
                $usermembershipd['payment_file'] = $filename1;
               
               
                
            }else {
				 $usermembershipd['payment_file'] = '';
				
			}
		$usermembership->fill( $usermembershipd );
		$usermembership->save();
		$usermembershipid = $usermembership->id;
		
		$subscription = new Subscription();
		 $trans_id = $user->username.'_trans_1';
		 $subscriptiond = array(
		    'membershipid' => $usermembershipid,
            'membershiptypeid' => $data['membershipid'],
            'trans_id' => $trans_id,
            'transection_id' => '0',
            'date' => date('Y-m-d'),
            'amount' => $amount,
            'transection_data' => 'offline',
			'payment_type' => $data['payment_type'],
            'payment_file' => $usermembershipd['payment_file'],
            'payment_remark' => $data['remark'],
			'status' => 0
         );
		 $subscription->fill( $subscriptiond );
		 $subscription->save();
	   
	    Session::flash('message', 'Your Premium membership registration done successfully.');
        return redirect('premiumRegister');
        
        
    }
	
    public function existreference_code(Request $request){

       $referal_code_exists = (count(\App\User::where('referal_code', '=', $request->input('reference_code'))->whereIn('roleid', [1, 2, 3, 4, 5])->get()) > 0) ? true : false;
            return response()->json($referal_code_exists);
        
    }
	
	 public function getuserdetail(Request $request){
		  $username = $request->input('username');

		 $user = User::join('country', 'users.country', '=' ,'country.id')->join('state', 'users.state', '=' ,'state.id')->join('city', 'users.city', '=' ,'city.id')->select(DB::raw("users.*,country.name as countyname,state.name as statename,city.name as cityname"))->where('users.username',$username)->where('users.roleid',6)->first();
		 
		 if($user != null){
			 
			 $msg= array('status' => 'success', 'data'=>$user);
			 
		 }else{
			 
			  $msg= array('status' => 'fail');
			  
		 }
		  return response()->json($msg);
	 }
	 
	 public function getuserreferencedetail(Request $request){
		  $reference = $request->input('reference');

		 $user = User::where('referal_code',$reference)->first();
		 
		 if($user != null){
			 
			 $msg= array('status' => 'success', 'data'=>$user);
			 
		 }else{
			 
			  $msg= array('status' => 'fail');
			  
		 }
		  return response()->json($msg);
	 }
	 
	 function send_premium_member_mail($data){
		 
		 
		 $data['signature'] = public_path().'/front/images/Shukul-Eshop.png';
	   $pdf = PDF::loadView('pdf/welcome-letter', array('user'=>$data));  
        $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);
		$filename = "welcome-letter_" .time(). ".pdf";
		// Save file to the directory
		$pdf->save(public_path().'/generatepdf/'.$filename);
		$filemanagement = new FileManagement();
		$fm  = array(
		 'title'=>"welcome Letter",
		 'type'=>"user_membership",
		 'type_id'=>$data['membership_id'],
		 'userid'=>$data['userid'],
		 'filename'=>$filename,
		 'status'=>1,
		);
		$filemanagement->fill( $fm );
		$filemanagement->save();
		
		$pdfs = PDF::loadView('pdf/membership-letter', array('user'=>$data));  
        $pdfs->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);
		$filename1 = "membeship-letter_" .time(). ".pdf";
		// Save file to the directory
		$pdfs->save(public_path().'/generatepdf/'.$filename1);
		$filemanagement1 = new FileManagement();
		$fm1  = array(
		 'title'=>"premium membership registartion recipt",
		 'type'=>"user_membership",
		 'type_id'=>$data['membership_id'],
		 'userid'=>$data['userid'],
		 'filename'=>$filename1,
		 'status'=>1,
		);
		$filemanagement1->fill( $fm1 );
		$filemanagement1->save();
		 
		  /* Send welcome mail */
		 
		 $emailtemplate =  EmailTemplate::where('Label','premium_membership_registration')->first();

        $find =  array('#name#','#membership_number#','#membership_type#','#registration_amount#', '#registration_date#');
        $replace = array($data['name'], $data['membership_number'], $data['membership_type'], $data['registration_amount'], $data['registration_date']);
        
        $subject = $emailtemplate->subject;
        $content = str_replace($find,$replace,$emailtemplate->content);
       
        $to = $data['to'];

        Mail::send('email',compact('content'), function ($message) use ($emailtemplate,$to,$filename,$filename1){
            $message->from($emailtemplate->fromEmail, $emailtemplate->from);
            $message->to($to);
            $message->subject($emailtemplate->subject);
            $message->attach(public_path().'/generatepdf/'.$filename);
            $message->attach(public_path().'/generatepdf/'.$filename1);
        });
         
        if (Mail::failures()) 
        {
            $failures[] = Mail::failures()[0];
            //Session::flash('error', 'Please Try Again...');
        }
        else
        {
        	//Session::flash('success', 'Thank You For Your Message. It has been Sent...');
        }
		$smstemplate =  SmsTemplate::where('Label','premium-membership-registration-sms')->first();
		$finds =  array('#membership_code#');
        $replaces = array($data['membership_number']);
		 $msg = str_replace($finds,$replaces,$smstemplate->content);
		$contactno = $data['contactno'];
		$send= sendsms($contactno, $msg);
		
		
		


        //return $pdf->download('medium.pdf');
      	return;	
	 }
}
