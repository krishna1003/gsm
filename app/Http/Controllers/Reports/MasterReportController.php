<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\SMSDeliveries;

class MasterReportController extends Controller
{
    public function showSMSHistory(Request $request) {
        if ($request->ajax()) {

            $data = SMSDeliveries::latest()->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('user_uuid', function($row) {
                    return geUser($row->user_uuid);
                })
                
                ->addColumn('api_uuid', function($row) {
                    return getAPITitle($row->api_uuid);
                })
                ->addColumn('client_uuid', function($row) {
                    return geUser($row->user_uuid);
                })
                ->addColumn('action', function($row) {
                        $btn = '<a href="'. route('delivery.smsdescription',$row->deliveries_uuid) .'" class="view btn btn-primary btn-sm">View</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('reports.master-sms-delivery-report');
    }

    public function smsWholeDescription(Request $request, $uuid=null) {
        if($uuid == null) {
            return redirect()->route('error404');
        }

        $smsDetail = SMSDeliveries::with(['getAPIDetails','getCompanyDetails','getUserDetails','getClientDetails'])
                                    ->where('deliveries_uuid', $uuid)
                                    ->first();
        return view('reports.sms-full-description')->with('smsDetail', $smsDetail);
    }
}
