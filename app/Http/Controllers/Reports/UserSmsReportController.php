<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Models\SMSDeliveries;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class UserSmsReportController extends Controller {
	public function showSMSHistory(Request $request) {
		if ($request->ajax()) {

			$data = SMSDeliveries::latest()->get();

			return Datatables::of($data)
				->addIndexColumn()
				->addColumn('user_uuid', function ($row) {
					return geUser($row->user_uuid);
				})
				->addColumn('company_uuid', function ($row) {
					return getCompany($row->company_uuid);
				})
				->addColumn('api_uuid', function ($row) {
					return getAPITitle($row->api_uuid);
				})
				->addColumn('client_uuid', function ($row) {
					return geUser($row->user_uuid);
				})
				->addColumn('action', function ($row) {
					$btn = '<a href="' . route('delivery.smsdescription', $row->deliveries_uuid) . '" class="view btn btn-primary btn-sm">View</a>';

					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
		}
		$activeTab = 'user_sms_report';
		return view('usersms_report', compact('activeTab'));
	}

	public function smsWholeDescription(Request $request, $uuid = null) {
		if ($uuid == null) {
			return redirect()->route('error404');
		}

		$smsDetail = SMSDeliveries::with(['getAPIDetails', 'getCompanyDetails', 'getUserDetails', 'getClientDetails'])
			->where('deliveries_uuid', $uuid)
			->first();
		return view('reports.sms-full-description')->with('smsDetail', $smsDetail);
	}

	public function allUserSms(Request $request) {

		$columns = array(
			0 => 'deliveries_uuid',
			1 => ' date',
			2 => 'sender_id',
			3 => 'contact_no',
			4 => 'message',
		);

		$totalData = SMSDeliveries::count();

		$totalFiltered = $totalData;
		$user = Auth::user();
		$user_id = $user->id;

		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');

		if (empty($request->input('search.value'))) {
			$posts = SMSDeliveries::WHERE('user_uuid', $user_id)
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();
		} else {
			$search = $request->input('search.value');

			$posts = SMSDeliveries::WHERE('user_uuid', '=', $user_id)
				->where('message', 'LIKE', "%{$search}%")
				->orWhere('delivery_status', 'LIKE', "%{$search}%")
				->orWhere('number', 'LIKE', "%{$search}%")
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			/*$posts =     DB::select( DB::raw(" SELECT s.*,COUNT(*) as totalFiltered  FROM sms_deliveries s WHERE s.user_uuid = '$user_id' AND ( s.message LIKE '%$search%' or  s.delivery_status LIKE '%$search%' or  s.number LIKE '%$search%' )  LIMIT $start, $limit ORDER BY $order,$dir ") );*/
			/*$posts =  Product::where('id','LIKE',"%{$search}%")
				                            ->orWhere('name', 'LIKE',"%{$search}%")
				                            ->offset($start)
				                            ->limit($limit)
				                            ->orderBy($order,$dir)
			*/

			$totalFiltered = SMSDeliveries::WHERE('user_uuid', '=', $user_id)
				->where('message', 'LIKE', "%{$search}%")
				->orWhere('delivery_status', 'LIKE', "%{$search}%")
				->orWhere('number', 'LIKE', "%{$search}%")
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->count();
		}

		$data = array();
		if (!empty($posts)) {
			foreach ($posts as $post) {

				$nestedData['id'] = $post->deliveries_uuid;
				$nestedData['date'] = date_format($post->created_at, "Y/m/d H:i:s");
				$nestedData['sender_id'] = 'websms';
				$nestedData['contactno'] = $post->number;
				$nestedData['status'] = $post->delivery_status;
				$nestedData['message'] = $post->message;

				/*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
					                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
					                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
				*/
				$data[] = $nestedData;
				// $totalFiltered = $posts->totalFiltered;
				$totalFiltered = 50;

			}
		}

		$json_data = array(
			"draw" => intval($request->input('draw')),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);

	}
}
