<?php

namespace App\Http\Controllers;

use App\SmsPackage;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Support\Facades\Log;

class SmsPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_smspackage';
        $SmsPackageData = SmsPackage::all();
        return View('sms_package/show',compact('SmsPackageData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_smspackage';
        return View('sms_package/create',compact('activeTab')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $user = Auth::user();
        $SmsPackage = new SmsPackage;
        $SmsPackage->name = $request->input('name');
        $SmsPackage->details = $request->input('details');
        $SmsPackage->no_of_msg = $request->input('no_of_msg');
        $SmsPackage->amount = $request->input('amount');
        $SmsPackage->status = $request->input('status') != "" ? $request->input('status') : 0;
        $SmsPackage->created_at = $date;

        

        if(!$SmsPackage->save()) {
            //Redirect error
            return redirect()->route('sms_package')->with('error','Failed to add SMS Package');
        }

        //Redirect success
        return redirect()->route('sms_package')->with('success','SMS Package added successfully.');
    
    }

    public function changeSmsPackageStatus(Request $request) {
        $SmsPackage_id = $request->SmsPackage_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($brand_id)) {
            $SmsPackage = SmsPackage::where('id',$SmsPackage_id)
                                ->update(['status' => $status]);

            if($SmsPackage) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_smspackage';
        $SmsPackage = SmsPackage::find($id);
        return View('sms_package/create',compact('SmsPackage','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $SmsPackage = SmsPackage::find($id);
            $SmsPackage->name = $request->input('name');
            $SmsPackage->details = $request->input('details');
            $SmsPackage->no_of_msg = $request->input('no_of_msg');
            $SmsPackage->amount = $request->input('amount');
            $SmsPackage->status = $request->input('status') != "" ? $request->input('status') : 0;
            $SmsPackage->updated_at = $date;

            

            if($SmsPackage->save()) {
                //Redirect success
                return redirect()->route('sms_package')->with('success','Sms Package updated successfully.');
            }

            //Redirect error
            return redirect()->route('sms_package')->with('error','Failed to Sms Package.');


        } catch (Exception $e) {
            Log::error("Failed to update Sms Package.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

     public function changeSmsPackageStatus_datatable(Request $request, $id) {
        $sms_package = SmsPackage::find($id);

        if($sms_package->status == 1) {
            $sms_package->status = 0;
        }elseif($sms_package->status == 0) {
            $sms_package->status = 1;
        }
        $sms_package->save();

        return redirect()->route('sms_package')->with('success', 'SMS Package Status Updated');

         
        

    }

    public function allSmsPackage(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2 =>'details',
                            3=> 'no_of_msg',
                            4=> 'amount',
                            5=> 'status',
                            6=> 'action',
                        );
  
        $totalData = SmsPackage::count();
        $user = Auth::user();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = SmsPackage::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = SmsPackage::where('name','LIKE',"%{$search}%")
                         ->orWhere('details', 'LIKE',"%{$search}%")
                         ->orWhere('no_of_msg', 'LIKE',"%{$search}%")
                         ->orWhere('amount', 'LIKE',"%{$search}%")
                         ->orWhere('no_of_msg', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = SmsPackage::where('name','LIKE',"%{$search}%")
                         ->orWhere('details', 'LIKE',"%{$search}%")
                         ->orWhere('no_of_msg', 'LIKE',"%{$search}%")
                         ->orWhere('amount', 'LIKE',"%{$search}%")
                         ->orWhere('no_of_msg', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                 
                $edit = route('sms_package.edit', $post->id);
                $delete = route('sms_package.delete', $post->id);
                
                $st = route('change.sms_package_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['details'] = $post->details;
                $nestedData['no_of_msg'] = $post->no_of_msg;
                $nestedData['amount'] = $post->amount;
                
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeBrandStatus" data-toggle="tooltip" title="Activate"> <i class="fa fa-play"></i> </a>&nbsp;';
                }
                 
                
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit Phonebook"><i class="fa fa-edit"></i></a>&nbsp;';

                 $options .= '<a href="'.$delete.'" class="btn btn-rounded btn-sm btn-danger" data-toggle="tooltip" title="Delete Package"><i class="fa fa-trash"></i></a>';
                 

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }

    public function delete1($id)
    {
        $activeTab = 'manage_smspackage';
        $SmsPackage = SmsPackage::find($id)->delete();
         return redirect()->route('sms_package')->with('success','SmsPackage Deleted Successfully.');
    }
}
