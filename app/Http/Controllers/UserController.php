<?php

namespace App\Http\Controllers;

use App\EmailTemplate;
use App\Http\Controllers\Controller;
use App\User;
use App\Wallet;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Spatie\Permission\Models\Role;

class UserController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$data = User::orderBy('id', 'DESC')->paginate(5);
		return view('users.index', compact('data'))
			->with('i', ($request->input('page', 1) - 1) * 5);
	}

	public function userpanelindex(Request $request) {

		$activeTab = "sms-send";
		$tab = "single";
		$smstypetab = "singlesmstab";
		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		//dd($contacts);
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'groups'))->with('smstypetab', 'singlesmstab');
	}

	public function addressindex(Request $request) {

		$activeTab = "address-book";
		if (isset($_GET['tab'])) {
			if ($_GET['tab'] == 'single') {
				$tab = "single";
			} else {
				$tab = "multi";
			}

		} else {
			$tab = "single";
		}

		$smstypetab = "singlesmstab";
		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'smstypetab', 'groups'));
	}

	public function apikeyindex(Request $request) {

		$activeTab = "apikey";

		return view('apikey', compact('activeTab'));
	}

	public function multismsindex(Request $request) {

		$activeTab = "sms-send";
		$tab = "single";
		$smstypetab = "multismstab";

		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'smstypetab', 'groups'));
	}
	public function groupsmsindex(Request $request) {

		$activeTab = "sms-send";
		$tab = "single";
		$smstypetab = "groupsmstab";

		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'smstypetab', 'groups'));
	}
	public function singlesmsindex(Request $request) {

		$activeTab = "sms-send";
		$tab = "single";
		$smstypetab = "singlesmstab";

		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'smstypetab', 'groups'));
	}

	public function bulksmsindex(Request $request) {

		$activeTab = "sms-send";
		$tab = "single";
		$smstypetab = "bulksmstab";

		$user = Auth::user();
		$userid = $user->id;
		$contacts = DB::table("contacts")->where('user_id', '=', $userid)->get();
		$groups = DB::table("groups")->where('userid', '=', $userid)->get();
		return view('userpanel', compact('activeTab', 'contacts', 'tab', 'smstypetab', 'groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$roles = Role::pluck('name', 'name')->all();
		return view('users.create', compact('roles'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|same:confirm-password',
			'roles' => 'required',
		]);

		$input = $request->all();
		$input['password'] = Hash::make($input['password']);

		$user = User::create($input);
		$user->assignRole($request->input('roles'));

		return redirect()->route('users.index')
			->with('success', 'User created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user = User::find($id);
		return view('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$user = User::find($id);
		$roles = Role::pluck('name', 'name')->all();
		$userRole = $user->roles->pluck('name', 'name')->all();

		return view('users.edit', compact('user', 'roles', 'userRole'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users,email,' . $id,
			'password' => 'same:confirm-password',
			'roles' => 'required',
		]);

		$input = $request->all();
		if (!empty($input['password'])) {
			$input['password'] = Hash::make($input['password']);
		} else {
			$input = array_except($input, array('password'));
		}

		$user = User::find($id);
		$user->update($input);
		DB::table('model_has_roles')->where('model_id', $id)->delete();

		$user->assignRole($request->input('roles'));

		return redirect()->route('users.index')
			->with('success', 'User updated successfully');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		User::find($id)->delete();
		return redirect()->route('users.index')
			->with('success', 'User deleted successfully');
	}

	public function createClientRegisterPage() {

		$countries = DB::table("country")->get();
		$activeTab = 'manage_client';
		return view('auth.client-register', compact('activeTab', 'countries'));

	}

	public function getCityList(Request $request) {
		$city = DB::table("city")
			->where("stateid", $request->c_id)
			->pluck("name", "id");
		return response()->json($city);
	}
	public function getStateList(Request $request) {
		$city = DB::table("state")
			->where("countryid", $request->c_id)
			->pluck("name", "id");
		return response()->json($city);
	}

	public function existusername(Request $request) {

		$id = $request->input('id');

		if ($id != '') {
			$title_exists = (count(\App\User::where('id', '!=', $id)->where('email', '=', $request->input('email'))->get()) > 0) ? false : true;
			return response()->json($title_exists);
		} else {
			$title_exists = (count(\App\User::where('email', '=', $request->input('email'))->get()) > 0) ? false : true;
			return response()->json($title_exists);
		}
	}
	/*existcontactno*/
	public function existcontactno(Request $request) {

		$id = $request->input('id');

		if ($id != '') {
			$title_exists = (count(\App\User::where('id', '!=', $id)->where('contactno', '=', $request->input('contactno'))->get()) > 0) ? false : true;
			return response()->json($title_exists);
		} else {
			$title_exists = (count(\App\User::where('contactno', '=', $request->input('contactno'))->get()) > 0) ? false : true;
			return response()->json($title_exists);
		}
	}

	public function saveClientRegister(Request $request) {
		//  echo '<pre>'; print_r($request->all()); echo '</pre>'; exit;
		$request->validate([

			'name' => 'required',
			'cname' => 'required',
			'emp_size' => 'required',
			'email' => 'required|email|max:255|unique:users,email',
			'password' => 'required',
			'contactno' => 'required|numeric|unique:users,contactno',
			'country' => 'required',
			'state' => 'required',
			'city' => 'required',

		]);

		$user = new User;

		//$user->username         =   $request->username;
		$user->name = $request->name;
		$user->cname = $request->cname;
		$user->emp_size = $request->emp_size;
		$user->email = $request->email;
		$user->password = Hash::make($request->password);
		$user->contactno = $request->contactno;
		$user->country = $request->country;
		$user->state = $request->state;
		$user->city = $request->city;
		$user->dob = $request->dob;
		$user->roleid = '2';
		$user->status = '1';
		$user->save();

		$user_id = $user->id;

		$Wallet = new Wallet;
		$Wallet->userid = $user->id;
		$Wallet->save();

		/*email code*/
		$emailtemplate = EmailTemplate::where('Label', 'welcome-mail')->first();

		$find = array('#name#');
		$replace = array($user->name);

		$subject = $emailtemplate->subject;
		$content = str_replace($find, $replace, $emailtemplate->content);

		$to = 'trunrupareliya1012@gmail.com';

		Mail::send('email', compact('content'), function ($message) use ($emailtemplate, $to) {
			$message->from($emailtemplate->fromEmail, $emailtemplate->from);
			$message->to($to);
			$message->subject($emailtemplate->subject);
		});

		if (Mail::failures()) {
			$failures[] = Mail::failures()[0];
			//Session::flash('error', 'Please Try Again...');
		} else {
			//Session::flash('success', 'Thank You For Your Message. It has been Sent...');
		}
		/*email code*/

		return redirect()->route('login')->with('user', $user)->with('success', 'Customer Registartion Successfully');

	}

	public function amyprofileView() {

		$user = Auth::user();

		//dd(Auth::user()->id,$userDoc, $bank_document, $aadhar_document, $pan_document);
		$activeTab = "profile";
		return view('users.my-profile')->with('user', $user)->with('activeTab', $activeTab);
	}

	public function aeditProfileForm(Request $request) {

		$user = \Auth::user();
		$countries = DB::table('country')->get();
		$states = DB::table('state')->get();
		$cities = DB::table('city')->get();
		// dd($user);
		$activeTab = "my_profile";
		return view('users.edit-profile', compact('user', 'countries', 'states', 'cities', 'activeTab'));
	}

	public function usereditProfileForm(Request $request) {

		$user = \Auth::user();

		// dd($user);
		$activeTab = "personal_details";
		return view('users.userprofile', compact('user', 'activeTab'));
	}

	public function aupdateProfile(Request $request, $id) {

		// dd($request->all());
		$request->validate([

			'name' => 'required',
			'cname' => 'required',
			'emp_size' => 'required',
			'country' => 'required',
			'state' => 'required',
			'city' => 'required',

		]);
		$user = \Auth::user();

		$user->name = $request->name;
		$user->cname = $request->cname;
		$user->emp_size = $request->emp_size;
		$user->country = $request->country;
		$user->state = $request->state;
		$user->city = $request->city;
		$user->dob = $request->dob;

		$user->save();

		return redirect()->route('aprofile.update')->with('user', $user)->with('success', 'Profile Updated Successfully.');
	}

	public function userupdatedetails(Request $request, $id) {

		// dd($request->all());
		$request->validate([

			'name' => 'required',
			'profession' => 'required',
			'location' => 'required',
			'dob' => 'required',

		]);
		$user = \Auth::user();

		$user->name = $request->name;
		$user->profession = $request->profession;
		$user->location = $request->location;
		$user->dob = $request->dob;

		$user->save();

		return redirect()->route('userprofile.update')->with('user', $user)->with('success_userdetails', 'Profile Updated Successfully.');
	}
}