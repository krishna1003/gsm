<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\SenderIDAPICOntroller;
use App\Models\SenderID_Details;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class UserManageSenderIDs extends Controller {
	/**
	 * Show All sender IDS
	 */
	public function showSendersId(Request $request) {
		if ($request->ajax()) {

			$apiResponse = SenderIDAPICOntroller::getSenderIDS($request);
			$response = $apiResponse->getData();

			if (($response->status == "success") && ($response->code == 200)) {
				$data = $response->data;
			}
			// $data = SenderID_Details::latest()->get();

			return Datatables::of($data)
				->addIndexColumn()
				->addColumn('user_name', function ($row) {
					$userName = geUser($row->user_uuid);
					return $userName;
				})
				->addColumn('action', function ($row) {
					$btn = '<a href="' . route('change.sender.status', [$row->sender_uuid, 'approve']) . '" class=" btn btn-info btn-sm">Approve</a>
                            <a href="' . route('change.sender.status', [$row->sender_uuid, 'rejected']) . '" class=" btn btn-danger btn-sm">Reject</a>
                            <a href="' . route('apply.new.senderids', $row->sender_uuid) . '" class="edit btn btn-primary btn-sm">Edit</a>
                            <a href="' . route('create.sender.remove', $row->sender_uuid) . '" class="remove btn btn-danger btn-sm">Remove</a>
                            ';
					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
		}
		//return view('sender-ids.show-ids');
		$user = Auth::user();
		$user_id = $user->id;

		$sendridData = SenderID_Details::WHERE('user_uuid', '=', $user_id)->get();

		$activeTab = 'user_sender_ids';
		return view('usersenderid', compact('activeTab', 'sendridData'));
	}

	public function applyForSenderID(Request $request, $senderid = null) {
		if ($request->isMethod('get') && $senderid != null) {
			$senderID = SenderID_Details::where('sender_uuid', $senderid)->first();

			if ($senderID == null) {
				return redirect()->back()->with('error', 'Invalid Route Way');
			}

			return view('sender-ids.create-senderid')->with('senderData', $senderID);
		}

		return view('sender-ids.create-senderid');
	}

	public function storeSenderIDS(Request $request, $uuid = null) {

		if ($request->isMethod('post')) {

			// dd($request->all());
			$user = Auth::user();
			$user_type = 'user';
			$status = 'apply';
			$request->request->add(['user_type' => $user_type, 'user_id' => $user->id, 'status' => $status]);

			if ($uuid != null) {
				$request->request->add(['sender_uuid' => $uuid]);
			}

			$apiResponse = SenderIDAPICOntroller::storeSenderIDs($request);

			$response = $apiResponse->getData();

			dd($response);

			if (($response->status == "success") && ($response->code == 200)) {
				$data = $response->data;
				return \redirect()->back()->with('success', 'apply for Sender ID Success');
			} else {
				return \redirect()->back()->with('error', 'Invalid Form Details');
			}

		} else {
			return view('layouts.error-404');
		}

	}

	public function UserstoreSenderIDS(Request $request, $uuid = null) {

		if ($request->isMethod('post')) {

			// dd($request->all());
			$user = Auth::user();
			$user_type = 'user';
			$status = 'apply';
			$request->request->add(['user_type' => $user_type, 'user_id' => $user->id, 'status' => $status]);

			if ($uuid != null) {
				$request->request->add(['sender_uuid' => $uuid]);
			}

			$apiResponse = SenderIDAPICOntroller::UserstoreSenderIDs($request);

			$response = $apiResponse->getData();

			// dd($response);

			if (($response->status == "success") && ($response->code == 200)) {
				$data = $response->data;
				return \redirect()->back()->with('success', 'Your Sender ID created successfully. Please wait to approve by admin');
			} else {
				return \redirect()->back()->with('error', 'Invalid Form Details');
			}

		} else {
			return view('layouts.error-404');
		}

	}

	public function removeSenderID(Request $request, $uuid = null) {
		if ($uuid) {

			$senderID = SenderID_Details::where('sender_uuid', $uuid)->delete();
			return redirect()->back()->with('success', 'Removed Sender ID');
		} else {
			return view('layouts.error-404');
		}
	}

	public function changeSenderIDStatus(Request $request, $uuid = null, $status = null) {
		// dd($request->all(), $uuid, $status);

		if ($uuid != null && $status != null) {

			if ($status == "apply") {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'apply']);
			} elseif ($status == 'pending') {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'pending']);
			} elseif ($status == 'process') {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'process']);
			} elseif ($status == 'approve') {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'approve']);
			} elseif ($status == 'rejected') {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'rejected']);
			} else {
				SenderID_Details::where('sender_uuid', $uuid)->update(['status' => 'rejected']);
			}

			return redirect()->back()->with('succes', 'Update Status Sender ID');
		} else {
			return view('layouts.error-404');
		}
	}

	public function allUserSms(Request $request) {

		$columns = array(
			0 => 'sender_uuid',
			1 => ' senderid',
			2 => 'status',

		);

		$totalData = SenderID_Details::count();

		$totalFiltered = $totalData;
		$user = Auth::user();
		$user_id = $user->id;

		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');

		if (empty($request->input('search.value'))) {
			$posts = SenderID_Details::WHERE('user_uuid', $user_id)
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();
		} else {
			$search = $request->input('search.value');

			$posts = SenderID_Details::WHERE('user_uuid', '=', $user_id)
				->where('senderid', 'LIKE', "%{$search}%")
				->orWhere('status', 'LIKE', "%{$search}%")
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			/*$posts =     DB::select( DB::raw(" SELECT s.*,COUNT(*) as totalFiltered  FROM sms_deliveries s WHERE s.user_uuid = '$user_id' AND ( s.message LIKE '%$search%' or  s.delivery_status LIKE '%$search%' or  s.number LIKE '%$search%' )  LIMIT $start, $limit ORDER BY $order,$dir ") );*/
			/*$posts =  Product::where('id','LIKE',"%{$search}%")
				                            ->orWhere('name', 'LIKE',"%{$search}%")
				                            ->offset($start)
				                            ->limit($limit)
				                            ->orderBy($order,$dir)
			*/

			$totalFiltered = SenderID_Details::WHERE('user_uuid', '=', $user_id)
				->where('senderid', 'LIKE', "%{$search}%")
				->orWhere('status', 'LIKE', "%{$search}%")
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->count();
		}

		$data = array();
		if (!empty($posts)) {
			foreach ($posts as $post) {

				$nestedData['id'] = $post->sender_uuid;
				$nestedData['senderid'] = $post->senderid;
				$nestedData['status'] = $post->status;
				$delete = route('create.sender.remove', $post->sender_uuid);
				$edit = route('apply.new.senderids', $post->sender_uuid);

				/*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
					                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
					                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
				*/
				$data[] = $nestedData;
				// $totalFiltered = $posts->totalFiltered;
				//$totalFiltered = 50;

			}
		}

		$json_data = array(
			"draw" => intval($request->input('draw')),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);

	}
}
