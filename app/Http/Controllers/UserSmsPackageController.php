<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Payment;
use App\UserSmsPackage;
use App\Wallet;
use DB;
use Illuminate\Http\Req;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserSmsPackageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$activeTab = 'manage_usersmspackage';
		$UserSmsPackageData = UserSmsPackage::all();
		return View('user_sms_package/show', compact('UserSmsPackageData', 'activeTab'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$activeTab = 'manage_usersmspackage';
		$smspackages = DB::table("sms_package_list")->where('status', '=', "1")->get();
		$users = DB::table("users")->where('roleid', '=', "2")->get();
		return View('user_sms_package/create', compact('activeTab', 'smspackages', 'users'));
	}

	public function getSmsPackageList(Request $request) {
		$smspackage = DB::table("sms_package_list")
			->where("id", $request->c_id)
			->pluck("no_of_msg", "amount");
		return response()->json($smspackage);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$date = date('Y-m-d h:i:s');
		$user = Auth::user();

		$Payment = new Payment;
		$Payment->userid = $request->input('userid');
		$Payment->amount = $request->input('amount');
		$Payment->type = $request->input('type');
		$Payment->remark = $request->input('details');
		$Payment->status = $request->input('status');
		$Payment->created_at = $date;
		if (!$Payment->save()) {
			//Redirect error
			return redirect()->route('user_sms_package')->with('error', 'Failed to add User SMS Package');
		}

		$payment_id = $Payment->id;
		$UserSmsPackage = new UserSmsPackage;
		$UserSmsPackage->userid = $request->input('userid');
		$UserSmsPackage->smspackageid = $request->input('smspackageid');
		$UserSmsPackage->amount = $request->input('amount');
		$UserSmsPackage->no_of_msg = $request->input('no_of_msg');
		$UserSmsPackage->payment_id = $payment_id;
		$UserSmsPackage->status = 1;
		$UserSmsPackage->created_at = $date;

		if (!$UserSmsPackage->save()) {
			//Redirect error
			return redirect()->route('user_sms_package')->with('error', 'Failed to add User SMS Package');
		}

		$nomsg = DB::table('wallet')->where('userid', '=', $request->input('userid'))->get();
		if ($nomsg->count() > 0) {
			$nomsg1 = DB::table('wallet')->where('userid', '=', $request->input('userid'))->first();
			$totalmsg = $nomsg1->no_of_msg + $request->input('no_of_msg');
		} else {
			$totalmsg = $request->input('no_of_msg');
		}

		//dd($totalmsg);

		$wallet = Wallet::updateOrCreate([
			'userid' => $request->input('userid'),
		], [
			'no_of_msg' => $totalmsg,

		]);

		//Redirect success
		return redirect()->route('user_sms_package')->with('success', 'User SMS Package added successfully.');

	}

	/*public function changeSmsPackageStatus(Request $request) {
		        $SmsPackage_id = $request->SmsPackage_id;
		        $status = ($request->status == 1 ? 0 : 1);
		        if(!empty($brand_id)) {
		            $SmsPackage = SmsPackage::where('id',$SmsPackage_id)
		                                ->update(['status' => $status]);

		            if($SmsPackage) {
		                echo "success";
		            } else {
		                echo "fail";
		            }

		        }
	*/

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Banner  $banner
	 * @return \Illuminate\Http\Response
	 */
	public function show(Banner $banner) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	/*public function edit($id)
		    {
		        $activeTab = 'manage_usersmspackage';
		        $UserSmsPackage = SmsPackage::find($id);
		        return View('user_sms_package/create',compact('UserSmsPackage','activeTab'));
	*/

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	/*public function update(Request $request, $id)
		    {
		        try {
		            $date = date('Y-m-d h:i:s');
		            $SmsPackage = SmsPackage::find($id);
		            $SmsPackage->name = $request->input('name');
		            $SmsPackage->details = $request->input('details');
		            $SmsPackage->no_of_msg = $request->input('no_of_msg');
		            $SmsPackage->amount = $request->input('amount');
		            $SmsPackage->status = $request->input('status') != "" ? $request->input('status') : 0;
		            $SmsPackage->updated_at = $date;

		            if($SmsPackage->save()) {
		                //Redirect success
		                return redirect()->route('sms_package')->with('success','Sms Package updated successfully.');
		            }

		            //Redirect error
		            return redirect()->route('sms_package')->with('error','Failed to Sms Package.');

		        } catch (Exception $e) {
		            Log::error("Failed to update Sms Package.");
		        }
	*/

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

	}

	public function changeUserSmsPackageStatus_datatable(Request $request, $id) {
		$sms_package = UserSmsPackage::find($id);

		if ($sms_package->status == 1) {
			$sms_package->status = 0;

			$nomsg1 = DB::table('wallet')->where('userid', '=', $sms_package->userid)->first();
			$totalmsg = $nomsg1->no_of_msg - $sms_package->no_of_msg;
			$wallet = Wallet::where('userid', $sms_package->userid)->update(['no_of_msg' => $totalmsg]);

		} elseif ($sms_package->status == 0) {
			$sms_package->status = 1;

			$nomsg1 = DB::table('wallet')->where('userid', '=', $sms_package->userid)->first();
			$totalmsg = $nomsg1->no_of_msg + $sms_package->no_of_msg;
			$wallet = Wallet::where('userid', $sms_package->userid)->update(['no_of_msg' => $totalmsg]);
		}
		$sms_package->save();

		return redirect()->route('user_sms_package')->with('success', 'User SMS Package Status Updated');

	}

	public function allUserSmsPackage(Request $request) {

		$columns = array(
			0 => 'id',
			1 => 'username',
			2 => 'mobile_number',
			3 => 'amount',
			4 => 'no_of_msg',
			5 => 'paymenttype',
			6 => 'paymentstatus',
			7 => 'status',
			8 => 'action',
		);

		$totalData = UserSmsPackage::count();
		$user = Auth::user();

		$totalFiltered = $totalData;

		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');

		if (empty($request->input('search.value'))) {
			$posts = UserSmsPackage::join('users', 'user_sms_package.userid', '=', 'users.id')->join('sms_package_list', 'user_sms_package.smspackageid', '=', 'sms_package_list.id')->join('payment', 'user_sms_package.payment_id', '=', 'payment.id')->select('user_sms_package.*', 'users.name as user_name', 'users.contactno as user_mob', 'sms_package_list.name as sms_package_list_name', 'payment.type as payment_type', 'payment.status as payment_status')->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();
		} else {
			$search = $request->input('search.value');

			$posts = UserSmsPackage::join('users', 'user_sms_package.userid', '=', 'users.id')->join('sms_package_list', 'user_sms_package.smspackageid', '=', 'sms_package_list.id')->join('payment', 'user_sms_package.payment_id', '=', 'payment.id')->select('user_sms_package.*', 'users.name as user_name', 'users.contactno as user_mob', 'sms_package_list.name as sms_package_list_name', 'payment.type as payment_type', 'payment.status as payment_status')
				->where('users.name', 'LIKE', "%{$search}%")
				->orWhere('users.contactno', 'LIKE', "%{$search}%")
				->orWhere('sms_package_list.name', 'LIKE', "%{$search}%")
				->orWhere('payment.type', 'LIKE', "%{$search}%")
				->orWhere('payment.status', 'LIKE', "%{$search}%")
				->orWhere('user_sms_package.no_of_msg', 'LIKE', "%{$search}%")
				->orWhere('user_sms_package.amount', 'LIKE', "%{$search}%")
				->offset($start)
				->limit($limit)
				->orderBy($order, $dir)
				->get();

			$totalFiltered = UserSmsPackage::join('users', 'user_sms_package.userid', '=', 'users.id')->join('sms_package_list', 'user_sms_package.smspackageid', '=', 'sms_package_list.id')->join('payment', 'user_sms_package.payment_id', '=', 'payment.id')->select('user_sms_package.*', 'users.name as user_name', 'users.contactno as user_mob', 'sms_package_list.name as sms_package_list_name', 'payment.type as payment_type', 'payment.status as payment_status')
				->where('users.name', 'LIKE', "%{$search}%")
				->orWhere('users.contactno', 'LIKE', "%{$search}%")
				->orWhere('sms_package_list.name', 'LIKE', "%{$search}%")
				->orWhere('payment.type', 'LIKE', "%{$search}%")
				->orWhere('payment.status', 'LIKE', "%{$search}%")
				->orWhere('user_sms_package.no_of_msg', 'LIKE', "%{$search}%")
				->orWhere('user_sms_package.amount', 'LIKE', "%{$search}%")
				->count();
		}

		$data = array();
		if (!empty($posts)) {
			foreach ($posts as $post) {

				$edit = route('user_sms_package.edit', $post->id);
				$delete = route('user_sms_package.delete', $post->id);

				$st = route('change.user_sms_package_datatable.status', $post->id);

				$nestedData['id'] = $post->id;
				$nestedData['name'] = $post->user_name;
				$nestedData['mobile_number'] = $post->user_mob;
				$nestedData['no_of_msg'] = $post->no_of_msg;
				$nestedData['amount'] = $post->amount;
				$nestedData['sms_package_list_name'] = $post->sms_package_list_name;
				$nestedData['payment_type'] = $post->payment_type;
				$nestedData['payment_status'] = $post->payment_status;

				if ($post->status == '1') {
					$nestedData['status'] = '<span class="badge badge-success">Active</span>';
				} else {
					$nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
				}

				$options = "";

				if ($post->status == '1') {

					$options = '<a href="' . $st . '" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause"> <i class="fa fa-power-off"></i> </a>&nbsp;';
				} else {
					// $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

					$options = '<a href="' . $st . '" class="btn btn-rounded btn-sm btn-success changeBrandStatus" data-toggle="tooltip" title="Activate"> <i class="fa fa-play"></i> </a>&nbsp;';
				}

				$options .= '<a href="' . $delete . '" class="btn btn-rounded btn-sm btn-danger" data-toggle="tooltip" title="Delete Package"><i class="fa fa-trash"></i></a>';

				$nestedData['options'] = $options;

				/*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
					                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
					                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
				*/
				$data[] = $nestedData;

			}
		}

		$json_data = array(
			"draw" => intval($request->input('draw')),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);

	}

	public function delete1($id) {
		$activeTab = 'manage_usersmspackage';
		$SmsPackage = UserSmsPackage::find($id);
		$payment = Payment::find($SmsPackage->payment_id);

		$nomsg1 = DB::table('wallet')->where('userid', '=', $SmsPackage->userid)->first();
		$totalmsg = $nomsg1->no_of_msg - $SmsPackage->no_of_msg;
		$wallet = Wallet::where('userid', $SmsPackage->userid)->update(['no_of_msg' => $totalmsg]);

		$payment->delete();
		$SmsPackage->delete();

		return redirect()->route('user_sms_package')->with('success', 'User SmsPackage Deleted Successfully.');
	}
}
