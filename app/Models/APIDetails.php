<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class APIDetails extends Model
{
    use SoftDeletes;

    protected $table = "api_details";
    protected $primaryKey = "uuid";
    public $incrementing = false;

}
