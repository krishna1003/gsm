<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactGroup extends Model
{
    use SoftDeletes;

    public function userGroup() {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
