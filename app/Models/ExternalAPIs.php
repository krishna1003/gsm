<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ExternalAPIs extends Model
{
    use SoftDeletes;

    protected $table = "external_apis";
    protected $primaryKey = "uuid";
    public $incrementing = false;
}
