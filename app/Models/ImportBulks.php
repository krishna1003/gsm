<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ImportBulks implements ToCollection,WithHeadingRow
{
    public $rowData;

    public function collection(Collection $rows)
    {
        // dd($rows[0]['callnumber']);
        $this->rowData = $rows;
        return $rows;
    }

    // headingRow function is use for specific row heading in your xls file
    public function headingRow(): int
    {
        return 1;
    }

}
