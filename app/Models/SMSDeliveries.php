<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SMSDeliveries extends Model
{
    protected $table = 'sms_deliveries';
    protected $primaryKey = "deliveries_uuid";
    public $incrementing = false;


    public function getAPIDetails()
    {
        return $this->hasOne('App\Models\APIDetails', 'uuid', 'api_uuid');
    }

    public function getUserDetails()
    {
        return $this->hasOne('App\User', 'id', 'user_uuid');
    }

    public function getClientDetails()
    {
        return $this->hasOne('App\User', 'id', 'client_uuid');
    }

    public function getCompanyDetails()
    {
        return $this->hasOne('App\Models\ExternalAPIs', 'uuid', 'company_uuid');
    }

}
