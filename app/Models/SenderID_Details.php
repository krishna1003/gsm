<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SenderID_Details extends Model
{
    use SoftDeletes;

    protected $table = "senderid_details";
    protected $primaryKey = "sender_uuid";
    public $incrementing = false;


}
