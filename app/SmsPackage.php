<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsPackage extends Model
{
    protected $table = "sms_package_list";
}
