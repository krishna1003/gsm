<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSmsPackage extends Model
{
    protected $table = "user_sms_package";
}
