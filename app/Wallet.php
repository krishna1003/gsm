<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = "wallet";

    protected $fillable = ['userid','amount','no_of_msg'];
}
