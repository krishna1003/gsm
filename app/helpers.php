<?php

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\APIDetails;
use App\Models\ExternalAPIs;


    function imploadValue($types){
        $strTypes = implode(",", $types);
        return $strTypes;
    }

    function explodeValue($types){
        $strTypes = explode(",", $types);
        return $strTypes;
    }

    function uuid() {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    function allUsers(){
        return User::all();
    }

    function geUser($id) {
        $user = User::select('name')->where('id', $id)->first();
         return $user->name;
    }

    function getAPITitle($uuid) {
        $api = APIDetails::select('title')->where('uuid', $uuid)->first();
        return $api->title;
    }

    function getCompany($uuid) {
        $company = ExternalAPIs::select('comapny_name')->where('uuid', $uuid)->first();
        return $company->comapny_name;
    }

    function resallersUsers(){
        return User::all();
    }

    function getCampignName()
    {
        $dateObj = Carbon::now();
        return "Camp_".$dateObj->format('jS-F-Y h:i:s A');
    }

    function sendGetSMS($request)
    {

        Log::info("\n\n".  $request->url. "\n\n ");
        
        $client = new Client();
        $response   =   $client->request($request->method, $request->url);
        $statusCode =   $response->getStatusCode();
        $body       =   $response->getBody()->getContents();
        $protocol   =   $response->getProtocolVersion();
        $phrase     =   $response->getReasonPhrase();

        if($statusCode == 200 && $phrase == "OK") {
            return response()->json(['status' =>  'success', 'code' => $statusCode, 'data' => $body]);
        }else{
            return response()->json(['status' =>  'fail', 'code' => $statusCode, 'data' => $body]);
        }

    }

    function sendPostSMS($request) {

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $request->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request->postParameters);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = curl_exec($ch);
        curl_close($ch);
        // echo $response.PHP_EOL;

        // return $response;
        // $body = explode("/", $response);
        // dd($body, $httpcode);

        if($httpcode == 0) {
            return response()->json(['status' =>  'success', 'code' => 200, 'data' => $response]);
        }else{
            return response()->json(['status' =>  'fail', 'code' => 201, 'data' => $response]);
        }

    }

    function arrayToString($str) {
        return explode(",",substr($str, 1, -1));
    }

    function checkString($string, $prefix) {
        return substr($string, 0, strlen($prefix)) == $prefix;
    }

    function isJSON($string) {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    function getRole() {
        
        $role = DB::table('roles')->where('id', Auth::user()->roleid)->first();
        if(Auth::user()->roleid == 0){
            return 'Admin';
        }else{
        return $role->name;
        }
    }

    function get_roles() {
        return DB::table('roles')->get();
    }

    function getCountry() {
        $country = DB::table('country')->select('name')->where("id", Auth::user()->country)->first();
        
       if($country != ""){
        return $country->name;
    }
    }

    function getWalletMessagge() {
        $wallet = DB::table('wallet')->select('no_of_msg')->where("userid", Auth::user()->id)->first();
        
       if($wallet != ""){
        return $wallet->no_of_msg;
    }else
    {
        return '0';
    }
    }

    function getState() {
        $state = DB::table('state')->select('name')->where("id", Auth::user()->state)->first();
        if($state != ""){
        return $state->name;
    }
       // return $state->name;
    }

    function get_activation_date() {
        return date('m M Y',strtotime(Auth::user()->created_at));
    }

    function getCity() {
        $city = DB::table('city')->select('name')->where("id", Auth::user()->city)->first();
       if($city != ""){
        return $city->name;
    }
    }
