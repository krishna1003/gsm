<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAPIDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_p_i_details', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('customer_id');
            $table->string('title');
            $table->string('api_url');
            $table->string('api_key')->nullable();
            $table->string('api_token')->nullable();
            $table->string('api_password')->nullable();
            $table->string('purchase')->nullable();
            $table->string('expired')->nullable();
            $table->enum('status', [true, false]);
            $table->text('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_p_i_details');
    }
}
