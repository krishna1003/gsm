<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSMSDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_deliveries', function (Blueprint $table) {
            $table->uuid('deliveries_uuid')->primary();
            $table->string('user_uuid')->nullable();
            $table->string('company_uuid')->nullable();
            $table->string('api_uuid')->nullable();
            $table->string('client_uuid')->nullable();
            $table->string('number')->nullable();
            $table->text('message')->nullable();
            $table->enum('delivery_status', ['send', 'reject', 'waiting']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_m_s_deliveries');
    }
}
