<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulkfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulkfiles', function (Blueprint $table) {
            $table->uuid('bulkfile_uuid')->primary();
            $table->string('user_uuid');
            $table->string('filename');
            $table->string('company_uuid')->nullable();
            $table->string('api_uuid')->nullable();
            $table->string('client_uuid')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulkfiles');
    }
}
