<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSenderIDDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senderid_details', function (Blueprint $table) {
            $table->uuid('sender_uuid')->primary();
            $table->string('user_uuid');
            $table->string('user_type');
            $table->string('senderid');
            $table->enum('status', ['apply', 'pending', 'process' , 'approve', 'rejected']);
            $table->string('type');
            $table->string('purpose');
            $table->string('approved_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sender_i_d__details');
    }
}
