<?php

use Illuminate\Database\Seeder;
use App\Models\Contact;
use App\Models\ContactGroup;
use Illuminate\Support\Str;


class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($x = 0; $x <= 50; $x++) {

            // $user = Contact::create([
            //     'id'    => Str::uuid(),
            //     'name' => str_random(7),
            //     'email' => str_random(5).'@gmail.com',
            //     'contact' => (rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9)),
            //     'user_id' => 2,
            //     'user_type' => 'user'
            // ]);

            ContactGroup::create([
                'id'    => Str::uuid(),
                'name' => str_random(7),
                'created_by' => rand(3,6)
            ]);

        }

    }
}
