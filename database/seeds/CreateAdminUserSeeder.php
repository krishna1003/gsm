<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'sale',
        	'email' => 'sale@gmail.com',
        	'password' => bcrypt('sale@123')
        ]);

        // $role = Role::create(['name' => 'Admin']);
        // $role = Role::create(['name' => 'reseller']);
        $role = Role::create(['name' => 'user']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
