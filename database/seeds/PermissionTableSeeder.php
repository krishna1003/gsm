<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'product-list',
           'product-create',
           'product-edit',
           'product-delete',

           'contact-list',
           'contact-create',
           'contact-edit',
           'contact-delete',

           'group-contact-list',
           'group-contact-create',
           'group-contact-edit',
           'group-contact-delete',

           'sms-master-report',
           'sms-report',
           'client-sms-report',
           'reseller-sms-report',

           'send-sms',
           'bulk-send-sms',
           'group-send-sms',

           'test-api',
           'client-api',

           'user-setting',
           'reseller-setting',
           'master-setting',

        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
