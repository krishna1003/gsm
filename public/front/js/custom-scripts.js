$(document).on('click', 'a.view-user', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        dataType: 'html',
        success: function (result) {
            if (result != "") {
                $('#view_user_modal')
                    .html(result)
                    .modal('show');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Sorry!',
                    text: 'No upline details found.'
                })
            }
            //__currency_convert_recursively($('#view_product_modal'));
        },
    });
});

$(document).on('click', 'a.view-user-upline', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        dataType: 'html',
        success: function (result) {
            if (result != "") {
                $('#view_user_upline_modal')
                    .html(result)
                    .modal('show');
               /* $('#UplineTable').DataTable({
                    scrollY: '60vh',
                    "scrollX": true
                });*/
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Sorry!',
                    text: 'No upline details found.'
                })
            }
            //__currency_convert_recursively($('#view_product_modal'));
        },
    });
});