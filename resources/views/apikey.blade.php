@extends('front_layouts.app')



@section('content')

<div class="bg-white apikey-sec">
            <div class="border-bottom p-4">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <h2 class="heading3 pb-0">Get API and secret Key</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-lg-6 offset-lg-3 mt-4">
                    <form class="mb-2 row no-gutter">
                        <div class="col-lg-8">
                            <input id="apikey" type="text" class="form-control rounded-0 copyvalue" value="JOPF5H0EK74MNEDLXNK1ZY92YV6YLSC8" placeholder="Copy API Key">
                        </div>
                        <div class="col-lg-4">
                            <a class="btn btn-block copy-btn1 secondary-btn text-white mb-2 rounded-0 align-items-center justify-content-center d-flex" onclick="myFunction1()" onmouseout="outFunc()"><span>Copy API Key</span></a>
                        </div>
                    </form>
                    <form class="mb-2 row no-gutter">
                        <div class="col-lg-8">
                            <input id="secretkey" type="text" class="form-control rounded-0 copyvalue" value="T9DCNTM9PO5UU5YC" placeholder="Copy Secret Key">
                        </div>
                        <div class="col-lg-4">
                            <a class="btn btn-block copy-btn2 secondary-btn text-white mb-2 rounded-0 align-items-center justify-content-center d-flex" onclick="myFunction2()" onmouseout="outFunc()"><span>Copy Secret Key</span></a>
                        </div>
                    </form>
                </div>
                <div class="col-lg-10 offset-lg-1 mt-3">
                    <p class="theme-color">
                        Create Campaign API Details
                    </p>
                    <table class="table table-striped table-responsive mt-2 mb-5">
                        <thead>
                            <tr>
                                <th class="text-center">Parameter name</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th class="text-center">Mandatory</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                            <tr>
                                <td class="text-center">apikey</td>
                                <td><span class="font-weight-500">apikey</span> is a unique identifier provided for verifying user.</td>
                                <td>Alphanumeric(32)</td>
                                <td class="text-center">Yes</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="theme-color">Campaign request</p>
                    <div class="api-code-sec p-3 mt-2 mb-5">
                        <p class="text-muted mb-2">// required imports</p>
                        <p class="mb-2"><code>import</code> java.io.BufferedReader;</p>
                        <p class="mb-2"><code>import</code> java.io.DataOutputStream;</p>
                        <p class="mb-2"><code>import</code> java.io.InputStreamReader;</p>
                        <p class="mb-2"><code>import</code> java.net.HttpURLConnection;</p>
                        <p class="mb-2"><code>import</code> java.net.URL;</p>
                        <p class="mb-2"><code>import</code> org.json.JSONObject;</p>
                    </div>
                </div>
            </div>
        </div>

@endsection



@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/moment.js')}}"></script>
    <script src="{{asset('front2/js/main.js')}}"></script>
    <script src="{{asset('front2/js/plugin/bootstrap-datetimepicker.min.js')}}"></script>
    
    
    <script>
        function myFunction1() {
            var copyText = document.getElementById("apikey");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
        }
        function myFunction2() {
            var copyText2 = document.getElementById("secretkey");
            copyText2.select();
            copyText2.setSelectionRange(0, 99999);
            document.execCommand("copy");
        }
        $(".copy-btn1").click(function () {
            $(this).append("Copied");
            $(".copy-btn1 span").remove();
            $('this').attr("disabled", false);  
        });
        $(".copy-btn2").click(function () {
            $(this).append("Copied");
            $(".copy-btn2 span").remove();
        });
    </script>


@endsection