@extends('front_layouts.app')

@section('css')

@endsection

@section('content')

<div class="container">
            <div class="row no-gutter align-items-center">
                <div class="col-lg-6 pt-3 pb-3 pt-lg-5 pb-lg-5 d-none d-lg-block">
                    <img id="loginbg1" class="position-relative" src="{{asset('front2/images/login/vector3.png')}}" />
                    <img id="loginbg2" class="login-bg position-absolute" src="{{asset('front2/images/login/vector4.png')}}" />
                </div>
                <div class="col-lg-1 pt-3 pb-3 pt-lg-5 pb-lg-5"></div>
                <div class="col-lg-5 pt-3 pb-3 pt-lg-5 pb-lg-5">
                    <div class="login-sec">
                        <h1 class="title-text text-center mb-4">Sign Up</h1>
                        <div id="grad1">
                            <div class="px-0 pb-0 mb-3">
                                <form id="msform" class="msform123"  method="post"  action="{{ route('client.request.register') }}" >
                                    @csrf
                                    <!-- fieldsets -->
                                    @if(!isset($user))
                                    <fieldset>
                                        <div class="form-card">
                                            <div class="custom-control mb-5">

                                                <label >Enter Full Name</label>

                                                <input type="text" class="form-control"  id="name" name="name" placeholder="Enter your fullname here..." required="required" />
                                                @error('name')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror

                                            </div>

                                            <div class="custom-control mb-5">


                                                <label>Enter Mobile Number</label>

                                                <input class="form-control" id="contactno" type="text" maxlength="12" minlength="10" name="contactno" placeholder="Enter your Mobile Number here..." required="" />
                                                @error('contactno')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror
                                            </div>

                                            <div class="custom-control mb-4">



                                                <label>Enter Password</label>

                                                <input class="form-control" id="password" type="password" name="password" placeholder="Enter your password here..." required=""/>
                                                @error('password')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror
                                            </div>

                                            <div class="row small-gutter">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-select mb-5">


                                                        <label>Select Country</label>
                                                        <select class="form-control pl-2 pr-2" id="country" name="country" required="">
                                                            <option value="">Choose Country</option>
                                                            @foreach ($countries as $country)
                                                            <option value="{{$country->id}}" {{ isset($user) ? (($user->country == $country->id) ? 'selected' : '') : '' }}> {{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('country')
                                                        <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                        @enderror

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-select mb-5">

                                                        <label>Select State</label>

                                                        <select class="form-control pl-2 pr-2" id="state" name="state" required="">
                                                            <option value="">Choose State</option>
                                                            @if(isset($user))
                                                            @foreach ($states as $state)
                                                            <option value="{{ $state->id }}" {{ isset($user) ? (($user->city == $state->id) ? 'selected' : '') : '' }}>{{ $state->name }}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        @error('state')
                                                        <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                        @enderror

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-select mb-5">

                                                        <label>Select City</label>

                                                        <select class="form-control pl-2 pr-2" id="city" name="city" required="">
                                                            <option value="">Choose City</option>
                                                            @if(isset($user))
                                                            @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}" {{ isset($user) ? (($user->city == $city->id) ? 'selected' : '') : '' }}>{{ $city->name }}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        @error('city')
                                                        <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                        @enderror

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" id="next" class="next btn theme-btn btn-block" value="Next Step">Next</button>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <div class="custom-control mb-5">


                                                <label>Enter Company Name</label>
                                                <input class="form-control" id="cname" type="text" name="cname" placeholder="Enter company name here..." required="" />
                                                @error('cname')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror

                                            </div>
                                            <div class="custom-control mb-5">

                                                <label>Enter Company Size</label>

                                                <input class="form-control" id="emp_size" type="text" name="emp_size"  placeholder="Select company size" required="" />
                                                @error('emp_size')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror
                                            </div>
                                            <div class="custom-control mb-5">

                                                <label>Enter Email</label>
                                                <input class="form-control" id="email" type="email" name="email" placeholder="Enter your email here..." required="" />
                                                @error('email')
                                                <span style="color: red;padding: 5px;font-size: 12px;">{{ $message }}</span>

                                                @enderror
                                            </div>

                                            <div >
                                               <input type="hidden" name="previous" class="previous" value="Previous" />
                                            </div>



                                        </div>



                                        <input type="submit" name="next" class="next btn theme-btn action-button btn-block" value="Create an account" />
                                    </fieldset>
                                    @else
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="p-5 text-center heading1">Success</h2>
                                        </div>
                                    </fieldset>
                                    @endif
                                </form>
                                <!-- progressbar -->
                                <ul id="progressbar" class="text-center mt-4">
                                    <li class="active rounded previous1" name="previous1"  id="account"></li>
                                    <li id="personal" class="rounded"></li>
                                </ul>
                            </div>
                        </div>
                        <p class="text-center pt-3">Already have an Account?  <a href="{{ route('login') }}" class="theme-color">Login</a></p>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/mouse.parallax.js')}}"></script>
    <script src="{{asset('front/js/jquery.validate.min.js')}}"></script>
     <script src="{{asset('front/js/additional-methods.min.js')}}"></script>
     <script>
        jQuery(document).ready(function () {
             var viewPortWidth = jQuery(window).width();
            if (viewPortWidth > 1024) {
                $('#loginbg1').mouseParallax({ moveFactor: 7 });
                $('#loginbg2').mouseParallax({ moveFactor: 2 });
            }
        });




        $(document).ready(function () {

            $('#contactno').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });


        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space not allowed");

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;

            $(".next").click(function () {

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                console.log(current_fs);
                console.log(previous_fs);

                var form = $("#msform");
        form.validate({
            messages: {
               email: {
                    remote: 'email already taken.',
                },
                contactno: {
                    remote: 'contact no already taken.',
                },
                 city: "select",
                 country: "select",
                 state: "select",
                 emp_size: "select",
            },
            rules: {

                email: {
                    required: true,
                    Validemail: true,
                    remote: {
                        type: 'post',
                        url: "{{ URL('existusername') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }

                },
                cname: {
                    required: true
                },
                name: {
                    required: true
                },
                emp_size: {
                    required: true
                },

                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                contactno: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                    remote: {
                        type: 'post',
                        url: "{{ URL('existcontactno') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }
                },

                country: {
                    required: true
                },

                city: {
                    required: true
                },
                state: {
                    required: true
                },

            }
        });

        console.log(form.valid());
        if (form.valid() == true){
            //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({ opacity: 0 }, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({ 'opacity': opacity });
                    },
                    duration: 600
                });
        }



            });

            $(".previous").click(function () {

                current_fs = $(this).parent().parent().parent();
                previous_fs = $(this).parent().parent().parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                $("#progressbar li").eq($("fieldset").index(previous_fs)).addClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({ opacity: 0 }, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({ 'opacity': opacity });
                    },
                    duration: 600
                });
            });

            $(".previous1").click(function () {



                current_fs = $('.previous').parent().parent().parent();
                previous_fs = $('.previous').parent().parent().parent().prev();
                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                $("#progressbar li").eq($("fieldset").index(previous_fs)).addClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({ opacity: 0 }, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({ 'opacity': opacity });
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function () {
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });

            $(".submit").click(function () {
                return false;
            })

        });
    </script>

    <script>



    $(document).ready(function() {

        $('#contactno').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });


        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space not allowed");

        $('.msform123').validate({
            errorPlacement: function(error, element) {
                error.insertAfter(element)
                error.addClass('text-danger');
            },
            messages: {
               email: {
                    remote: 'email already taken.',
                },
                contactno: {
                    remote: 'contact no already taken.',
                },
                city: "select city",
                country: "select country",
                state: "select state",
            },
            rules: {

                email: {
                    required: true,
                    Validemail: true,
                    remote: {
                        type: 'post',
                        url: "{{ URL('existusername') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }

                },
                cname: {
                    required: true
                },
                name: {
                    required: true
                },
                emp_size: {
                    required: true
                },

                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                contactno: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                    remote: {
                        type: 'post',
                        url: "{{ URL('existcontactno') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }
                },

                country: {
                    required: true
                },

                city: {
                    required: true
                },
                state: {
                    required: true
                },

            }
        });



$('#country').change(function() {
    //alert('1');
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-state-list1')}}?c_id=" + countryID,
                    success: function(res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $.each(res, function(key, value) {
                                $("#state").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        } else {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#state").append('<option value="">Choose State</option>');
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }
        });

        $('#state').on('change', function() {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list1')}}?c_id=" + stateID,
                    success: function(res) {
                        //console.log(res);
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }

        });
        });


</script>
@endsection