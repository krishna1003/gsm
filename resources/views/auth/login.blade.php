@extends('front_layouts.app')

@section('css')

@endsection

@section('content')

<div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 pt-3 mt-5 pb-3 pt-lg-5 pb-lg-5 d-none d-lg-block">
                    <img id="loginbg1" class="login-bg position-relative" src="{{asset('front2/images/login/vector1.png')}}" />
                    <img id="loginbg2" class="login-bg position-absolute" src="{{asset('front2/images/login/vector2.png')}}" />
                </div>
                <div class="col-lg-1 pt-3 pb-3 pt-lg-5 pb-lg-5"></div>
                <div class="col-lg-5 pt-3 pb-3 pt-lg-5 pb-lg-5">

                    <div class="login-sec">
                        <h1 class="title-text text-center mb-5">Login</h1>
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <form method="POST" action="{{ route('login') }}">
                          @csrf
                          @error('email')
                          <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                            <div class="custom-control mb-5">
                                <label>Enter Mobile / Email</label>

                                <label>Enter Mobile / Email</label>

                                <input type="text" class="form-control" name="email"  placeholder="Enter your mobile / email here..." required />
                            </div>
                            <div class="custom-control mb-5">
                                <label>Enter Password</label>
                                <input class="form-control" placeholder="Enter your password here..." type="password" id="password" name="password" required />
                                <a href="forgot.html" class="position-absolute forgot-pass">Forgot?</a>
                            </div>
                            <div class="custom-control">
                                <button type="submit" class="btn theme-btn btn-block">Login</button>
                            </div>
                        </form>
                        <p class="text-center pt-3">Don’t have an Account? <a href="{{ route('ClientRegister') }}" class="theme-color">Register</a></p>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/mouse.parallax.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            $("#header").load("header.html");
            $("#footer").load("footer.html");
            var viewPortWidth = jQuery(window).width();
            if (viewPortWidth > 1024) {
                $('#loginbg1').mouseParallax({ moveFactor: 7 });
                $('#loginbg2').mouseParallax({ moveFactor: 2 });
            }
        });
    </script>
@endsection