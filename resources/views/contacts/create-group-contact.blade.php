@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Contacts Group</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('contact.group.show') }}">Contacts</a></li>
                <li class="breadcrumb-item active">Create Group </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- form start -->
                            <form role="form" method="POST" action="{{ route('contact.group.create.request', isset($contactGroup) ? $contactGroup->id : '') }}">
                                @csrf
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="name">Group Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $contactGroup->name ?? ''}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="user">User</label>
                                        <select class="form-control" id="user" name="user">
                                            <option value="">--Select User--</option>
                                            @foreach (allUsers() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        {{ isset($contactGroup) ? 'Update' : 'Submit'}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });
</script>


@endsection
