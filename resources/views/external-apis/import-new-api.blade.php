@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Import Third Part APIs</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">External API</a></li>
                <li class="breadcrumb-item active">Import New APIs</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- form start -->
                            <form role="form" method="POST" action="{{ route('externalapi.create.request', isset($contact) ? $contact->id : '') }}">
                                @csrf
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="comapny_name">Comapny Name</label>
                                        <input type="text" class="form-control" id="comapny_name" name="comapny_name" placeholder="Enter Comapny Name" value="{{ $contact->comapny_name ?? ''}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ $contact->email ?? ''}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact Number</label>
                                        <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" value="{{ $contact->contact ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control" id="location" name="location" placeholder="location" value="{{ $contact->location ?? ''}}" required>
                                    </div>

                                    <!-- API Details -->

                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ $contact->title ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="api_url">api_url</label>
                                        <textarea class="form-control" id="api_url" name="api_url" placeholder="api_url">{!! $contact->api_url ?? '' !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="api_token">api_token</label>
                                        <textarea class="form-control" id="api_token" name="api_token" placeholder="api_token">{!! $contact->api_token ?? '' !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="api_password">api_password</label>
                                        <input type="text" class="form-control" id="api_password" name="api_password" placeholder="api_password" value="{{ $contact->api_password ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="purchase">purchase</label>
                                        <input type="date" class="form-control" id="purchase" name="purchase" placeholder="purchase" value="{{ $contact->purchase ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="expired">expired</label>
                                        <input type="date" class="form-control" id="expired" name="expired" placeholder="expired" value="{{ $contact->expired ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">
                                            <input type="radio" id="active" name="status" value="0" required checked>
                                            Active
                                        </label>
                                        <label for="status">
                                            <input type="radio" id="deactive" name="status" value="1" required>
                                            Deactive
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">description</label>
                                        <textarea class="form-control" id="description" name="description" placeholder="description">{!! $contact->description ?? '' !!}</textarea>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        {{ isset($contact) ? 'Update' : 'Submit'}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });
</script>


@endsection
