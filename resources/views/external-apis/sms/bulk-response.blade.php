@extends('layouts.app')

@section('extracss')
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Send Bulk SMS</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">External API</a></li>
                <li class="breadcrumb-item active">Send Bulk SMS</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ asset('sample/sample.xlsx') }}" class="btn btn-success">Sample File Download</a>
                            </div>
                        </div>
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="alert" id="message" style="display: none"></div>
                            <!-- form start -->
                            <form role="form" id="uploadFile" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <!-- Send SMS -->
                                    <div class="form-group">
                                        <label for="bulkFile">File Upload</label>
                                        <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="bulkFile" id="bulkFile">
                                            <label class="custom-file-label" for="bulkFile">Choose file</label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </form>


                            <form role="form" id="sendSMSForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Message Channel</label>
                                        <div>
                                            <label class="radio-inline" for="promotional">
                                                <input type="radio" name="message_channel" value="promo" id="promotional" required>
                                                Promotional
                                            </label>
                                            <label class="radio-inline" for="transactional">
                                                <input type="radio" name="message_channel" value="trans" id="promotional" required>
                                                Transactional
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="campaign">Campaign</label>
                                        <input type="text" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" required readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="senderid">Sender ID</label>
                                        <select class="form-control" id="senderid" name="senderid" readonly>
                                            <option value="LCROWN" selected>LCROWN</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="unicode">Unicode : </label>
                                        <input type="checkbox" id="unicode" name="unicode">
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <label for="schedule" class="">Schedule : </label>
                                            <input type="checkbox"  class="" id="schedule" name="schedule">
                                        </div>
                                        <div class="col-md-4 shedule-date">
                                            <input type="date" class="form-control" name="schedule_date" id="schedule_date">
                                        </div>
                                    </div>
                                    <div class="form-group" id="bulkMsg">
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>


                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

     <!-- Main content -->
     <section class="content container"></section>
    <!-- /.content -->
</form>

@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>

    $(function() {

        $("#uploadFile").show();
        $("#sendSMSForm").hide();


        $('#uploadFile').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('load.bulkview.request') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(res)
                {
                    if((res.status == 'success') && (res.code == 200)) {
                        console.log("Success response :: ", res.data);
                        var responseData = res.data;
                        $('#message').css('display', 'block');
                        $('#message').addClass('alert-success');
                        $('.alert-success').delay(7000).fadeOut();
                        $('#message').html(res.msg);

                        $("#uploadFile").hide();
                        $("#sendSMSForm").show();

                        var headers = responseData.bulkdata.header;
                        var body = responseData.bulkdata.body;

                        console.log("check headers : ", headers.length);
                        if(headers.length <= '2') {
                            console.log("Correct two fiels");
                            $("#bulkMsg").html(`
                                <label for="message">Message</label>
                                <textarea class="form-control" id="message" name="message" required></textarea>
                            `);
                        }
                        // console.log("Final Check :: ", responseData.htmlview);

                        $('.container').html(responseData.htmlview);

                    }else {
                        console.log("Error response :: ", res.status +  res.code);
                        $('#message').css('display', 'block');
                        $('#message').addClass('alert-danger');
                        $('.alert-danger').delay(5000).fadeOut();
                        $('#message').html('');
                        $.each(res.msg, function( key, value ) {
                            console.log("ERROR ", value);
                            $("#message").append('<li>'+value+'</li>');
                        });
                    }

                }
            })
        });

        $('#sendSMSForm').on('submit', function(event){
            event.preventDefault();
            console.log("selected bulk Data ", selectedRows);

            if(selectedRows.length == 0) {
                alert("select sms data ");
                $('#smsMessage').show();
                $('#smsMessage').delay(5000).fadeOut();
                return false;
            }else{
                smsdata = JSON.stringify(selectedRows);;
            }

            var formData = new FormData(this);
            formData.append('filedata', smsdata);
            formData.append('fileuuid', fileuuid);

            console.log("selected Messages ::::: ", formData);

            $.ajax({
                url:"{{ route('send.bulksms.request') }}",
                method:"POST",
                data:formData,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(res) {
                    console.log("response :: ", res);
                    if((res.status == 'success') && (res.code == 200)) {
                        window.location.href = "{{ route('home') }}";
                    }
                }
            });
        });


    });


</script>




@endsection
