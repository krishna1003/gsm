@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Send Multi SMS</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">External API</a></li>
                <li class="breadcrumb-item active">Send Multi SMS</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- form start -->
                            <form role="form" method="POST" action="{{ route('send.multisms.request') }}">
                                @csrf
                                <div class="card-body">
                                    <!-- Send SMS -->
                                    <div class="form-group">
                                        <label for="contact">Contact Number *Note : (Multi Numers Between Use Coma , Operator)</label>
                                        <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="campaign">Campaign</label>
                                        <input type="text" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" required readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="senderid">Sender ID</label>
                                        <select class="form-control" id="senderid" name="senderid" readonly>
                                            <option value="WEBSMS" selected>WEBSMS</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea class="form-control" id="message" name="message" placeholder="Enter Message Here" required></textarea>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });
</script>


@endsection
