@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Preview Bulk Data</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">External API</a></li>
                <li class="breadcrumb-item active">Preview Bulk Data</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ asset('sample/sample.xlsx') }}" class="btn btn-success">Sample File Download</a>
                            </div>
                        </div>
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- form start -->
                            <form role="form" method="POST" action="{{ route('send.bulksms.request') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <!-- Send SMS -->
                                    <div class="form-group">
                                        <label for="campaign">Campaign</label>
                                        <input type="text" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" required readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="senderid">Sender ID</label>
                                        <select class="form-control" id="senderid" name="senderid" readonly>
                                            <option value="LCROWN" selected>LCROWN</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            </form>

                            <hr>
                            {{ dd($responseData) }}

                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });
</script>


@endsection
