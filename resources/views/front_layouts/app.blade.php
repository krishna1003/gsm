<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Home Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
   

    @include('front_partials.head')

    @yield('css')

</head>

<body>

    <!-- Start Page Wrapper  -->
    <div class="page-wrapper">

        @include('front_layouts.head')

        @yield('content')

        @include('front_layouts.foot')

       

    </div>
    <!-- End Page Wrapper  -->

    <a href="#" class="scrollup"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

    @yield('model')
    @yield('js')

</body>
</html>