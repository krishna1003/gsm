<footer class="pl-3 pr-3 pt-2 pb-2">
    <div class="container-fluid">
        <ul class="d-sm-flex align-items-sm-center text-center text-sm-left">
            <li class="pr-sm-4 pr-0 mb-3 mb-sm-0"><a href="#"><img src="{{asset('front2/images/favicon.png')}}" /></a></li>
            <li class="pr-sm-4 pr-0 mb-3 mb-sm-0"><a href="#">FAQ's</a></li>
            <li class="pr-sm-4 pr-0 mb-3 mb-sm-0"><a href="#">Privacy Policy</a></li>
            <li class="pr-sm-4 pr-0 mb-3 mb-sm-0"><a href="#">Terms and conditions</a></li>
            <li class="pr-sm-4 pr-0 mb-0 mb-sm-0"><a href="#">Do not Disturb</a></li>
        </ul>
    </div>
</footer>
<script type="text/javascript">
	window.setTimeout(function () {
    $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 5000);

	window.setTimeout(function () {
    $(".alert-danger").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 5000);
</script>