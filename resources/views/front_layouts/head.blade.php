<header class="p-lg-3 p-0 pt-2 pb-2">
    @auth
    <div class="container-fluid">
        <div class="row extra-small-gutter align-items-center justify-content-between">
            <div class="col-lg-9 col-6">
                <div class="row extra-small-gutter align-items-center">
                    <div class="col-lg-4 d-flex align-items-center">
                        <button class="navbar-toggle border-0 mr-2 p-0 d-md-block d-lg-none" id="navbar-toggle" role="button">
                            <span></span>
                        </button>
                        <a href="{{ route('userpanel') }}"><img src="{{asset('front2/images/logo.png')}}" /></a>
                    </div>
                    <div class="col-lg-8">
                        <ul class="d-lg-flex main-nav justify-content-lg-between">
                            <li><a class="<?= ($activeTab == "address-book" || $activeTab == "sms-send"   ? "active" : ""); ?>" href="{{ route('userpanel') }}">SMS Send</a></li>
                            <li><a class="<?= ($activeTab == "user_sms_report"   ? "active" : ""); ?>" href="{{route('user.sms.report')}}">SMS Report</a></li>
                            <li><a class="<?= ($activeTab == "apikey"   ? "active" : ""); ?>" href="{{route('apikeyindex')}}">API Key</a></li>
                            <li><a href="{{route('user.show.all.senderid')}}" class="<?= ($activeTab == "user_sender_ids"   ? "active" : ""); ?>" >Sender ID</a></li>
                            <li><a class="{{ route('user.logout') }}" href="campaign.html">Campaing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <ul class="d-flex justify-content-end align-items-center">
                    <li class="mr-4">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle btn-link border-0 d-flex align-items-center p-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="mr-2" src="{{asset('front2/images/wallet-icon.svg')}}" /> <span class="text-dark">{{ getWalletMessagge() }} Msg</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right overflow-hidden" aria-labelledby="dropdownMenu2">
                                <a href="{{ route('addmoney') }}" class="dropdown-item"><img src="{{asset('front2/images/add-money-icon.png')}}" /> Add Money</a>
                                <a href="{{ route('transaction-history') }}" class="dropdown-item"><img src="{{asset('front2/images/payment-icon.png')}}" /> Payment History</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle btn-link border-0 p-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="align-middle" src="{{asset('front2/images/profile.png')}}" />
                            </button>
                            <div class="dropdown-menu dropdown-menu-right overflow-hidden" aria-labelledby="dropdownMenu2">
                                <a href="{{ route('userprofile.update') }}" class="dropdown-item" type="button">My Profile</a>
                                <a href="{{ route('logout') }}" class="dropdown-item" type="button" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form> 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @else
    <div class="container-fluid">
        <div class="row no-gutter align-items-center justify-content-between">
            <div class="col-sm-6 col-5">
                <a href="{{ route('home') }}"><img src="{{asset('front2/images/logo.png')}}" /></a>
            </div>
            <div class="col-sm-6 col-7 justify-content-end d-flex">
                <a href="{{ route('login') }}" class="btn mr-3 theme-btn">Login</a>
                <a href="{{ route('ClientRegister') }}" class="btn theme-btn">Sign Up</a>
            </div>
        </div>
    </div>
    
    @endauth
</header>