<section class="testimonials-section pt-80 pb-75">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-title">
                    <h2>Client Reviews</h2>
                    <div class="sec-line mb-40"></div>
                </div>
                <div class="testimonials-post testimonials_slide_col_two owl-carousel owl-theme owl-navst st-three">
                    @if (!empty(get_customer_reviews()))
                        @foreach (get_customer_reviews() as $review)
                            <div class="testimonials-item">
                                <div class="thumb">
                                    <img src="{!!asset('profiles/'.$review->profilepic) !!}" alt="">
                                </div>
                                <div class="content">
                                    <h5>{{$review->fullname}} <small>- Traveler ut illum</small></h5>
                                    <ul>
                                        @if (!$review->rating < 1)
                                            @for ($i = 1; $i <= $review->rating; $i++)
                                                <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            @endfor
                                        @endif
                                    </ul>
                                    <p><i class="fa fa-quote-left"></i>{!! $review->review !!}</p>
                                    <p>
                                        <a href="{{route('reviews.view.user.review',['id'=>$review->id,'userid'=>$review->userid])}}" class="btn btn-sm btn-yatra">Read more</a>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>