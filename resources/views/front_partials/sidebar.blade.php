<!-- <div class="col-lg-3 col-sm-3 p-0"> -->
<nav id="sidebar" class="sticky-top">
    <div class="px-3 mt-2">

        <!-- <div class="custom-menu">
            <button type="button" id="sidebarCollapse" class="btn btn-primary">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
            </button>
        </div> -->



        <img class="img logo rounded-circle mb-3" onerror="this.src='{{asset('front/images/user.png')}}';" src="{!! asset('profiles/'.Auth::user()->profilepic) !!}" >
        <h5>{{ Auth::user()->username }}</h5>
        <div class="role">
           
        </div>
        <hr>
        <ul class="list-unstyled components mt-3 mb-5">
            <li class="{{!empty($activeTab) ? ($activeTab == "dashboard" ? "active" : "") : ""}}">
                <a href="">Dashboard</a>
            </li>
            @if (get_user_permission("manage_users","view"))
            <li class="{{!empty($activeTab) ? ($activeTab == "manage_users" ? "active" : "") : ""}}" >
                <a href="{{ route('users') }}"> Users</a>
            </li>
            @endif

             @if (Auth::user()->roleid == '0')
            <li class="{{!empty($activeTab) ? ($activeTab == "manage_retailer" ? "active" : "") : ""}}" >
                <a href="{{ route('retailer') }}"> Retailer</a>
            </li>
            @endif
            
            @if (get_user_permission("manage_banners","view"))
            <li class="{{!empty($activeTab) ? ($activeTab == "manage_banners" ? "active" : "") : ""}}">
                        <a href="{{route('banner')}}">Banner</a>
                    </li>
                    @endif
            @if (get_user_permission("manage_category","view"))
            <li class="{{!empty($activeTab) ? ($activeTab == "manage_category" ? "active" : "") : ""}}">
                        <a href="{{route('category')}}">Category</a>
                    </li>
                    @endif
            
            @if (get_user_permission("manage_subcategory","view"))
                     <li class="{{!empty($activeTab) ? ($activeTab == "manage_subcategory" ? "active" : "") : ""}}">
                        <a href="{{route('subcategory')}}">SubCategory</a>
                    </li>
                    @endif
            
            @if (get_user_permission("manage_product","view"))

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_product" ? "active" : "") : ""}}">
                        <a href="{{route('product')}}">Product</a>
                    </li>
                    @endif


                      @if (Auth::user()->roleid == '11')
                     @if (get_user_permission("manage_retailerproduct","view"))

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_retailerproduct" ? "active" : "") : ""}}">
                        <a href="{{route('retailerproduct')}}">Retailer Product</a>
                    </li>
                    @endif
                    @endif

                    @if (Auth::user()->roleid == 7)

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_productrequest" ? "active" : "") : ""}}">
                        <a href="{{route('productrequest')}}">Product Request</a>
                    </li>
                    @endif

                     

                    <li>
                <a href="#request" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Request</a>
                <ul class="collapse list-unstyled" id="request">

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_product_request" ? "active" : "") : ""}}">
                        <a href="{{route('manage_product_request')}}">Product Request</a>
                    </li>

                    @if (get_user_permission("retailer_close_account_req","view"))

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_allcloseaccountrequest" ? "active" : "") : ""}}">
                        <a href="{{route('allCloseaccountrequest')}}">Close Account Request</a>
                    </li>
                    @endif

                    
                </ul>
            </li>

                    @if (get_user_permission("manage_product_unit","view"))
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_product_unit" ? "active" : "") : ""}}">
                        <a href="{{route('product_unit')}}">Product Unit</a>
                    </li>
                    @endif


                    @if (get_user_permission("manage_brand","view"))

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_brand" ? "active" : "") : ""}}">
                        <a href="{{route('brand')}}">Brand</a>
                    </li>
                    @endif

                    @if (get_user_permission("manage_coupon","view"))

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_coupon" ? "active" : "") : ""}}">
                        <a href="{{route('coupon')}}">Coupon</a>
                    </li>
                    @endif

                     @if (get_user_permission("manage_menu","view"))
                     <li class="{{!empty($activeTab) ? ($activeTab == "manage_menu" ? "active" : "") : ""}}">
                        <a href="{{route('menu')}}">Menu</a>
                    </li>
                    @endif

                    @if (get_user_permission("manage_roles","view"))
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_roles" ? "active" : "") : ""}}">
                        <a href="{{ route('roles') }}">Roles</a>
                    </li>
                    @endif
                    @if (get_user_permission("manage_modules","view"))
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_modules" ? "active" : "") : ""}}">
                        <a href="{{ route('modules') }}">Modules</a>
                    </li>
                    @endif
                    @if (get_user_permission("manage_permissions","view"))
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_permissions" ? "active" : "") : ""}}">
                        <a href="{{ route('permissions') }}">Manage Permissions</a>
                    </li>
                    @endif
                     @if (Auth::user()->roleid == '0')
                    <li class="{{!empty($activeTab) ? ($activeTab == "kycDocVerification" ? "active" : "") : ""}}" >
                    <a  href="{{ route('admin.document.varification') }}">KYC Verification</a>
            </li>
            @endif



            



           

            <hr>
            <li class="">
                <a href="{{ route('admin.logout') }}">Logout</a>
            </li>
        </ul>





        <!-- <div class="footer invisible">
                <p>
                                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
                                  </p>
            </div> -->

    </div>
</nav>

<!-- </div> -->