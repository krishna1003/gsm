@extends('front_layouts.app')

@section('css')
<style>

        .mouse-bg {
            width: 536px;
            height: 400px;
            position: relative;
        }

        .pramotional-bg.position-absolute {
            padding: 0 100px;
        }

        #background1 {
            background-image: url("{{asset('front2/images/home/design1.png')}}");
        }

        #background2 {
            background-image: url("{{asset('front2/images/home/design2.png')}}");
            position: absolute;
            background-repeat: no-repeat;
            background-position: right bottom;
        }
    </style>
@endsection

@section('content')

<div class="container-fluid">
            <div class="sms-platform-sec p-4">
                <div class="border-bottom pb-5">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-lg-6">
                            <h1 class="heading1 mb-3">India’s No. 1 Bulk SMS Platform</h1>
                            <p>
                                Increase sales and customer satisfaction with smarter SMS campaigns, instant
                                OTPs, notifications, two-way interactions, and other
                                award-winning bulk SMS services.
                            </p>
                            <div class="row mt-4">
                                <div class="col-md-7 mb-3">
                                    <input type="text" class="form-control rounded mr-3" placeholder="Email Address" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                </div>
                                <div class="col-md-5 mb-3">
                                    <button class="btn btn-block theme-btn rounded">Create a Free Account</button>
                                </div>
                            </div>
                            <div class="rated d-flex align-items-center mt-4">
                                <img src="{{asset('front2/images/home/star.png')}}" class="mr-3" /> <span>Rated 4.8/5 in Google</span>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-5 position-relative mt-5 d-none d-lg-block">
                            <div id="background1" class="mouse-bg"></div>
                            <div id="background2" class="mouse-bg"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="features_sec pt-4 pb-4">
            <div class="text-center mb-5">
                <h2 class="heading2 text-center d-inline-block">Comprehensive Features</h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/delivery-rate.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">High Delivery rate</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/senderid.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">High Delivery rate</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/schedule.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Schedule Campaign</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/price.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Flat Price</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/contacts.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Own List of Contacts</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/traking.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Tracking & Reports</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/pesonlized.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Personalized body</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/support.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">Support</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="media">
                            <img class="mr-3" src="{{asset('front2/images/home/api-support.png')}}" alt="">
                            <div class="media-body">
                                <h4 class="heading3">API Support</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fermentum velit quis lectus faucibus, id commodo mi condimentum. Cras viverra sit amet tortor ut tempus.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pramotional-sms-sec p-4">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-5 position-relative mt-5 d-none d-lg-block">
                        <img id="background3" class="pramotional-bg position-relative" src="{{asset('front2/images/home/pramotional-sms-bg1.png')}}" />
                        <img id="background4" class="pramotional-bg position-absolute" src="{{asset('front2/images/home/pramotional-sms-bg2.png')}}" />
                    </div>
                    <div class="col-lg-7">
                        <h5 class="heading1 text-center mb-5">
                            Promotional SMS or Transactional SMS,
                            we’ve got you covered
                        </h5>
                        <div class="row mt-4">
                            <div class="col-md-6 text-center">
                                <h6 class="heading3">Promotional SMS</h6>
                                <p>
                                    Configured by default for all new accounts,
                                    promotional SMS is generally used for sending
                                    any offers or promotions to new and existing
                                    customers. Messages are sent to non-DND.
                                </p>
                            </div>
                            <div class="col-md-6 text-center">
                                <h6 class="heading3">Transactional SMS</h6>
                                <p>
                                    Configured by default for all new accounts,
                                    promotional SMS is generally used for sending
                                    any offers or promotions to new and existing
                                    customers. Messages are sent to non-DND.
                                </p>
                            </div>
                        </div>
                        <div class="use-steps mt-5">
                            <h3 class="heading4 text-center">How to Use</h3>
                            <ul class="d-flex justify-content-between position-relative">
                                <li class="text-center">
                                    <span class="d-block"><img src="{{asset('front2/images/home/step1.svg')}}" /></span>
                                    Create ID
                                </li>
                                <li class="text-center">
                                    <span class="d-block"><img src="{{asset('front2/images/home/step2.svg')}}" /></span>
                                    Campaign
                                </li>
                                <li class="text-center">
                                    <span class="d-block"><img src="{{asset('front2/images/home/step3.svg')}}" /></span>
                                    Generate API
                                </li>
                                <li class="text-center">
                                    <span class="d-block"><img src="{{asset('front2/front2/images/home/step4.svg')}}" /></span>
                                    Secret Key
                                </li>
                                <li class="text-center">
                                    <span class="d-block"><img src="{{asset('front2/images/home/step5.svg')}}" /></span>
                                    Refer API
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing-sec pt-4 pb-4">
            <div class="container">
                <div class="text-center mb-5">
                    <h2 class="heading2 text-center d-inline-block mb-3">Pricing</h2>
                    <p>Pay As You Go. No Hidden Setup Fees. Forever Free To Use Platform.</p>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <table class="table pricing-table table-striped shadow rounded">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">SMS Bundle</th>
                                    <th scope="col" class="text-center">Per Bundle</th>
                                    <th scope="col" class="text-center">Per SMS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">100 sms</td>
                                    <td class="text-center"><b>Free</b></td>
                                    <td class="text-center"><b>Free</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1000 sms</td>
                                    <td class="text-center">200 Rs</td>
                                    <td class="text-center">0.20 Rs</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-7">
                        <ul class="pricing-sms">
                            <li>Lifetime validity for SMS credits</li>
                            <li>Credits can be used for both Promotional & Transactional SMS</li>
                            <li>Free access to web portal & SMS APIs</li>
                        </ul>
                        <span class="pt-3 pb-4 d-block">
                            Just pay for SMS. Get access to 50+ free features right out of the box including:
                        </span>
                        <div class="pricing-features row small-gutter">
                            <ul class="col-md-4">
                                <li>Send Files via SMS</li>
                                <li>Send in 20+ Languages</li>
                                <li>Comprehensive APIs</li>
                                <li>Surveys & Forms</li>
                            </ul>
                            <ul class="col-md-4">
                                <li>Personalize Bulk Sends</li>
                                <li>Real-time Reports</li>
                                <li>Manage Inbound SMS</li>
                                <li>Excel to SMS Plugin</li>
                            </ul>
                            <ul class="col-md-4">
                                <li>URL Shortener</li>
                                <li>Custom Sender IDs</li>
                                <li>Opt-out/Opt-in Path</li>
                                <li>Android/iOS App</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/mouse.parallax.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            $("#header").load("header.html");
            $("#footer").load("footer.html");
            var viewPortWidth = jQuery(window).width();
            if (viewPortWidth > 1024) {
                $('#background1').mouseParallax({ moveFactor: 3 });
                $('#background2').mouseParallax({ moveFactor: 6 });

                $('#background3').mouseParallax({ moveFactor: 4 });
                $('#background4').mouseParallax({ moveFactor: 1 });
            }
        });
    </script>
@endsection