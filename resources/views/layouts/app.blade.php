<!DOCTYPE html>
<html>
<head>
    @include('layouts.base.head')
    @yield('extracss')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('layouts.base.navbar')

  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('layouts.base.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @include('partials.custom-messages')

    @yield('content')

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="{{ url('/') }}">daddyscode.com</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.2
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.base.foot')
    @yield('extrajs')
<script>
    $(document).ready(function(){
        $(".alert").delay(5000).slideUp(300);
    });
</script>
</body>
</html>
