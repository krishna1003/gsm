<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('lte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Daddys-SMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('lte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="Javascript::void(0)" class="d-block">

                    {{ ucfirst(Auth::user()->name) }}

        </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview ">
            <a href="{{ route('home') }}" class="nav-link ">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
            </li>
            <li class="nav-header">Permissions</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i> <p> Users Role & Permissions <i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('users.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Manage Users</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('roles.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Manage Role</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('products.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Manage Product</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">Technical</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i> <p> API <i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="pages/layout/top-nav.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Test API</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/layout/fixed-topnav.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Client API</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">Thirt Party API</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> External API <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('list.external.apis') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All API</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('sendsms.single.view') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Send SMS</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('sendsms.multi.view') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Send Multi SMS</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('sendsms.bulk.view') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Send Bulk SMS</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">System Users</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Users <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Client</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/chartjs.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Resellers</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/inline.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>System User</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">System Data</li>
            <li class="nav-item has-treeview">
                <a href="Javascript::void(0)" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Sender IDS <i class="right fas fa-angle-left"></i> </p>
                </a>                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('show.all.senderids') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show IDS</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('contact.group.show') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Group Contacts</p>
                        </a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="Javascript::void(0)" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Contact <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('contact.show') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Personal Contacts</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('contact.group.show') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Group Contacts</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/inline.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Bulk Contacts</p>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item has-treeview">
            <a href="{{ route('phonebook') }}" class="nav-link {{!empty($activeTab) ? ($activeTab == "manage_phonebook" ? "active" : "") : ""}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Phone Book</p>
            </a>
            </li>
            <li class="nav-item has-treeview ">
            <a href="{{ route('sms_package') }}" class="nav-link {{!empty($activeTab) ? ($activeTab == "manage_smspackage" ? "active" : "") : ""}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>SMS Package List</p>
            </a>
            </li>
            <li class="nav-item has-treeview ">
            <a href="{{ route('user_sms_package') }}" class="nav-link {{!empty($activeTab) ? ($activeTab == "manage_usersmspackage" ? "active" : "") : ""}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>User SMS Package</p>
            </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Campaign <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="pages/charts/flot.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Personal Contacts</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/chartjs.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Group Contacts</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/inline.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Bulk Contacts</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">System Analytics</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Report <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('delivery.master.history') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>SMS Report</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/chartjs.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Client Report</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/inline.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Resellers Report</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> SMS <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Send SMS</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/chartjs.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Group Contacts</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="pages/charts/inline.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Bulk Contacts</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-header">System Setting</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p> Setting <i class="right fas fa-angle-left"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="pages/charts/chartjs.html" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>ChartJS</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="pages/charts/flot.html" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Flot</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="pages/charts/inline.html" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Inline</p>
                    </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-tree"></i>
                <p>
                UI Elements
                <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="pages/UI/general.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>General</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Icons</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/buttons.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Buttons</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/sliders.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sliders</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/modals.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Modals & Alerts</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/navbar.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Navbar & Tabs</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/timeline.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Timeline</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/UI/ribbons.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ribbons</p>
                </a>
                </li>
            </ul>
            </li>
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                Forms
                <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="pages/forms/general.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>General Elements</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/forms/advanced.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Advanced Elements</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/forms/editors.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Editors</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/forms/validation.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Validation</p>
                </a>
                </li>
            </ul>
            </li>
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-table"></i>
                <p>
                Tables
                <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="pages/tables/simple.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Simple Tables</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/tables/data.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>DataTables</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="pages/tables/jsgrid.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>jsGrid</p>
                </a>
                </li>
            </ul>
            </li>
            <li class="nav-header">LABELS</li>
            <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-danger"></i>
                <p class="text">Important</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-warning"></i>
                <p>Warning</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-info"></i>
                <p>Informational</p>
            </a>
            </li>
        </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
