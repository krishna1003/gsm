<!-- Main content -->
<section class="content">
    <div class="alert alert-danger" id="smsMessage" style="display: none;">Please Select Messages</div>
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
                <div class="pull-right" style="float:right;">
                    <span>File : {{ $fileName }}</span>
                </div>
            </div>

          <!-- /.card-header -->
            <div class="card-body">
                <table id="contacts-data-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="all" id="all" value="{{json_encode($body)}}"> </th>
                            @foreach ($header as $head)
                                <th>{{ strtoupper($head) }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($body as $key => $row)
                            <tr>
                                <?php $record = json_encode($row) ?>
                                <td><input type="checkbox" name="msg[]" id="chkbox_{{$row->number}}" value="{{$record}})" onclick="selectRow({{$row->number}}, {{$record}})"> </td>
                                @foreach ($row as $col)
                                    <td> {{ $col }} </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<script>

    var fileuuid = "{{ $fileuuid }}";
    var selectedRows = [];


    function emptyArr() {
        var selectedRows = [];
    }

    //Check All
    $('#all').click(function() {
        if($(this).prop("checked") == true) {
            emptyArr();
            $('input:checkbox').prop('checked', true);
            var allValues = JSON.parse($(this).val());
            allValues.forEach(element => {
                selectedRows.push(element);
            });
        }
        else if($(this).prop("checked") == false){
            $('input:checkbox').prop('checked', false);
            var allValues = JSON.parse($(this).val());
            allValues.forEach(element => {
                selectedRows.pop(element);
            });
        }

        console.log("main array check : ", selectedRows);
    });

    function selectRow(number, records) {

        if($('#chkbox_'+number).prop("checked") == true) {
            console.log("Add this", number);
            var boxcheck = $('#chkbox_'+number).val();
            selectedRows.push(records);
        }else if($('#chkbox_'+number).prop("checked") == false) {
            console.log("Selected remove", number);
            var boxcheck = $('#chkbox_'+number).val();
            selectedRows.pop(records);
        }
        // console.log("Select Data  :: ", selectedRows);
    }
</script>
