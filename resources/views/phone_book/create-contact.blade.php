@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Phone Book</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('phonebook') }}">Phone Book</a></li>
                <li class="breadcrumb-item active">Create Contact</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class=" card-primary">

                            @if (!empty($PhoneBook))
                            <form method="post" action="{{ route('phonebook.update', $PhoneBook->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">Phone Book Information:</h3>
                                
                               
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$PhoneBook->name}}" id="name" name="name" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Mobiel Number: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$PhoneBook->mobile_number}}" id="mobile_number" name="mobile_number" />
                                    </div>
                                   
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$PhoneBook->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('phonebook.store')}}" method="post" id="PhoneBookForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">PhoneBook Information:</h3>
                                  
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name">Name: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="name" name="name" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Mobiel Number: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="mobile_number" name="mobile_number" />
                                    </div>
                                   
                                </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                               
                                            </label>
                                            
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });

    
    $('#PhoneBookForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            
        },
        rules: {
            name: "required",
            mobile_number: "required",
            
            status: "required"
        }
    });

    
</script>



@endsection
