premium-register.blade.php@extends('front_layouts.app')
@section('css')
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />
<style>
    #ui-datepicker-div {
        width: 19em;
        z-index: 4 !important;
    }
</style>
@endsection
@section('content')

<section class="contact-section signup">
    <div class="container my-5">

        @include("tools.errors")
        <div class="row ">
            <div class="col-md-12">
                <div class="booking-forms" method="post" action="#">

                    <nav>
                        <div class="nav nav-tabs d-flex justify-content-center" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link" id="nav-hotelbk-tab" href="{{url('register')}}" aria-selected="true">
                                General Signup</a>
                            <a class="nav-item nav-link border-right-0 active premium" id="nav-packagesbk-tab" data-toggle="tab" href="#premium" aria-controls="premium" aria-selected="false">Premium Signup
                            </a>

                        </div>
                    </nav>
                    <div class="tab-content pt-4" id="nav-tabContent">
                        @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        <!-- item start -->
                        <div class="tab-pane fade show active" id="premium" role="tabpanel" aria-labelledby="nav-packagesbk-tab">
                            <form method="post" id="premium-signup" action="{{route('create.premiumRegister')}}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-row">

                                    <div class="col-md-6 d-flex align-items-stretch">

                                        <div class="card w-100 border-dark">

                                            <div class="card-header d-flex justify-content-between align-items-center">
                                                <div>Login Details</div>
                                                @if(!Auth::check()) <div class="premium-login">
                                                    Existing customer? please login here (free /premium) <a class="btn btn-shukul btn-sm btn-rounded" type="btn" data-toggle="modal" data-target="#login">Login</a>
                                                </div> @endif
                                            </div>
                                            <div class="card-body ">
                                                <div class="row">

                                                    <div class="form-group col-md-6">
                                                        <label>Username</label>
                                                        <input type="text" class="form-control" placeholder="Enter Username" required @if(Auth::check()) value="{{$user->username}}" readonly @else name="admin_usr" id="usernames" @endif>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    @if(!Auth::check())
                                                    <div class="form-group col-md-6 password_field">
                                                        <label>Password</label>
                                                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 d-flex align-items-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Reference Details</div>
                                            <div class="card-body ">
                                                <div class="form-group">
                                                    <label>Reference Code</label>
                                                    <input type="text" name="reference_code" id="reference_code" class="form-control" placeholder="Enter Reference Code" required @if($referal_code !='' ) readonly value="{{$referal_code}}" @endif>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                    <div class="valid-feedback">
                                                        Reference Name Will Show Here
                                                    </div>
                                                </div>

                                                <div class="form-group  @if($referal_code == '') d-none @endif referece-user">
                                                    <label>Reference User</label>
                                                    <input type="text" id="reference_user" value="{{$reference_name}}" class="form-control" readonly>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 d-flex align-items-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">User Details</div>
                                            <div class="card-body">

                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="Enter Your First Name" @if(Auth::check()) value="{{$user->firstname}}" readonly @endif required>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Enter Your Last Name" @if(Auth::check()) value="{{$user->lastname}}" readonly @endif required>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>


                                                <!--<div class="form-group">
                                                        <label>Name</label>
                                                        <input type="text" name="name" id="name" class="form-control"
                                                            placeholder="Input Name" @if(Auth::check()) value ="{{$user->name}}" readonly @endif required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                    </div>-->

                                                <div class="form-group">
                                                    <label>Mobile</label>
                                                    <input type="text" name="contact" id="contact" class="form-control" placeholder="Enter Mobile Number" required @if(Auth::check()) value="{{$user->contactno}}" readonly @endif onkeypress="return isNumberKey(event)" maxlength="12" minlength="10">
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="form-check">
                                                        <input type="checkbox" checked value="1" class="form-check-input" name="IsWhatsapp" id="IsWhatsapp">
                                                        <label class="form-check-label" for="exampleCheck1">Is Whatsapp number same as mobile number?</label>
                                                    </div>
                                                </div>
                                                <div class="form-group d-none whatsappnumber">
                                                    <label>Whatsapp Number</label>
                                                    <input type="text" name="whatsappno" id="whatsappno" class="form-control" placeholder="Enter Whatsapp Number" required onkeypress="return isNumberKey(event)" maxlength="12" minlength="10">
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email ID" required @if(Auth::check()) value="{{$user->email}}" readonly @endif>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Age Group</label>
                                                    <select class="form-control" name="ageGroup" id="ageGroup" @if(Auth::check()) value="{{$user->ageGroup}}" disabled @endif>
                                                        <option value="">Age group</option>
                                                        @if(Auth::check())
                                                        <option value="student" {{ ( $user->ageGroup == 'student') ? 'selected' : '' }}>Student</option>
                                                        <option value="20-30" {{ ( $user->ageGroup == '20-30') ? 'selected' : '' }}>20-30</option>
                                                        <option value="30-40" {{ ( $user->ageGroup == '30-40') ? 'selected' : '' }}>30-40</option>
                                                        <option value="40-50" {{ ( $user->ageGroup == '40-50') ? 'selected' : '' }}>40-50</option>
                                                        <option value="50-60" {{ ( $user->ageGroup == '50-60') ? 'selected' : '' }}>50-60</option>
                                                        @else
                                                        <option value="student">Student</option>
                                                        <option value="20-30">20-30</option>
                                                        <option value="30-40">30-40</option>
                                                        <option value="40-50">40-50</option>
                                                        <option value="50-60">50-60</option>
                                                        @endif
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Date Of Birth</label>
                                                    <input type="text" name="dob" id="dob" class="form-control" placeholder="Enter Date of Birth" required @if(Auth::check()) value="{{ $user->dob ? date('d-m-Y', strtotime($user->dob)) : ''}}" readonly @endif>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 d-flex align-items-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Address Details</div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input type="text" name="address" id="address" class="form-control" placeholder="Enter Your Address" required @if(Auth::check()) value="{{$user->address}}" disabled @endif>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control" name="country" id="country" @if(Auth::check()) disabled @endif>
                                                        <option value="">Choose Country</option>
                                                        @if (!empty(get_countries()))
                                                        @foreach (get_countries() as $country)
                                                        @if(Auth::check())
                                                        <option value="{{$country->id}}" {{ ( $user->country == $country->id) ? 'selected' : '' }}>{{$country->name}}</option>
                                                        @else

                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>State</label>
                                                    <select class="form-control" name="state" id="state" @if(Auth::check()) value="{{$user->ageGroup}}" disabled @endif>
                                                        <option value="">Choose State</option>
                                                        @if(Auth::check())
                                                        <option value="{{$user->state}}" selected>{{$user->statename}}</option>
                                                        @endif
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>City</label>
                                                    <select class="form-control city" name="city" id="city" @if(Auth::check()) value="{{$user->ageGroup}}" disabled @endif>
                                                        <option value="">Choose City</option>
                                                        @if(Auth::check())
                                                        <option value="{{$user->city}}" selected>{{$user->cityname}}</option>
                                                        @endif
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Locality</label>
                                                    <input type="text" name="locality" id="locality" class="form-control" placeholder="Enter Your locality" required @if(Auth::check()) value="{{$user->locality}}" @endif>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Pincode</label>
                                                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Enter Your pincode" required @if(Auth::check()) value="{{$user->pincode}}" @endif onkeypress="return isNumberKey(event)" maxlength="6">
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6 d-flex align-items-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Membership Details</div>
                                            <div class="card-body ">
                                                <div class="form-group">
                                                    <select class="form-control" name="membershipid" id="membershipid" required>

                                                        @if (!empty($membershiptypes))
                                                        @foreach ($membershiptypes as $type)
                                                        <option value="{{$type->id}}" data-type="{{json_encode($type)}}" {{ $loop->first ? 'selected="selected"' : '' }}>{{$type->name}}</option>
                                                        @endforeach
                                                        @endif

                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="card membercard">
                                                        <!-- <div class="card-header">Membership Details</div> -->
                                                        @if (!empty($membershiptypes))
                                                        <div class="card-body">
                                                            <h5 class="card-titlemember-title">{{$membershiptypes[0]->name}}</h5>

                                                            <div class="is_package_info"></div>


                                                            <p class="member-price mb-2">Price: <strong>Rs. {{$membershiptypes[0]->amount}} /-</strong></p>

                                                            <div class="member-desc">{!! $membershiptypes[0]->description !!}</div>


                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 d-flex align-items-stretch">
                                        <div class="card w-100">

                                            <div class="card-header">Payment Mode</div>
                                            <div class="card-body">
                                                <div class="online_charge">
                                                    <table class="table table-borderless" id="total_table">
                                                        <thead>
                                                            @if (!empty($membershiptypes))
                                                            <tr>
                                                                <th class="text-left">Subtotal Amount</th>
                                                                <th class="text-right">Rs. <span id="total">{{$membershiptypes[0]->amount}}</span> /-</th>
                                                            </tr>
                                                            <tr>
                                                                <?php $charge = floatval(get_option('online_charge'));
                                                                $fees = ($membershiptypes[0]->amount * $charge) / 100;
                                                                $total_amount = $membershiptypes[0]->amount + $fees;
                                                                ?>
                                                                <th class="text-left">Convenience Fee (<span id="charge_per">{{$charge}}</span>%)</th>

                                                                <th class="text-right">Rs. <span id="online_charge">{{$fees}}</span> /-</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-left">Total Amount</th>
                                                                <th class="text-right"><b>Rs. <span class="net_total">{{$total_amount}}</span> /-</b></th>
                                                            </tr>
                                                            @endif
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="custom-control custom-radio ">
                                                    <input type="radio" id="razorpay" name="payment_type" checked value="razorpay" class="custom-control-input">
                                                    <label class="custom-control-label" for="razorpay">Online Payment</label>
                                                </div>

                                                <div class="custom-control custom-radio ">
                                                    <input type="radio" id="bank_transfer" name="payment_type" value="bank_transfer" class="custom-control-input">
                                                    <label class="custom-control-label" for="bank_transfer">Bank Transfer</label>
                                                </div>
                                                <div class="custom-control custom-radio ">
                                                    <input type="radio" id="cash" name="payment_type" value="cash" class="custom-control-input">
                                                    <label class="custom-control-label" for="cash">Cash</label>
                                                </div>

                                                <div class="m-2 d-none pay_change">
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <input type="file" class="form-control" name="image" id="image" onchange="readURL(this);" required>
                                                        <img id="preview-image" class="d-none img-thumbnail" src="" alt="your image" style="width:150px; height:150px;" />

                                                    </div>

                                                    <div class="form-group">
                                                        <label>Remark</label>
                                                        <textarea name="remark" required id="remark" class="form-control"></textarea>


                                                    </div>
                                                </div>




                                            </div>
                                            <div class="card-header"></div>

                                            <div class="card-body">
                                                <div class="text-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="tnc_accepts" class="custom-control-input" id="tnc_accepts">
                                                        <label class="custom-control-label text-left" for="tnc_accepts">I have read and agree with the <a href="{{route('terms.condition')}}" target="_blank">Terms and Conditions</a> <br><span id="errorToShow"></span> </label>
                                                    </div>
                                                </div>

                                                <div class="form-group2 mt-3">
                                                    <div class="contact-textarea text-center">
                                                        <button class="btn btn-theme" id="pay_now" type="submit" value="Submit Form">Sign Up Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <input type="hidden" name="signup_token" id="signup_token" value="new">

                            </form>
                        </div>
                        <!-- item end -->


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

@section('js')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script>
    var online_fee = "{{floatval(get_option('online_charge'))}}";
    $(document).ready(function() {


        $('#dob').datepicker({
            minDate: new Date(1900, 1 - 1, 1),
            maxDate: '-18Y',
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:-18'
        });


        /*$("#dob").datepicker(
    {
      minDate: new Date(1900,1-1,1),
	  maxDate: '-18Y',
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      yearRange: '-110:-18'
    }
  );   */

        $('#IsWhatsapp').change(function() {
            if (!$('#IsWhatsapp').is(':checked')) {
                $('.whatsappnumber').removeClass('d-none');
                $('#whatsappno').val('');
            } else {
                $('.whatsappnumber').addClass('d-none');

            }
        });
        $('#premium-signup')[0].reset()

        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space not allowed");

        $('#premium-signup').validate({
            errorPlacement: function(error, element) {

                if (element.attr("name") == "tnc_accepts") {
                    error.appendTo("#errorToShow");
                } else {
                    // something else if it's not a checkbox

                    element.css('background', '#ffdddd');
                    error.insertAfter(element);
                }
            },
            rules: {

                reference_code: {
                    required: true,
                    minlength: 8,
                    maxlength: 15,
                    remote: {
                        type: 'post',
                        url: "{{ URL('user/existreference_code') }}",
                        async: false,
                        async: false,
                        data: {
                            reference_code: function() {
                                return $("input[name='reference_code']").val();
                            },
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }


                },
                firstname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                lastname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                business_person: {
                    digits: true
                },

                admin_usr: {
                    required: true,
                    minlength: 3,
                    maxlength: 30,
                    noSpace: true,
                    remote: {
                        type: 'post',
                        url: "{{ URL('user/existusername') }}",
                        async: false,
                        async: false,
                        data: {
                            username: function() {
                                return $("input[name='admin_usr']").val();
                            },
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }


                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,

                },
                contactno: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                },
                email: {
                    required: true,
                    Validemail: true,
                },
                ageGroup: {
                    required: true
                },
                address: {
                    required: true,
                    minlength: 3,
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                locality: {
                    required: true,
                    minlength: 3,
                    maxlength: 25,
                },
                pincode: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength: 6,
                },
                tnc_accepts: {
                    required: true
                },
                dob: {
                    required: true
                }
            },
            messages: {
                admin_usr: {
                    remote: '<p class="mt-2 small font-weight-bold">Username already taken. If you are existing customer then please login here <a class="text-danger pointer" type="btn" data-toggle="modal" data-target="#login">Login</a><p>',
                },
                reference_code: {
                    remote: "Reference code is invalid",
                }
            },

            submitHandler: function(form) {

                var payment_type = $('input[type=radio][name=payment_type]:checked').val();
                if (payment_type == 'razorpay') {
                    $("#pay_now").addClass('disabled');
                    $("#pay_now").prop('disabled', true);
                    var amount_cal = gettotalprice();
                    var totalAmount = amount_cal.netamount;
                    var formdata = $('#premium-signup').serialize();
                    var options = {
                        "key": "{{ env('RAZORPAY_KEY') }}",
                        "amount": (totalAmount * 100), // 2000 paise = INR 20
                        "name": "Shukul Yatra",
                        "description": "Shukul Yatra premium membership",
                        "image": "{{asset('front/images/logo.png')}}",
                        "handler": function(response) {

                            //console.log(response);

                            $.ajax({
                                url: "{{url('premiumRegister/create_premiumRegister')}}",
                                type: 'post',
                                dataType: 'json',
                                data: formdata + "&payment_id=" + response.razorpay_payment_id + "&netamount=" + totalAmount + "&amount=" + amount_cal.totalprice + "&fees=" + amount_cal.fees,
                                success: function(msg) {
                                    console.log(msg);
                                    $('#premium-signup')[0].reset();
                                    $('html, body').animate({
                                        scrollTop: 0
                                    }, 0);

                                    if (msg.status === true) {

                                        var htmlmsg = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg.msg + '</div>';

                                        $(htmlmsg).insertBefore('#premium');

                                        @if(Auth::check())
                                        setTimeout(function() {
                                            location.reload();
                                        }, 2000);
                                        @endif
                                    } else {

                                        var htmlmsg = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg.msg + '</div>';

                                        $(htmlmsg).insertBefore('#premium');
                                        //alert(msg.message);
                                    }
                                    /*   console.log(msg);
                       window.location.href = SITEURL + '/thank-you/'+msg.tempcode;
					   console.log( window.location.href);*/
                                }
                            });

                        },
                        "prefill": {
                            "contact": $('#contact').val(),
                            "email": $('#email').val(),
                        },
                        "theme": {
                            "color": "#528FF0"
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();
                } else {
                    $("#pay_now").removeClass('disabled');
                    $("#pay_now").prop('disabled', false);
                    form.submit();
                }
            }
        });

        // username change
        $('#usernames').on('input', function(e) {
            var value = $(this).val();
            console.log(value.length);
            if (value.length >= 4 && !$('#usernames-error').is(':empty')) {
                $("#username").val(value);
            } else {
                $("#username").val('');
            }
        });
        $('#reference_code').on('input', function(e) {
            var value = $(this).val();
            console.log(value.length);
            if (value.length >= 7) {

                $.ajax({
                    type: "POST",
                    data: {
                        reference: value,
                        _token: "{{ csrf_token() }}"
                    },
                    url: "{{url('user/get-user-reference')}}",
                    success: function(res) {
                        console.log(res);

                        if (res.status == "success") {

                            var result = res.data;
                            $('.referece-user').removeClass('d-none');
                            //	$('#usernames').attr("readonly", true);
                            $('#reference_user').val(result.name);
                            //$('#contact').val(result.contactno).attr("readonly", true);


                        } else {
                            $('.referece-user').addClass('d-none');
                            $('#reference_user').val('');

                        }

                    }
                });
            }

        });

        $('#membershipid').change(function() {
            var membershipid = $(this).val();
            var datatype = $(this).find(':selected').data('type');
            console.log(datatype);
            $('.member-title').html(datatype.name);
            $('.member-price').html('Price: <strong>Rs. ' + datatype.amount + '/-</strong>');
            // var str = datatype.description;

            $('.member-desc').html(datatype.description);

            if (datatype.is_package == 0) {
                var person_limit = '<p>Person</p><div class="row"> <div class="col-md-3"> <input type="number" name="business_person" id="business_person" class="form-control" value="10" min="10" onchange="counttotalprice()" > </div>  <div class="col-md-3"> X  ' + datatype.amount + ' </div></div>'
                $('.is_package_info').html(person_limit);
                //	 $(person_limit).insertBefore('.member-price');
                var totalprice = 10 * datatype.amount;
                $('.member-price').html('Price: <strong>Rs. ' + totalprice + '/-</strong>');
            } else {
                $('.is_package_info').html('');
            }
            var total = gettotalprice();

        });

        $('input[type=radio][name=payment_type]').change(function() {
            if (this.value == 'razorpay') {
                $('.pay_change').addClass('d-none');
                $("input[id='image']").prop('required', false);
                $("#remark").prop('required', false);


            } else if (this.value == 'cash') {
                $('.pay_change').removeClass('d-none');
                $("input[id='image']").prop('required', false);
                $("#remark").prop('required', true);
                $("#pay_now").removeClass('disabled');
                $("#pay_now").prop('disabled', false);
            } else {
                $('.pay_change').removeClass('d-none');
                $("input[id='image']").prop('required', true);
                $("#remark").prop('required', true);
                $("#pay_now").removeClass('disabled');
                $("#pay_now").prop('disabled', false);
            }
            var total = gettotalprice();

        });



        $('#country').change(function() {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-state-list')}}?country_id=" + countryID,
                    success: function(res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $.each(res, function(key, value) {
                                $("#state").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        } else {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#state").append('<option value="">Choose State</option>');
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }
        });

        $('#state').on('change', function() {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list')}}?state_id=" + stateID,
                    success: function(res) {
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }

        });

    });

    function counttotalprice() {
        var limit = $("#business_person").val();
        var datatype = $('#membershipid').find(':selected').data('type');
        var price = datatype.amount;

        var totalprice = limit * price;
        $('.member-price').html('Price: <strong>Rs. ' + totalprice + '/-</strong>');
        gettotalprice();
    }

    function gettotalprice() {
        var limit = $("#business_person").val();
        var datatype = $('#membershipid').find(':selected').data('type');
        var price = datatype.amount;
        var totalprice = datatype.amount;
        var fees = 0;
        if (datatype.is_package == 0) {
            var totalprice = limit * price;
        }
        //   alert(online_fee);
        var payment_type = $('input[type=radio][name=payment_type]:checked').val();
        if (payment_type == 'razorpay') {
            $("#charge_per").html(online_fee);
            $("#total").html(totalprice);
            fees = (totalprice * online_fee) / 100;
            var net_amount = totalprice + fees;
            $("#online_charge").html(fees);
            $(".net_total").html(net_amount);
        } else {

            $("#charge_per").html('0');
            $("#total").html(totalprice);

            var net_amount = totalprice + fees;
            $("#online_charge").html(fees);
            $(".net_total").html(net_amount);
        }

        var obj = {
            totalprice: totalprice,
            fees: fees,
            netamount: totalprice + fees
        };
        return obj;

    }

    function readURL(input) {
        $('#preview-image').removeClass('d-none');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#preview-image')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>


@endsection

@endsection