@extends('layouts.app')

@section('extracss')
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Master Delivery Report</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">Delivery Report</a></li>
                <li class="breadcrumb-item active">SMS Delivery Report</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">SMS Delivery Report</h3>

                </div>

              <!-- /.card-header -->
                <div class="card-body">
                    <table id="contacts-data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Company</th>
                                <th>API</th>
                                <th>Client</th>
                                <th>Number</th>
                                <th>Message</th>
                                <th>Delivery Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
    $(function () {
        var table = $('#contacts-data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('delivery.master.history') }}",
            columns: [
                {data: 'user_uuid', name: 'user_uuid'},
                {data: 'company_uuid', name: 'company_uuid'},
                {data: 'api_uuid', name: 'api_uuid'},
                {data: 'client_uuid', name: 'client_uuid'},
                {data: 'number', name: 'number'},
                {data: 'message', name: 'message'},
                {data: 'delivery_status', name: 'delivery_status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: true, searchable: true},
            ]
        });
    });
</script>


@endsection
