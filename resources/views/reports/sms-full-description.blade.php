@extends('layouts.app')


@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">SMS Detail Report</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">Delivery Report</a></li>
                <li class="breadcrumb-item active">SMS Delivery Report</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">SMS Delivery Report</h3>
                    <a href="{{ route('delivery.master.history') }}" class="btn btn-info" style="float: right">Back</a>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">Company Details</h3>
                                  <div class="card-tools">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-md-4"> Name :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getCompanyDetails->comapny_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Contact :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getCompanyDetails->contact }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Email :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getCompanyDetails->email }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Location :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getCompanyDetails->location }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">API Details</h3>
                                  <div class="card-tools">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-md-4"> Title :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getAPIDetails->title }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Method :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getAPIDetails->method }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Description :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getAPIDetails->description }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">User Details</h3>
                                  <div class="card-tools">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-md-4"> Name :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getUserDetails->name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Email :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->getUserDetails->email }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  <h3 class="card-title">SMS Details</h3>
                                  <div class="card-tools">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                  </div>
                                  <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-md-4"> Number :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->number }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Message :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->message }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Delivery Status :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->delivery_status }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-4"> Delivery Time :</label>
                                        <div class="col-md-8">
                                            {{ $smsDetail->created_at }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection
