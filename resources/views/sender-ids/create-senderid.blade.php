@extends('layouts.app')

@section('extracss')

@endsection

@section('content') 
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Create New Sender ID</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('contact.show') }}">Sender IDS</a></li>
                <li class="breadcrumb-item active">Create New Sender ID</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- form start -->
                            <form role="form" method="POST" action="{{ route('create.sender.request', isset($senderData) ? $senderData->sender_uuid : '') }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="senderid">Sender ID</label>
                                        <input type="text" class="form-control" id="senderid" name="senderid" placeholder="Enter SenderID" value="{{ $senderData->senderid ?? ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Type </label> : 
                                            
                                        <label for="promotional"> 
                                            Promotional 
                                            <input type="radio" id="promotional" name="type" value="promotional" required
                                            {{ isset($senderData) ? (($senderData->type == 'promotional') ? 'checked' : '') : ''}} >
                                        </label>
                                        <label for="transactoinal">  
                                            Transactoinal 
                                            <input type="radio" id="transactoinal" name="type" value="transactoinal" required
                                            {{ isset($senderData) ? (($senderData->type == 'transactoinal') ? 'checked' : '') : ''}} >
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="purpos">Purpose	</label>
                                        <textarea class="form-control" id="purpos" name="purpose" required>{{ $senderData->purpose ?? ''}} </textarea>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        {{ isset($senderData) ? 'Update' : 'Submit'}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->

<script>
    $(function () {

    });
</script>


@endsection
