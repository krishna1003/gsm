@extends('layouts.app')

@section('extracss')
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Show Sender IDS</h1> 
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">Sender IDS</a></li>
                <li class="breadcrumb-item active">Show Sender IDS</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Show Sender IDS</h3>
                    <a href="{{ route('apply.new.senderids') }}" class="btn btn-info" style="float: right">Add +</a>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                    <table id="senderid-data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>User Type</th>
                                <th>Senderid</th>
                                <th>Status</th>
                                <th>Type</th>
                                <th>Purpose</th>
                                <th>Approved By</th>
                                <th>Create At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
    $(function () {
        var table = $('#senderid-data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('show.all.senderids') }}",
            columns: [
                {data: 'user_name', name: 'user_name'},
                {data: 'user_type', name: 'user_type'},
                {data: 'senderid', name: 'senderid'},
                {data: 'status', name: 'status'},
                {data: 'type', name: 'type'},
                {data: 'purpose', name: 'purpose'},
                {data: 'approved_by', name: 'approved_by'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: true, searchable: true},
            ]
        });
    });
</script>


@endsection
