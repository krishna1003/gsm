@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">SMS Package</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('sms_package') }}">SMS Package</a></li>
                <li class="breadcrumb-item active">Create SMS Package</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class=" card-primary">

                            @if (!empty($SmsPackage))
                            <form method="post" action="{{ route('sms_package.update', $SmsPackage->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">SMS Package Information:</h3>
                                
                               
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->name}}" id="name" name="name" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="no_of_msg">No. Of Msg: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->no_of_msg}}" id="no_of_msg" name="no_of_msg" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->amount}}" id="amount" name="amount" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="details" name="details">{{$SmsPackage->details}}</textarea>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$SmsPackage->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('sms_package.store')}}" method="post" id="SmsPackageForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">SMS Package Information:</h3>
                                  
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name">Name: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="name" name="name" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="no_of_msg">No. Of Msg: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="no_of_msg" name="no_of_msg" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="amount" name="amount" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="details" name="details"></textarea>
                                    </div>
                                </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                               
                                            </label>
                                            
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>

    $(function () {

    });

    
    $('#SmsPackageForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            
        },
        rules: {
            name: "required",
            details: "required",
            no_of_msg: "required",
            amount: "required",
            
            status: "required"
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('details');
    });

    
</script>



@endsection
