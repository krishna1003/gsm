@extends('layouts.app')

@section('extracss')
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">SMS Package</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">SMS Package</a></li>
                <li class="breadcrumb-item active">All SMS Package</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">All SMS Package</h3>
                    <a href="{{ route('sms_package.create') }}" class="btn btn-info" style="float: right">Add +</a>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                <table id="SmsPackageTable" class="table table-hover  display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                           
                                           <th>Name</th>
                                           <th>Details</th>
                                           <th>No Of Msg</th>
                                           <th>Amount</th>
                                            
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                </table>
                            </div>
                        </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#SmsPackageTable').DataTable({
             scrollY: '60vh',
            "scrollX": true,
            
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('allsmspackage') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
                   "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "columns": [
                
                { "data": "name" },
                { "data": "details" },
                { "data": "no_of_msg" },
                { "data": "amount" },
                { "data": "status" },
                { "data": "options" }
                
            ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
    });
</script>


@endsection
