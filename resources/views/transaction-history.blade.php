@extends('front_layouts.app')

@section('css')
<link href="{{asset('front2/css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet" />
<style type="text/css">
	.c1{
		color: #4308BF !important ;
	}
</style>
@endsection

@section('content')

<div class="transaction-history-sec apikey-sec text-center p-5 bg-white">
            <h2 class="heading1">Transaction History</h2>
            <p>All your billing data is available here...</p>
            <div class="col-lg-8 offset-2 mt-5">
                <table class="table table-striped transactionhistory-table" id="transaction_history">
                    <thead>
                        <tr>
                            <th class="c1">Sr. No.</th>
                            <th class="c1">Payment ID</th>
                            <th class="c1">Payment Amount</th>
                            <th class="c1">Payment Mode</th>
                        </tr>
                    </thead>
                    
                    <tbody> 
                        @php($count=1)
                        @foreach ($trahisdata as $transaction_history)
                        <tr>
                            <td scope="row">{{$count}}</td>
                            <td>{{$transaction_history->payment_id}}</td>
                            <td>{{$transaction_history->amount}}/-</td>
                            <td class="text-danger">{{$transaction_history->type}}</td>
                            
                        </tr>
                         @php($count++)
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="data-empty">
                <h2 class="heading1">Transaction History</h2>
                <p>All your billing data is available here...</p>
                <div class="mt-5 mb-5">
                    <img src="{{asset('front2/images/nodata.png')}}" />
                </div>
            </div>
        </div>

@endsection



@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
           $('#transaction_history').DataTable();
        });

      </script>


@endsection