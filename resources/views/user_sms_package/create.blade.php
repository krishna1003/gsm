@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">User SMS Package</h1>
            </div><!-- /.col -->
            <div class="col-sm-6"> 
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('user_sms_package') }}">User SMS Package</a></li>
                <li class="breadcrumb-item active">Create User SMS Package</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <!-- general form elements -->
                        <div class=" card-primary">

                            @if (!empty($UserSmsPackage))
                            <form method="post" action="{{ route('user_sms_package.update', $SmsPackage->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">User SMS Package Information:</h3>
                                
                               
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->name}}" id="name" name="name" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="no_of_msg">No. Of Msg: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->no_of_msg}}" id="no_of_msg" name="no_of_msg" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$SmsPackage->amount}}" id="amount" name="amount" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="details" name="details">{{$SmsPackage->details}}</textarea>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$SmsPackage->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('user_sms_package.store')}}" method="post" id="SmsPackageForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">User SMS Package Information:</h3>
                                  
                                    <div class="form-row">  
                                    

                                            <div class="form-group col-md-6">

                                                <label for="Category">User</label>
                                                <select class="form-control" id="userid" name="userid">
                                                    <option value="">Choose User</option>
                                                    @foreach ($users as $user)
                                                    <option value="{{$user->id}}"> {{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                             <div class="form-group col-md-6">

                                                <label for="Category">SMS Package</label>
                                                <select class="form-control" id="smspackageid" name="smspackageid">
                                                    <option value="">Choose SMS Package</option>
                                                    @foreach ($smspackages as $smspackage)
                                                    <option value="{{$smspackage->id}}" > {{ $smspackage->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                    </div>
                                    

                                    <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="no_of_msg">No. Of Msg: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="no_of_msg" name="no_of_msg" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="amount" name="amount" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">  
                                    

                                            <div class="form-group col-md-6">

                                                <label for="Category">Payment Type</label>
                                                <select class="form-control" id="type" name="type">
                                                    <option value="online" disabled="">Online</option>
                                                    <option value="offline">Offline</option>
                                                    
                                                </select>
                                                
                                            </div>
                                             <div class="form-group col-md-6">

                                                <label for="Category">Payment Status</label>
                                                <select class="form-control" id="status" name="status">
                                                    <option value="paid">Paid</option>
                                                    <option value="unpaid">Unpaid</option>
                                                </select>
                                                
                                            </div>
                                    </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Remark:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="details" name="details"></textarea>
                                    </div>
                                </div>
                                    
                                    
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    <!-- /.card -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('extrajs')
<!-- DataTables -->
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>

    $(function () {

    });

    
    $('#SmsPackageForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            
        },
        rules: {
            userid: "required",
            smspackageid: "required",
            details: "required",
            type: "required",
            status: "required",
            no_of_msg: "required",
            amount: "required"
            
        }
    });

    $('#smspackageid').change(function() {
            var cID = $(this).val();
                
            if (cID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-smspackage-list')}}?c_id=" + cID,
                    success: function(res) {
                        console.log(res);
                        console.log(res.length);
                        if (res) {
                            $("#no_of_msg").empty();
                            $("#amount").empty();
                            if(res.length == "0") {
                            $("#no_of_msg").val("");
                            $("#amount").val("");
                            
                        }else{
                            $.each(res, function(key, value) {
                                $("#no_of_msg").val(key);
                                $("#amount").val(value);
                            });
                        }
                            
                            
                        } else{
                            $("#no_of_msg").val("");
                            $("#amount").val("");
                        }
                    }

                });
            } else {
                $("#no_of_msg").val("");
                            $("#amount").val("");
                
            }
        });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('details');
    });

    
</script>



@endsection
