@extends('front_layouts.app')

@section('css')

@endsection

@section('content')

<div class="row no-gutter bg-white">
    <div class="col-lg-9 col-12">
        <div class="row no-gutter">
            <div class="col-lg-1 col-12"></div>
            <div class="col-lg-11 col-12">
                <div class="sms-panel login-sec">
                    <div class="tab-content p-3" id="pills-tabContent">
                        <ul class="nav nav-pills border-bottom pb-4 mt-4 mb-4" id="pills-tab" role="tablist">

                            <li class="nav-item">
                                <a class="<?=($activeTab == "sms-send" ? "active" : "");?>" id="pills-home-tab" data-toggle="pill" href="#sms-send" role="tab" aria-controls="sms-send" aria-selected="true">SMS Send</a>
                            </li>
                            <li class="nav-item">
                                <a class="<?=($activeTab == "address-book" ? "active" : "");?>" id="pills-profile-tab" data-toggle="pill" href="#address-book" role="tab" aria-controls="address-book" aria-selected="false">Address Book</a>
                            </li>
                        </ul>
                        <div class="tab-pane fade show  <?=($activeTab == "sms-send" ? "active" : "");?>" id="sms-send" role="tabpanel" aria-labelledby="pills-home-tab">
                            <ul class="nav nav-tabs border-bottom-0" id="sms-options" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?=($smstypetab == "singlesmstab" ? "active" : "");?>" id="sms-tab" data-toggle="tab" href="#sms" role="tab" aria-controls="sms" aria-selected="true">SMS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?=($smstypetab == "multismstab" ? "active" : "");?>" id="multiple-tab" data-toggle="tab" href="#multiple" role="tab" aria-controls="multiple" aria-selected="false">Multiple</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?=($smstypetab == "bulksmstab" ? "active" : "");?>" id="bulk-tab" data-toggle="tab" href="#bulk" role="tab" aria-controls="bulk" aria-selected="false">Bulk</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?=($smstypetab == "groupsmstab" ? "active" : "");?>"" id="group-tab" data-toggle="tab" href="#group" role="tab" aria-controls="group" aria-selected="false">Group</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade <?=($smstypetab == "singlesmstab" ? "show active" : "");?>" id="sms" role="tabpanel" aria-labelledby="sms-tab">

                                    <div class="row align-items-center">
                                        <div class="col-lg-6">
                                            @if ($message = Session::get('success_singlesmssend'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                            @if ($message = Session::get('error_singlesmssend'))
                                            <div class="alert alert-danger alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                            <form method="POST" action="{{ route('send.usersinglesms.request') }}">
                                                @csrf
                                                <div class="custom-control custom-select mb-4">
                                                    <label>SMS Type</label>
                                                    <select class="form-control pl-2 pr-2">
                                                        <option value="">Select sms type...</option>
                                                        <option value="">Personal sms</option>
                                                        <option value="">OTP sms</option>
                                                        <option value="">Flash sms</option>
                                                        <option value="">MMS</option>
                                                    </select>
                                                </div>
                                                <div class="custom-control mb-4 mobile-num">
                                                    @error('contact')
                                                    <label style="color: red;">{{ $message }}</label>
                                                    @else
                                                    <label>Enter mobile number</label>
                                                    @enderror
                                                    <span class="position-absolute pr-3">+91</span>
                                                    <input id="contact" name="contact" class="form-control" placeholder="Enter mobile number here..." />
                                                </div>
                                                <input type="hidden" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" >
                                                <div class="custom-control mb-4">
                                                    <label>Sender ID</label>
                                                    <select class="form-control pl-2 pr-2" id="senderid" name="senderid" readonly>
                                                        <option value="WEBSMS" selected>WEBSMS</option>
                                                    </select>
                                                </div>
                                                <div class="custom-control custom-select mt-0 mb-4">
                                                    <label>Select Language</label>
                                                    <select class="form-control pl-2 pr-2">
                                                        <option value="">Select Language</option>
                                                        <option value="">English</option>
                                                        <option value="">Gujarati</option>
                                                        <option value="">Hindi</option>
                                                    </select>
                                                </div>
                                                <div class="custom-control mb-2">
                                                    @error('message')
                                                    <label style="color: red;">{{ $message }}</label>
                                                    @else
                                                    <label>Write Message</label>
                                                    @enderror
                                                    <textarea class="custom-control w-100 pl-3 pt-3" rows="8" id="message" name="message" placeholder="Enter message here..."></textarea>
                                                </div>
                                                <div class="form-group send-option">
                                                    <nav>
                                                        <div class="nav nav-tabs border-bottom-0" id="nav-tab" role="tablist">
                                                            <a class="nav-item nav-link active position-relative mr-5" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Send Now</a>
                                                            <a class="nav-item nav-link position-relative" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Send Later</a>
                                                        </div>
                                                    </nav>
                                                    <div class="tab-content" id="nav-tabContent">
                                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"></div>
                                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="custom-control mb-2">
                                                                        <label>Sender ID</label>
                                                                        <input id='datetimepicker1' class="form-control" type='text' placeholder="Select sender ID..." />
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="custom-control mb-2">
                                                                        <label>Sender ID</label>
                                                                        <input id='datetimepicker2' type="text" class="form-control" placeholder="Select sender ID..." />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="custom-control">
                                                    @if(getWalletMessagge() != "0")
                                                    <button type="submit" class="btn theme-btn btn-block">Send SMS</button>
                                                    @else
                                                    <a href=""  class="btn theme-btn btn-block" style="    font-size: 18px;">Recharge Your Wallet</a>
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-6 text-center d-none d-lg-block">
                                            <img src="{{asset('front2/images/delivery-bg.png')}}" />
                                            <h2 class="mt-5">
                                                Instant Delivery, No.1 Service provider in India
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?=($smstypetab == "multismstab" ? "show active" : "");?>" id="multiple" role="tabpanel" aria-labelledby="multiple-tab">


                                    <div class="row align-items-center">
                                        <div class="col-lg-6">
                                            @if($message = Session::get('success_multismssend'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                            @if ($message = Session::get('error_multismssend'))
                                            <div class="alert alert-danger alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                            <form  method="post" action="{{ route('send.usermultisms.request') }}" >
                                             @csrf
                                             <div class="custom-control custom-select mb-4">
                                                <label>SMS Type</label>
                                                <select class="form-control pl-2 pr-2">
                                                    <option value="">Select sms type...</option>
                                                    <option value="">Personal sms</option>
                                                    <option value="">OTP sms</option>
                                                    <option value="">Flash sms</option>
                                                    <option value="">MMS</option>
                                                </select>
                                            </div>
                                            <input type="hidden" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" >
                                            <div class="custom-control mb-4 ">
                                                @error('contact')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                                <label>Enter mobile number</label>
                                                @enderror
                                                <input class="form-control" id="contact" name="contact"  placeholder="Enter mobile number here..." />
                                                <span style="font-size: 11px;color: red;">*Note: Multi Numers Between Use Coma , Operator</span>

                                            </div>

                                            <div class="custom-control mb-4">
                                                <label>Sender ID</label>
                                                <select class="form-control pl-2 pr-2" id="senderid" name="senderid" readonly>
                                                    <option value="WEBSMS" selected>WEBSMS</option>
                                                </select>
                                            </div>
                                            <div class="custom-control custom-select mt-0 mb-4">
                                                <label>Select Language</label>
                                                <select class="form-control pl-2 pr-2">
                                                    <option value="">Select Language</option>
                                                    <option value="">English</option>
                                                    <option value="">Gujarati</option>
                                                    <option value="">Hindi</option>
                                                </select>
                                            </div>
                                            <div class="custom-control mb-2">
                                                @error('message')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                                <label>Write Message</label>
                                                @enderror

                                                <textarea class="custom-control w-100 pl-3 pt-3" rows="8" id="message" name="message" placeholder="Enter message here..."></textarea>
                                            </div>
                                            <div class="form-group send-option">
                                                <nav>
                                                    <div class="nav nav-tabs border-bottom-0" id="nav-tab" role="tablist">
                                                        <a class="nav-item nav-link active position-relative mr-5" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Send Now</a>
                                                        <a class="nav-item nav-link position-relative" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Send Later</a>
                                                    </div>
                                                </nav>
                                                <div class="tab-content" id="nav-tabContent">
                                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"></div>
                                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="custom-control mb-2">
                                                                    <label>Sender ID</label>
                                                                    <input id='datetimepicker1' class="form-control" type='text' placeholder="Select sender ID..." />
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="custom-control mb-2">
                                                                    <label>Sender ID</label>
                                                                    <input id='datetimepicker2' type="text" class="form-control" placeholder="Select sender ID..." />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="custom-control">
                                                @if(getWalletMessagge() != "0")
                                                <button type="submit" class="btn theme-btn btn-block">Send SMS</button>
                                                @else
                                                <a href=""  class="btn theme-btn btn-block" style="    font-size: 18px;">Recharge Your Wallet</a>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-6 text-center d-none d-lg-block">
                                        <img src="{{asset('front2/images/delivery-bg.png')}}" />
                                        <h2 class="mt-5">
                                            Instant Delivery, No.1 Service provider in India
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade <?=($smstypetab == "bulksmstab" ? "show active" : "");?>" id="bulk" role="tabpanel" aria-labelledby="bulk-tab">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                        <div class="alert" id="message" style="display: none"></div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ asset('sample/bulk.xlsx') }}" class="btn btn-success">Sample File Download</a>
                                            </div>
                                        </div>
                                        <form role="form" id="uploadFile" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="custom-control mb-4 custom-file">
                                                <label for="customFile" >Add Excel</label>
                                                <input type="file" id="customFile" style="line-height: 53px" name="bulkFile" class="form-control" />
                                            </div>

                                            <div class="custom-control">
                                                <button type="submit" class="btn theme-btn btn-block">Upload</button>
                                            </div>
                                        </form>
                                        <form role="form" id="sendSMSForm" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="custom-control custom-select mb-4">
                                                <label>SMS Type</label>
                                                <select class="form-control pl-2 pr-2">
                                                    <option value="">Select sms type...</option>
                                                    <option value="">Personal sms</option>
                                                    <option value="">OTP sms</option>
                                                    <option value="">Flash sms</option>
                                                    <option value="">MMS</option>
                                                </select>
                                            </div>
                                            <input type="hidden" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" >
                                            <div class="custom-control mb-4">
                                                <label>Sender ID</label>
                                                <select class="form-control pl-2 pr-2" id="senderid" name="senderid" readonly>
                                                    <option value="LCROWN" selected>LCROWN</option>
                                                </select>
                                            </div>
                                            <div class="custom-control custom-select mt-0 mb-4">
                                                <label>Select Language</label>
                                                <select class="form-control pl-2 pr-2">
                                                    <option value="">Select Language</option>
                                                    <option value="">English</option>
                                                    <option value="">Gujarati</option>
                                                    <option value="">Hindi</option>
                                                </select>
                                            </div>
                                            <div class="custom-control mb-2" id="bulkMsg">




                                            </div>
                                            <div class="form-group send-option">
                                                <nav>
                                                    <div class="nav nav-tabs border-bottom-0" id="nav-tab" role="tablist">
                                                        <a class="nav-item nav-link active position-relative mr-5" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Send Now</a>
                                                        <a class="nav-item nav-link position-relative" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Send Later</a>
                                                    </div>
                                                </nav>
                                                <div class="tab-content" id="nav-tabContent">
                                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"></div>
                                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="custom-control mb-2">
                                                                    <label>Sender ID</label>
                                                                    <input id='datetimepicker1' class="form-control" type='text' placeholder="Select sender ID..." />
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="custom-control mb-2">
                                                                    <label>Sender ID</label>
                                                                    <input id='datetimepicker2' type="text" class="form-control" placeholder="Select sender ID..." />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="custom-control">
                                                <button type="submit" class="btn theme-btn btn-block">Send SMS</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-lg-6 text-center d-none d-lg-block">
                                        <img src="{{asset('front2/images/delivery-bg.png')}}" />
                                        <h2 class="mt-5">
                                            Instant Delivery, No.1 Service provider in India
                                        </h2>
                                    </div>
                                    <section class="content container"></section>
                                </div>
                            </div>
                            <div class="tab-pane fade <?=($smstypetab == "groupsmstab" ? "show active" : "");?>" id="group" role="tabpanel" aria-labelledby="group-tab">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                     @if($message = Session::get('success_groupsmssend'))
                                     <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    @if ($message = Session::get('error_groupsmssend'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    <form action="{{ route('send.usergroupsms.request') }}" method="post" name="groupsmsform" id="groupsmsform">
                                        @csrf
                                        <div class="custom-control custom-select mb-4">
                                            @error('smsgroupid')
                                            <label style="color: red;">{{ $message }}</label>
                                            @else
                                            <label>Select Group</label>
                                            @enderror
                                            <select class="form-control pl-2 pr-2" id="smsgroupid" name="smsgroupid" required="" >

                                                <option value="">Select Group...</option>
                                                @foreach ($groups as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="group_contact_list"></div>
                                        <input type="hidden" class="form-control" id="campaign" name="campaign" value="{{ getCampignName() }}" >
                                        <div class="custom-control mb-4">
                                            <label>Sender ID</label>
                                            <select class="form-control pl-2 pr-2" id="senderid" name="senderid" readonly>
                                                <option value="WEBSMS" selected>WEBSMS</option>
                                            </select>
                                        </div>

                                        <div class="custom-control custom-select mt-0 mb-4">
                                            <label>Select Language</label>
                                            <select class="form-control pl-2 pr-2">
                                                <option value="">Select Language</option>
                                                <option value="">English</option>
                                                <option value="">Gujarati</option>
                                                <option value="">Hindi</option>
                                            </select>
                                        </div>
                                        <div class="custom-control mb-2">
                                            @error('message')
                                            <label style="color: red;">{{ $message }}</label>
                                            @else
                                            <label>Write Message</label>
                                            @enderror
                                            <textarea class="custom-control w-100 pl-3 pt-3" rows="8" id="message" name="message"  placeholder="Enter message here..."></textarea>
                                        </div>
                                        <div class="form-group send-option">
                                            <nav>
                                                <div class="nav nav-tabs border-bottom-0" id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link active position-relative mr-5" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Send Now</a>
                                                    <a class="nav-item nav-link position-relative" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Send Later</a>
                                                </div>
                                            </nav>
                                            <div class="tab-content" id="nav-tabContent">
                                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"></div>
                                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="custom-control mb-2">
                                                                <label>Sender ID</label>
                                                                <input id='datetimepicker1' class="form-control" type='text' placeholder="Select sender ID..." />
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="custom-control mb-2">
                                                                <label>Sender ID</label>
                                                                <input id='datetimepicker2' type="text" class="form-control" placeholder="Select sender ID..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="custom-control">
                                            <button type="submit" class="btn theme-btn btn-block">Send SMS</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-lg-6 text-center d-none d-lg-block">
                                    <img src="{{asset('front2/images/delivery-bg.png')}}" />
                                    <h2 class="mt-5">
                                        Instant Delivery, No.1 Service provider in India
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="tab-pane fade show <?=($activeTab == "address-book" ? "active" : "");?>" id="address-book" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="row align-items-center">
                        <div class="col-lg-6">

                            @if ($message = Session::get('success_single_contact_add'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('success_group_add'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('success_multi_contact_add'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('error_multiple_contact_add'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <div id="single-contacts" style="<?=($tab == "multi" ? "display:none" : "display: block;");?>;">
                                @if(isset($contact))
                                <form method="post" action="{{ route('user.contacts.update',$contact_update_id) }}" >
                                 @else
                                 <form method="post" action="{{route('user.contacts.create.request', isset($contact) ? $contact->id : '') }} ">
                                    @endif
                                    @csrf
                                    <div class="custom-control mb-4">
                                        @error('name')
                                        <label style="color: red;">{{ $message }}</label>
                                        @else
                                        <label>Enter Name</label>
                                        @enderror
                                        <input class="form-control" placeholder="Enter name here..." id="name" name="name" value="{{ $contact->name ?? ''}}" />
                                    </div>
                                    @if(!isset($contact))
                                    <b class="theme-color d-block pb-3 text-right addmultiplecontacts" style="cursor:pointer;">Add Multiple Contacts </b>
                                    @endif
                                    <div class="custom-control mb-4 mobile-num">
                                        @error('contact')
                                        <label style="color: red;">{{ $message }}</label>
                                        @else
                                        <label>Enter Mobile Number</label>
                                        @enderror

                                        <span class="position-absolute pr-3">+91</span>
                                        <input class="form-control" placeholder="Enter mobile number here..." id="contact" name="contact" value="{{ $contact->contact ?? ''}}" />
                                    </div>
                                    @if(!isset($contact))
                                    <div class="custom-control mb-4 custom-file">
                                        <label for="customFile">Add Excel</label>
                                        <input type="file" id="customFile" class="form-control" />
                                    </div>
                                    @endif
                                    <div class="custom-control">
                                        <button type="submit" class="btn theme-btn btn-block">{{ isset($contact) ? 'Update' : 'Add' }} Contact</button>
                                        @if(isset($contact))

                                        <button type="reset" class="btn  btn-block" onclick="location.href = '{{route('addressindex')}}';"> Cancel</button>

                                        @endif
                                    </div>
                                </form>
                            </div>

                            <div id="multiple-contacts"  style="<?=($tab == "multi" ? "display:block" : "display: none;");?>;">
                                <form method="post" id="ml3" action="{{route('user.multiplecontacts.create.request')}} ">
                                    @csrf


                                    <p>
                                        Enter contact name followed by mobile number
                                        separated with comma. You can save upto 50 contacts
                                        in one go.
                                    </p>


                                    <div class="custom-control mb-4">
                                        @error('textarea')
                                        <label style="color: red;">{{ $message }}</label>
                                        @else
                                        <label class="mlc">Your Contacts</label>
                                        @enderror

                                        <textarea class="w-100 p-3" id="myMso" name="textarea" placeholder="Dev , 919xxxxxxxxx &#10;Ajay , 919xxxxxxxxx&#10;Vijay , 919xxxxxxxxx&#10;Dhanjay , 919xxxxxxxxx&#10;" rows="8" cols="50" required=""></textarea>
                                        <b class="theme-color d-block text-right pt-3 addsinglecontacts" style="cursor:pointer;">Add Single Contact </b>
                                    </div>
                                    <div class="custom-control mb-4">
                                        <button type="submit" class="btn theme-btn btn-block"> Add Contact</button>
                                    </div>
                                </form>
                            </div>

                            <div id="group-contacts"  style="display: none;">
                                <form method="post" name="creategroup" id="creategroup" action="{{route('contact.usergroup.create')}} ">
                                    @csrf

                                    <div class="custom-control mb-4">
                                        @error('name')
                                        <label style="color: red;">{{ $message }}</label>
                                        @else
                                        <label>Enter Group Name</label>
                                        @enderror
                                        <input class="form-control" placeholder="Enter name here..." id="groupname" name="groupname" value="" />
                                        <input type="hidden" class="form-control" placeholder="Enter name here..." id="g_contactid" name="g_contactid" value="" />

                                        <input type="hidden" class="form-control" placeholder="Enter name here..." id="g_group" name="g_group" value="" />
                                        <input type="hidden" class="form-control"  id="grouptype" name="grouptype" value="" />
                                    </div>


                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Mobile Number</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="grpMultipleContactsTable">

                                        </tbody>
                                    </table>
                                    <div class="custom-control mb-4">
                                        <button type="submit" id="sugroup" class="btn theme-btn btn-block"> Add Group</button>

                                        <button type="reset" class="btn  btn-block" onclick="location.href = '{{route('addressindex')}}';"> Cancel</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="col-lg-6 text-center">
                            <img src="{{asset('front2/images/delivery-bg.png')}}" />
                            <h2 class="mt-5">
                                Instant Delivery, No.1 Service provider in India
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="col-lg-3 col-12 p-0">
    <div class="rightside-contact border-left">
        <ul class="nav nav-pills row no-gutter" role="tablist">
            <li class="nav-item col-6">
                <a class="nav-link active text-center rounded-0" id="contact-tab" data-toggle="pill" href="#contact-num" role="tab" aria-controls="pills-home" aria-selected="true">Contacts</a>
            </li>
            <li class="nav-item col-6">
                <a class="nav-link text-center rounded-0" id="group-tab" data-toggle="pill" href="#group-num" role="tab" aria-controls="pills-profile" aria-selected="false">Groups</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="contact-num" role="tabpanel" aria-labelledby="pills-home">
                <div class="search d-flex align-items-center">
                    <span class="pr-3"><img src="{{asset('front2/images/search-icon.svg')}}" /></span>
                    <input type="text" class="border-0 bg-transparent w-100" id="search" name="search" placeholder="Search contact" />
                </div>
                <ul class="contact-list" id="c_list">
                 @foreach ($contacts as $contact_list)
                 <li class="media" onclick="selectContact('{{$contact_list->contact}}','{{$contact_list->name}}','{{$contact_list->id}}');" style="cursor: pointer;">
                    <span class="mr-3 cfl_" id="cfl_{{$contact_list->contact}}">{{ ucfirst($contact_list->name[0]) }}</span>
                    <span class="mr-3 check_" id="check_{{$contact_list->contact}}" style="display: none;"><img style="padding: 4px;"   src="{{asset('front2/images/iconmonstr-check-mark-16.svg')}}" /></span>
                    <div class="media-body row no-gutter align-items-center">
                        <div class="col-10">
                            <h5 class="mt-0 mb-1">{{$contact_list->name}}</h5>
                            {{$contact_list->contact}}
                        </div>
                        <div class="col-2 d-flex justify-content-between action-btn <?=($activeTab == "address-book" ? "active" : "");?>">
                            <a href="{{route('user.contacts.edit', $contact_list->id)}}"><img src="{{asset('front2/images/edit.svg')}}" /></a>
                            <a href="javascript:;" data-toggle="modal" onclick="deleteData('{{$contact_list->id}}' )" data-target="#DeleteModal" ><img src="{{asset('front2/images/delete.svg')}}" /></a>
                        </div>
                    </div>
                </li>
                @endforeach

            </ul>
        </div>
        <div class="tab-pane fade" id="group-num" role="tabpanel" aria-labelledby="pills-profile">
            <div class="search d-flex align-items-center">
                <span class="pr-3"><img src="{{asset('front2/images/search-icon.svg')}}" /></span>
                <input type="text" id="searchgroup" name="searchgroup" class="border-0 bg-transparent w-100" placeholder="Search Group " />
            </div>
            <ul class="contact-list" id="g_list">
                @foreach ($groups as $group)
                <li class="media">
                    <span class="mr-3">{{ ucfirst($group->name[0]) }}</span>
                    <div class="media-body row no-gutter align-items-center">
                        <div class="col-10">
                            <h5 class="mt-0 mb-1">{{$group->name}}</h5>
                            +91 87802 00828
                        </div>
                        <div class="col-2 d-flex justify-content-between action-btn <?=($activeTab == "address-book" ? "active" : "");?>">
                            <a href="#" onclick="Editgroupcontact('{{$group->id}}');"><img src="{{asset('front2/images/edit.svg')}}" /></a>
                            <a href="{{route('user.groups.delete', $group->id)}}"><img src="{{asset('front2/images/delete.svg')}}" /></a>
                        </div>
                    </div>
                </li>
                @endforeach

            </ul>
        </div>
    </div>



</div>
</div>
</div>

<div id="DeleteModal" class="modal fade text-danger" role="dialog">
   <div class="modal-dialog ">
     <!-- Modal content-->
     <form action="" id="deleteForm" method="post">
         <div class="modal-content">
             <div class="modal-header bg-danger">
                <h4 class="modal-title text-center" style="font-weight: 600;color: antiquewhite;">DELETE CONFIRMATION</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
             {{ csrf_field() }}
             <p class="text-center">Are You Sure Want To Delete Contact ?</p>
         </div>
         <div class="modal-footer">
             <center>
                 <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                 <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button>
             </center>
         </div>
     </div>
 </form>
</div>
</div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
<script src="{{asset('front2/js/plugin/moment.js')}}"></script>
<script src="{{asset('front2/js/main.js')}}"></script>
<script src="{{asset('front2/js/plugin/bootstrap-datetimepicker.min.js')}}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">

    function nl2br (str, is_xhtml) {
      var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
      return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    }


    $.validator.addMethod('ValidEmail', function(value, element) {
      var txt = 're';
      var validator = this;
      // var errors = {};
      // errors[element.name] =  '';
      // validator.showErrors(errors);

      //console.log(value);
      var addmult = nl2br(value);
      console.log("addmult==>",addmult);
      var data1 = addmult.split('<br/>');
      console.log("data1==>",data1);
      for (var i = 0; i < data1.length; i++){
        console.log(data1[i]);
        line = data1[i].split(',');
        var ll = line.length;
        console.log(ll);
        if(ll != 2){
          errors = {};
          errors[element.name] = 'Invalid Format';
          validator.showErrors(errors);
          return true;
        }else{
          console.log("line==>",line);
          var name1 = line[0];
          var name = name1.replace(/\s/g, '');
          var contactno1 = line[1];
          console.log("contactno1",contactno1);
          if(contactno1 !== undefined){
            var contactno = contactno1.replace(/\s/g, '');
            console.log(name);
            var letters = /^[a-zA-Z\s]+$/;
            var isValid = letters.test(name);
            if(!isValid){
                errors = {};
              errors[element.name] =  'do not enter number in name';
              validator.showErrors(errors);
              return true;
            }
            console.log(contactno1);

            var letters1 = /^\d{10}$/;
            var isValid1 = letters1.test(contactno);
            if(!isValid1){
              errors = {};
              errors[element.name] =  'Invalid contact no';
              validator.showErrors(errors);
              return true;
              console.log('ll');
            }
          }else{
            console.log('else');
            errors = {};
            errors[element.name] =  'Invalid Format';
            validator.showErrors(errors);
            return true;
          }
        }


        //return false;

      }
      console.log('false');
      return value;
    }, '');
    $('#ml3').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
        },
        rules:{
            textarea:{
                required:true,
                ValidEmail:true
            },

        }
    });


    jQuery(document).ready(function () {
        $("#header").load("header-main.html");
        $("#footer").load("footer.html");
        $('#datetimepicker1').datetimepicker({
            locale: 'ru'
        });
        @if($activeTab == "sms-send")
        $('#c_list .media').each(function (){
            this.style.pointerEvents = 'none';
        });
        @endif
    });
    $(".addmultiplecontacts").click(function () {
        $("#multiple-contacts").slideDown();
        $("#single-contacts").slideUp();
    });
    $(".addsinglecontacts").click(function () {
        $("#single-contacts").slideDown();
        $("#multiple-contacts").slideUp();
    });
    $('#pills-profile-tab').click(function () {
        $(".contact-list .action-btn").addClass('active');
            //$("#c_list .media").prop("onclick", null).off("click");
            //document.getElementByClass('media').style.pointerEvents = 'none';
            $('#c_list .media').each(function (){
                this.style.pointerEvents = 'auto';
            });
        });

// To re-enable:

$('#pills-home-tab').click(function () {
    $(".contact-list .action-btn").removeClass('active');
            //$("#c_list .media").prop("onclick", null).on("click");
            $('#c_list .media').each(function (){
                this.style.pointerEvents = 'none';
            });

        });
    </script>
    <script type="text/javascript">
     function deleteData(id)
     {
         var id = id;
         var url = '{{ route("user.contacts.delete", ":id") }}';
         console.log(url);
         url = url.replace(':id', id);
         $("#deleteForm").attr('action', url);
     }

     function formSubmit()
     {
         $("#deleteForm").submit();
     }
 </script>
 <script type="text/javascript">
    $('#creategroup').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');

        },
        rules: {
            groupname: "required",

        }
    });
</script>


<script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script type="text/javascript">
    /*$("#myMso").on('change',function(){
            var value = $(this).val();
            var word = value.split(",");
           // console.log(word[0]);
              //  console.log(word[1]);
            $.each(word,function (key,value) {
                //console.log(key);
                console.log(word[0]);
                console.log(word[1]);
                if(value.length != 10){
                    //alert("Hey! some of the contact number you have entered is not valid");
                }

            });
        });*/
    </script>
    <script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>

        $('#smsgroupid').on('change',function(){
            $groupsmscontact = [];
            var value=$(this).val();
            console.log(value);
            $.ajax({
                type : 'get',
                url : '{{url("smsgroupid/select")}}',
                data:{'id':value},
                success:function(data){
                    console.log(data);
                    $('#group_contact_list').html(data);

                    var str = $('#smsgroupcontact').val();
                    var groupsmscontact = str.split(',');
                    for (var i = 0; i < groupsmscontact.length; i++) {
                        console.log(groupsmscontact[i]);
                    }

                    console.log(groupsmscontact);

                    $(".cancelDeletegroupsmsContact").click(function () {
                        var id = $(this).attr('id').split("_");
                        var cmobile = id[1];

                        var ind = groupsmscontact.indexOf(cmobile);
                        groupsmscontact.splice(ind, 1);

                        $('#c_'+cmobile).parent().hide();
                        console.log(groupsmscontact.length);
                        var smsgroupcontactid = groupsmscontact.join();
                        console.log(smsgroupcontactid);
                        $('#smsgroupcontact').val(smsgroupcontactid)
                        if(groupsmscontact.length == 0)
                        {
                          $('#smsgroupid').val('') ;
                          $('#group_contact_list').html('') ;
                      }

                  });

                    $(document).ready(function() {
                        $('#t123').DataTable( {
                            "scrollY":        "200px",
                            "scrollCollapse": true,
                            "paging":         false,searching: false,
                            ordering:  false,"info":     false
                        } );
                    } );
                }
            });
        })




        $(".cancelDeletegroupsmsContact").click(function () {
            var id = $(this).attr('id').split("_");
            var cmobile = id[1];

            var ind = groupsmscontact.indexOf(cmobile);
            groupsmscontact.splice(ind, 1);

            $('#c_'+cmobile),parent().hide();

        });

        $(function() {

            $("#uploadFile").show();
            $("#sendSMSForm").hide();


            $('#uploadFile').on('submit', function(event){
                event.preventDefault();
                $.ajax({
                    url:"{{ route('load.bulkview.request') }}",
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(res)
                    {
                        if((res.status == 'success') && (res.code == 200)) {
                            console.log("Success response :: ", res.data);
                            var responseData = res.data;
                            $('#message').css('display', 'block');
                            $('#message').addClass('alert-success');
                            $('.alert-success').delay(7000).fadeOut();
                            $('#message').html(res.msg);

                            $("#uploadFile").hide();
                            $("#sendSMSForm").show();

                            var headers = responseData.bulkdata.header;
                            var body = responseData.bulkdata.body;

                            console.log("check headers : ", headers.length);
                            if(headers.length <= 2) {
                                console.log("Correct two fiels");
                                $("#bulkMsg").html(`
                                    <label for="message">Message</label>
                                    <textarea class="custom-control w-100 pl-3 pt-3" rows="8" id="message" name="message" required></textarea>
                                    `);
                            }
                        // console.log("Final Check :: ", responseData.htmlview);

                        $('.container').html(responseData.htmlview);

                    }else {
                        console.log("Error response :: ", res.status +  res.code);
                        $('#message').css('display', 'block');
                        $('#message').addClass('alert-danger');
                        $('.alert-danger').delay(5000).fadeOut();
                        $('#message').html('');
                        $.each(res.msg, function( key, value ) {
                            console.log("ERROR ", value);
                            $("#message").append('<li>'+value+'</li>');
                        });
                    }

                }
            })
            });

            $('#sendSMSForm').on('submit', function(event){
                event.preventDefault();
                console.log("selected bulk Data ", selectedRows);

                if(selectedRows.length == 0) {
                    alert("select SMS Type ");
                    $('#smsMessage').show();
                    $('#smsMessage').delay(5000).fadeOut();
                    return false;
                }else{
                    smsdata = JSON.stringify(selectedRows);;
                }

                var formData = new FormData(this);
                formData.append('filedata', smsdata);
                formData.append('fileuuid', fileuuid);

                console.log("selected Messages ::::: ", formData);

                $.ajax({
                    url:"{{ route('send.bulksms.request') }}",
                    method:"POST",
                    data:formData,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(res) {
                        console.log("response :: ", res);
                        if((res.status == 'success') && (res.code == 200)) {


                            window.location.href = "{{ route('bulksmsindex') }}";
                        }
                    }
                });
            });


        });

function Editgroupcontact(groupid)
{

    selectedContacts = [];
    selectedNames = [];
    selectedIds = [];
    $(".check_").hide();
    $(".cfl_").show();



    console.log(groupid);
    $.ajax({
        type : 'get',
        url : '{{url("usercontact/editgroupcontact")}}',
        data:{'groupid':groupid},
        success:function(data){
            console.log(data);

            var obj = JSON.parse(data);
    //var selectedContacts = []

    if(obj.length > 0){
        for (var i=0;i<obj.length;i++){
            console.log(obj[i].id);
            console.log(obj[i].contactid);

            var cmobile = obj[i].contact;
            var cname = obj[i].name;
            var cid = obj[i].contactid;
            var gname = obj[i].groupname;

            selectContact(cmobile, cname, cid)
        }

        $('#groupname').val(gname);
        $('#grouptype').val('u');
        $('#g_group').val(groupid);
        $('#sugroup').html('Update Group')

            //Now populate the second dropdown i.e "Sub Category"

        }


        console.log(selectedContacts);






   // console.log(res['id']);
//$('#c_list').html(data);
}
});


}

    /*Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};*/
var selectedContacts = [];
var selectedNames = [];
var selectedIds = [];
function selectContact(cmobile,cname,cid) {
    //$("#groupNamesAdd,#createContactDiv").hide();
    //alert(cmobile);
    console.log(selectedContacts.includes(cmobile));
    console.log(selectedContacts.length);

    if(selectedContacts.includes(cmobile) == false)
    {
       selectedContacts.push(cmobile);
       selectedNames.push(cname);
       selectedIds.push(cid);
       $("#check_" + cmobile).show();
       $("#cfl_" + cmobile).hide();
   }else
   {
       //selectedContacts.remove('seven');
       var ind = selectedContacts.indexOf(cmobile);
       var nind = selectedNames.indexOf(cname);
       var cind = selectedIds.indexOf(cid);
       selectedContacts.splice(ind, 1);
       selectedNames.splice(nind, 1);
       selectedIds.splice(cind, 1);

       $("#check_" + cmobile).hide();
       $("#cfl_" + cmobile).show();
   }

   if(selectedContacts.length > 0)
   {


       $("#single-contacts").hide();
       $("#multiple-contacts").hide();
       $("#group-contacts").show();

       var mdlDiv = '';
       for (var i = 0; i < selectedContacts.length; i++) {
        var contactName = selectedNames[i];
        var contactid = selectedIds[i];
        mdlDiv += '<tr>';
        mdlDiv += '<td>' + contactName + '</td>';
        mdlDiv += '<td>' + selectedContacts[i] + '</td>';
        mdlDiv += '<td class="cancelDeleteContact" id="c_' + selectedContacts[i] + '_' + contactName + '_' + contactid + '"><img src="{{asset('front2/images/delete.svg')}}"></td>';

        mdlDiv += '</tr>';
    }
    var sc = selectedContacts.join();
    var scid = selectedIds.join();
    console.log(sc);
    console.log(scid);
    $("#grpMultipleContactsTable").html(mdlDiv);
    $("#g_contactid").val(scid);
    $(".cancelDeleteContact").click(function () {
        var id = $(this).attr('id').split("_");
        var cmobile = id[1];
        var cname = id[2];
        var cid = id[3];

        selectContact(cmobile, cname, cid)
    });
}else
{
    $("#single-contacts").show();
    $("#group-contacts").hide();
}








console.log(selectedContacts)
    //console.log(cmobile.length )

    //console.log(cmobile)
    //console.log(selectedContacts.indexOf(cmobile))



}






</script>

<script type="text/javascript">
    $('#search').on('keyup',function(){
        var value=$(this).val();
        console.log(value);
        $.ajax({
            type : 'get',
            url : '{{url("usercontact/search")}}',
            data:{'search':value},
            success:function(data){
                console.log(data);
                $('#c_list').html(data);

                for (var i = 0; i < selectedContacts.length; i++) {
                    var contactss = selectedContacts[i];
                    $("#check_" + contactss).show();
                    $("#cfl_" + contactss).hide();
                }
            }
        });
    })

    $('#searchgroup').on('keyup',function(){
        var value=$(this).val();
        console.log(value);
        $.ajax({
            type : 'get',
            url : '{{url("usercontactgroup/search")}}',
            data:{'search':value},
            success:function(data){
                console.log(data);
                $('#g_list').html(data);
            }
        });
    })
</script>
@endsection