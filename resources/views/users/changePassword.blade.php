@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Change Password</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                <li class="breadcrumb-item active">Change Password</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                
                <div class="card-body">
                    <!-- general form elements -->

                        <div class="card card-primary">
                    <form method="POST" action="{{ route('change.password') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="">
                            
                                    <div class="card-header">Change password</div>
                                    <div class="card-body row">
                                         

                                        <div class="form-group col-md-4">
                                            <label>Old Password</label>
                                            <input type="password" name="current_password" id="password" class="form-control" placeholder="Enter Current Password" required>
                                            {!! $errors->first('current_password', '<p class="help-block" style="color: #d81212;font-weight: 600;">:message</p>') !!}
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>New Password</label>
                                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter New Password" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            {!! $errors->first('new_password', '<p class="help-block" style="color: #d81212;font-weight: 600;">:message</p>') !!}
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Confirm Password</label>
                                            <input type="password" name="new_confirm_password" id="new_confirm_password" class="form-control" placeholder="Enter Confirmed Password" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            {!! $errors->first('new_confirm_password', '<p class="help-block" style="color: #d81212;font-weight: 600;">:message</p>') !!}
                                        </div>
                                        
                                    </div>
                                

                            
                            
                            <div class="col-md-12 text-center">
                            	<div class="card-header">
				                    
				                    <button class="btn btn-primary" type="submit" value="Submit Form" style="float: right;">Update Password</button>
				                </div>
                                

                            </div>



                        </div>
                    </form>
                 </div>
                    <!-- /.card -->
                </div>
                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('extrajs')
   
       
    
    
@endsection
