@extends('layouts.app')

@section('extracss')

@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Edit Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                <li class="breadcrumb-item active"> Edit Profile</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                
                <div class="col-lg-12  d-flex align-self-stretch">
                    <form method="POST" action="{{ route('aprofile.update.request', Auth::user()->id) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-12 d-flex align-items-stretch">
                                <div class="card w-100">
                                    <div class="card-header">User Details</div>
                                    <div class="card-body row">

                                        <div class="form-group col-md-6">
                                            <label>Company Name</label>
                                            <input type="text" name="cname" id="cname" class="form-control" placeholder="Enter Comapny Name"  value="{{ $user->cname ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Company Employee</label>
                                            <input type="text" name="emp_size" id="emp_size" class="form-control" placeholder="Enter Employee Size"  value="{{ $user->emp_size ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label> Name</label>
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter  Name"  value="{{ $user->name ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Mobile</label>
                                            <input type="text" name="contactno" id="contactno" class="form-control" readonly placeholder="Enter Mobile Number"  value="{{ $user->contactno ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        

                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input type="email" name="email" readonly id="email" class="form-control" placeholder="Enter Email ID" value="{{ $user->email ?? ''}}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        
                                        
                                        <div class="form-group col-md-6">
                                            <label for="Pincode"> DOB </label>
                                            <input class="form-control" id="dob" type="date" name="dob" placeholder="Select DOB" value="{{ $user->dob ?? ''}}" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 d-flex align-items-stretch">
                                <div class="card w-100">
                                    <div class="card-header">Location Details</div>
                                    <div class="card-body row">
                                        
                                        <div class="form-group col-md-4">
                                            <label>Country</label>
                                            <select class="form-control" name="country" id="country" required="" >
                                                @foreach ($countries as $country)
                                                <option value="{{$country->id}}" {{ isset($user) ? (($user->country == $country->id) ? 'selected' : '') : '' }}> {{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>State</label>
                                            <select class="form-control" name="state" id="state" required="" >
                                                @foreach ($states as $state)
                                                <option value="{{ $state->id }}" {{ isset($user) ? (($user->city == $state->id) ? 'selected' : '') : '' }}>{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>City</label>
                                            <select class="form-control" name="city" id="city" required="" >
                                                @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" {{ isset($user) ? (($user->city == $city->id) ? 'selected' : '') : '' }}>{{ $city->name }}</option>
                                                @endforeach

                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        
                                        

                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12 text-center">
                            	<div class="card-header">
				                    
				                    <button class="btn btn-primary" type="submit" value="Submit Form" style="float: right;">Save</button>
				                </div>
                                

                            </div>



                        </div>
                    </form>
                </div>
                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('extrajs')
   
       <script>
    $(document).ready(function() {
         

        $('#country').change(function() {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-state-list1')}}?country_id=" + countryID,
                    success: function(res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $.each(res, function(key, value) {
                                $("#state").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        } else {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#state").append('<option value="">Choose State</option>');
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }
        });

        $('#state').on('change', function() {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list1')}}?c_id=" + stateID,
                    success: function(res) {
                    	//console.log(res);
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }

        });

    });

    function showStates(countryId) {
        console.log("country ID :: ", countryId);

        if (countryId) {
            $.ajax({
                type: "GET",
                url: "{{url('get-state-list1')}}?country_id=" + countryId,
                success: function(res) {
                    if (res) {
                        $("#state").empty();
                        $("#state").append('<option>Choose State</option>');
                        $.each(res, function(key, value) {
                            $("#state").append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        $("#state").empty();
                        $("#city").empty();
                    }
                }
            });
        }

    }

    function showCities(stateId) {
        console.log('State Id :: ', stateId);

        if (stateId) {
            $.ajax({
                type: "GET",
                url: "{{url('get-city-list1')}}?state_id=" + stateId,
                success: function(res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option>Choose State</option>');
                        $.each(res, function(key, value) {
                            $("#city").append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        $("#city").empty();
                    }
                }
            });
        } else {
            $("#city").empty();
        }
    }
</script>
    
@endsection
