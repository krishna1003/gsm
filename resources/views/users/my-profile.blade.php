@extends('layouts.app')

@section('extracss')
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="Javascript::void(0)">Users</a></li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
            <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    
                    <div class="pt-2">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show <?= ($activeTab == "profile" ? "active" : ""); ?>" id="profile" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            
                                            <div class="col-md-12 text-center">
                                <div class="card-header">
                                    <h4 style="float: left;">My Profile</h4>
                                    <a href="{{ route('aprofile.update')}}" style="float: right;" class="btn btn-primary">Edit Profile</a>

                                     <a href="{{ route('changepassword')}}" style="float: right;" class="btn btn-primary">Change password</a>
                                    
                                </div>
                                

                            </div>
                                            <div class="card-body">

                                                <div class="changeprofile row">
                                                    <div class="col-md-2">
                                                        <img onError="this.onerror=null;this.src='{{asset("user (1).png")}}';"  style="width:150px" alt="Saraswatha Granules" class="img-responsive ng-star-inserted" src="{{ asset('profiles/'.Auth::user()->profilepic)}}">
                                                    </div>
                                                    <div class="col-md-3 ml-3">
                                                        <h4>{{ Auth::user()->email }}</h4>
                                                        <h4>{{ Auth::user()->contactno }}</h4>
                                                        <div class="mute">{{ getRole() }}</div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">User Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-4 col-form-label">Name</label>
                                                    <label class="col-8 col-form-label"> : {{ Auth::user()->name }}</label>
                                                    <label class="col-4 col-form-label">Mobile</label>
                                                    <label class="col-8 col-form-label">: {{ Auth::user()->contactno }}</label>
                                                    <label class="col-4 col-form-label">Email</label>
                                                    <label class="col-8 col-form-label">: {{ Auth::user()->email }}</label>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Address Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    
                                                    <label class="col-3 col-form-label">Country</label>
                                                    <label class="col-9 col-form-label">: {{ getCountry() }}</label>
                                                    <label class="col-3 col-form-label">State</label>
                                                    <label class="col-9 col-form-label">: {{getState()}}</label>
                                                    <label class="col-3 col-form-label">City</label>
                                                    <label class="col-9 col-form-label">: {{getCity()}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection


@section('extrajs')
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<script>

</script>
@endsection
