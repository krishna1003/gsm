@extends('front_layouts.app')

@section('css')

@endsection

@section('content')

<div class="bg-white p-lg-0 p-3">
            <div class="row no-gutter">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="sms-panel login-sec">
                        <div class="tab-content" id="pills-tabContent">
                            <ul class="nav nav-pills border-bottom pb-4 mt-4 mb-4" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="<?= ($activeTab == "personal_details" ? "active" : ""); ?>" id="pills-home-tab" data-toggle="pill" href="#personal-details" role="tab" aria-controls="personal-details" aria-selected="true">Personal Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="<?= ($activeTab == "accountinfo" ? "active" : ""); ?>" id="pills-profile-tab" data-toggle="pill" href="#account-info" role="tab" aria-controls="account-info" aria-selected="false">Account Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="<?= ($activeTab == "changepassword" ? "active" : ""); ?>" id="pills-profile-tab" data-toggle="pill" href="#reset-password" role="tab" aria-controls="reset-password" aria-selected="false">Reset Password</a>
                                </li>
                            </ul>
                            <div class="tab-pane fade show <?= ($activeTab == "personal_details" ? "active" : ""); ?>" id="personal-details" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="row align-items-center mb-5">
                                    <div class="col-md-5">
                                        @if ($message = Session::get('success_userdetails'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                    <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                        <form method="POST" action="{{ route('userupdatedetails.update.request', Auth::user()->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="custom-control mb-4">
                                                
                                                @error('name')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>Full Name</label>
                                                @enderror
                                                <input class="form-control bg-transparent" name="name" id="name" value="{{ $user->name ?? '' }}" placeholder="" required />
                                            </div>
                                            <div class="custom-control mb-4">
                                                @error('dob')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>Date of Birth</label>
                                                @enderror
                                                
                                                <input class="form-control bg-transparent" id="dob" type="date" name="dob" placeholder="Select DOB" value="{{ $user->dob ?? ''}}" required="" />
                                            </div>
                                            <div class="custom-control mb-4">
                                                @error('profession')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                                <label>Profession</label>
                                                @enderror
                                               
                                                <input class="form-control bg-transparent" name="profession" id="profession" value="{{ $user->profession ?? '' }}" placeholder="" required="" />
                                            </div>
                                            <div class="custom-control mb-4">
                                                @error('location')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>Location</label>
                                                @enderror
                                                
                                                <input class="form-control bg-transparent" name="location" id="location" value="{{ $user->location ?? '' }}"  placeholder="" required="" />
                                            </div>
                                            <div class="custom-control">
                                                <button type="submit" class="btn theme-btn btn-block">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-6 text-center d-none d-lg-block">
                                        <img src="{{asset('front2/images/my-profile.png')}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show <?= ($activeTab == "accountinfo" ? "active" : ""); ?>" id="account-info" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="row align-items-center mb-5">
                                    <div class="col-md-5">
                                        <form action="">
                                            <div class="custom-control mb-4">
                                                <label>Mobile Number</label>
                                                <input class="form-control bg-transparent" name="contactno" id="contactno" value="{{ $user->contactno ?? '' }}" required readonly />
                                            </div>
                                            <div class="custom-control mb-4">
                                                <label>Email Address</label>
                                                <input class="form-control bg-transparent" type="email" name="email" readonly id="email" value="{{ $user->email ?? ''}}" required />
                                            </div>
                                            <div class="custom-control mb-4">
                                                <label>Password</label>
                                                <input class="form-control bg-transparent" value="Loren@5722" />
                                            </div>
                                            <div class="custom-control">
                                                <button type="button" class="btn theme-btn btn-block">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-6 text-center d-none d-lg-block">
                                        <img src="{{asset('front2/images/my-profile.png')}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show <?= ($activeTab == "changepassword" ? "active" : ""); ?>" id="reset-password" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                         @if ($message = Session::get('success'))
                                            <div class="alert alert-success alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                    <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                        <form method="POST" action="{{ route('user.change.password') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="custom-control mb-4">

                                                 @error('current_password')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>Old Password</label>
                                                @enderror
                                                
                                                <input class="form-control bg-transparent" placeholder="Enter old password..." type="password" name="current_password" id="password" required="" />
                                            </div>
                                            <div class="custom-control mb-4">
                                                @error('new_password')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>New Password</label>
                                                @enderror
                                                <input class="form-control bg-transparent" placeholder="Enter new password..." type="password" name="new_password" id="new_password" required="" />
                                            </div>
                                            <div class="custom-control mb-4">
                                                @error('new_confirm_password')
                                                <label style="color: red;">{{ $message }}</label>
                                                @else
                                               <label>Confirm New Password</label>
                                                @enderror
                                                <input class="form-control bg-transparent" placeholder="Re-enter new password..." type="password" name="new_confirm_password" id="new_confirm_password" required="" />
                                            </div>
                                            <div class="custom-control">
                                                <button   type="submit" class="btn theme-btn btn-block">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-6 text-center d-none d-lg-block">
                                        <img src="{{asset('front2/images/my-profile.png')}}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/moment.js')}}"></script>
    <script src="{{asset('front2/js/main.js')}}"></script>
    <script src="{{asset('front2/js/plugin/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $("#header").load("header-main.html");
            $("#footer").load("footer.html");
            $('#datetimepicker1').datetimepicker({
                locale: 'ru'
            });
        });
        $(".addmultiplecontacts").click(function () {
            $("#multiple-contacts").slideDown();
            $("#single-contacts").slideUp();
        });
        $(".addsinglecontacts").click(function () {
            $("#single-contacts").slideDown();
            $("#multiple-contacts").slideUp();
        });
        $('#pills-profile-tab').click(function () {
            $(".contact-list .action-btn").addClass('active');
        });
        $('#pills-home-tab').click(function () {
            $(".contact-list .action-btn").removeClass('active');
        });
    </script>
@endsection