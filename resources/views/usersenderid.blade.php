@extends('front_layouts.app')

@section('css')
<link href="{{asset('front2/css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet" />
@endsection

@section('content')

<div class="bg-white">
            <div class="border-bottom p-lg-3 p-2">
                <div class="container">
                    <div class="row no-gutter align-items-center">
                        <div class="col-md-6 col-6">
                            <h2 class="heading3 pb-0">Sender ID List</h2>
                        </div>
                        <div class="col-md-6 col-6 d-flex justify-content-end">
                            <button class="btn theme-btn" data-toggle="modal" data-target="#createSenderid">Create Sender ID</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 offset-lg-3">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>


                @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
                <table class="table table-striped" id="senderid">
                    <thead>
                        <tr>
                            <th scope="col" class="border-0">Sr. No.</th>
                            <th scope="col" class="border-0">Sender ID</th>
                            <th scope="col" class="border-0">Status</th>
                            <th scope="col" class="border-0">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($count=1)
                        @foreach ($sendridData as $senderid)
                        <tr>
                            <th scope="row">{{$count}}</th>
                            <td>{{$senderid->senderid}}</td>
                            @if($senderid->status == "approve")
                            <td class="text-success">Approved</td>
                            @elseif($senderid->status == "apply")
                            <td class="text-danger">Pending</td>
                            @else
                            <td class="text-danger" style="text-transform: capitalize;">{{$senderid->status}}</td>
                            @endif
                            <td class="text-danger">
                                <a class="mr-3 edit-modal" id="edit-modal"  data-senderid="{{$senderid->senderid}}" data-sender_uuid="{{$senderid->sender_uuid}}"><img src="{{asset('front2/images/edit.svg')}}"></a>
                                <!-- <a href="{{route('create.usersender.remove',$senderid->sender_uuid)}}"><img src="{{asset('front2/images/delete.svg')}}"></a> -->
                             <a href="javascript:;" data-toggle="modal" onclick="deleteData('{{$senderid->sender_uuid}}')" data-target="#DeleteModal"><img src="{{asset('front2/images/delete.svg')}}"></a>
                            </td>
                        </tr>
                         @php($count++)
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

@endsection

@section('model')
<div class="modal fade" id="createSenderid" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex align-items-center">
                    <h5 class="modal-title font-weight-500">Create Sender ID</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{asset('front2/images/close.png')}}"/></span>
                    </button>
                </div>
                <div class="modal-body pt-3 pb-3 pl-4 pr-4">
                    <form name="senderform" id="senderform" role="form" method="POST" action="{{ route('create.user.sender.request', isset($senderData) ? $senderData->sender_uuid : '') }}">
                        @csrf
                    <p class="text-center mb-4">
                        you can create sender id to track
                        sms records
                    </p>
                    <input type="hidden" class="sender_uuid" id="sender_uuid" name="sender_uuid" value="" >
                    <div class="custom-control mb-4">
                        <label>Enter Sender ID</label>
                        <input class="form-control bg-transparent senderid" id="senderid" name="senderid" placeholder="Enter sender id here..." value="{{ $senderData->senderid ?? ''}}"  />
                    </div>
                    <div class="text-center">
                        <button type="submit" id="sbutton" class="btn theme-btn pl-5 pr-5">{{ isset($senderData) ? 'Update' : 'Submit'}}</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>




  <div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex align-items-center">
                    <h5 class="modal-title font-weight-500">Delete Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{asset('front2/images/close.png')}}"/></span>
                    </button>
                </div>
                <div class="modal-body pt-3 pb-3 pl-4 pr-4">
                <form action="" id="deleteForm" method="get">
                     <div class="modal-content">

                         <div class="modal-body ">
                             {{ csrf_field() }}

                             <p class="text-center">Are You Sure Want To Delete ?</p>
                         </div>
                         <div class="modal-footer" style="-webkit-box-pack: center !important;display: block;">
                             <center>
                                 <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                                 <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button>
                             </center>
                         </div>
                     </div>
                 </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/moment.js')}}"></script>
    <script src="{{asset('front2/js/main.js')}}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
           $('#senderid').DataTable();
        });

       $(document).on('click', '#edit-modal', function() {
            $('.modal-title').text('Edit Sender ID');
            $('.senderid').val($(this).data('senderid'));
            $('.sender_uuid').val($(this).data('sender_uuid'));
            $(".sbutton").attr('value', 'Update');

            $('#createSenderid').modal('show');
        });


        $(document).ready(function() {
        $('#singlesms-table').DataTable({


            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('reports/allusersms') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [

                { "data": "id" },
                { "data": "date" },
                { "data": "sender_id" },
                { "data": "contactno" },
                { "data": "message" },


            ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
    });
    </script>
    <script type="text/javascript">

        jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

        $('#senderform').validate({



        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            //console.log('1');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
                //console.log('1');
            }
        },

        rules: {

                senderid: {

                    required: true,
                    lettersonly: true,
                    maxlength: 6
                },

        }

    });
    </script>
    <script type="text/javascript">
        function deleteData(id)
     {
         var id = id;
         var url = '{{ route("create.usersender.remove", ":id") }}';
         url = url.replace(':id', id);
         //$('.sender_id _delete').val($(this).data('sender_uuid'));
         $("#deleteForm").attr('action', url);
     }

     function formSubmit()
     {
         $("#deleteForm").submit();
     }

     $('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});
    </script>


@endsection