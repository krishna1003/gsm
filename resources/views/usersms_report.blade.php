@extends('front_layouts.app')

@section('css')

<link href="{{asset('front2/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('front2/css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet" />
@endsection

@section('content')

<div class="bg-white sms-panel">
            <ul class="nav nav-pills mb-3 border-bottom p-4" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active rounded-0" id="sms-tab1" data-toggle="pill" href="#single-SMS" role="tab" aria-controls="pills-home" aria-selected="true">Single SMS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded-0" id="sms-tab2" data-toggle="pill" href="#bulk-SMS" role="tab" aria-controls="pills-profile" aria-selected="false">Bulk SMS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded-0" id="sms-tab3" data-toggle="pill" href="#group-SMS" role="tab" aria-controls="pills-contact" aria-selected="false">Group SMS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded-0" id="sms-tab4" data-toggle="pill" href="#schedule-SMS" role="tab" aria-controls="pills-contact" aria-selected="false">Schedule SMS</a>
                </li>
            </ul>
            <div class="tab-content container-fluid p-4" id="pills-tabContent">
                <div class="tab-pane fade show active shadow border p-3" id="single-SMS" role="tabpanel" aria-labelledby="sms-tab1">
                    <table class="sms-table" id="singlesms-table">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Date</th>
                                <th>Sender ID</th>
                                <th>Contact No</th>
                                <th>Message</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                    </table>
                </div>
                <div class="tab-pane fade shadow border p-3" id="bulk-SMS" role="tabpanel" aria-labelledby="sms-tab2">
                    <table class="sms-table" id="bulksms-table">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Date</th>
                                <th>Sender ID</th>
                                <th>Contact No</th>
                                <th>Massege</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade shadow border p-3" id="group-SMS" role="tabpanel" aria-labelledby="sms-tab3">
                    <table class="sms-table" id="groupsms-table">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Date</th>
                                <th>Sender ID</th>
                                <th>Contact No</th>
                                <th>Massege</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade shadow border p-3" id="schedule-SMS" role="tabpanel" aria-labelledby="sms-tab4">
                    <table class="sms-table" id="schedulesms-table">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Date</th>
                                <th>Sender ID</th>
                                <th>Contact No</th>
                                <th>Massege</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>12-10-2020</td>
                                <td>CHK005</td>
                                <td>+91 79908 72540</td>
                                <td>Hi, How are you? I am fine.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    <script src="{{asset('front2/js/plugin/mouse.parallax.js')}}"></script>
    <script src="{{asset('front2/js/main.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
           $('#bulksms-table').DataTable();
        });

        jQuery(document).ready(function () {
           $('#groupsms-table').DataTable();
        });

        jQuery(document).ready(function () {
           $('#schedulesms-table').DataTable();
        });

        $(document).ready(function() {
        $('#singlesms-table').DataTable({


            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('reports/allusersms') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [

                { "data": "id" },
                { "data": "date" },
                { "data": "sender_id" },
                { "data": "contactno" },
                { "data": "message" },
                { "data": "status" },


            ],
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
    });
    </script>


@endsection