@extends('front_layouts.app')

@section('css')
<link href="{{asset('front2/css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet" />
@endsection

@section('content')

<div class="container">
            <div class="row align-items-center" >
                
                <div class="col-lg-3 pt-3 pb-3 pt-lg-5 pb-lg-5"></div>
                <div class="col-lg-6 pt-3 pb-3 pt-lg-5 pb-lg-5" >
                    <div class="login-sec" id="premium">
                        <h1 class="title-text text-center mb-5">Recharge Wallet</h1>
                        <form  id="walletform"  name ="walletform" class="walletform">
                            
                            <div class="custom-control mb-5"> 
                                
                                
                                <div class="custom-control custom-select mb-4">
                                    @error('smspackageid')
                                    <label style="color: red;">{{ $message }}</label>
                                    @else
                                    <label>SMS Type</label>
                                    @enderror
                                    <select class="form-control" id="smspackageid" name="smspackageid">
                                    <option value="">Choose SMS Package</option>
                                    @foreach ($smspackages as $smspackage)
                                    <option value="{{$smspackage->id}}" > {{ $smspackage->name }}</option>
                                    @endforeach
                                </select>
                                </div>


                            </div>
                            <div class="custom-control mb-5">
                                <label>Msg</label>
                                @error('no_of_msg')
                                <label style="color: red;">{{ $message }}</label>
                                @else
                                <label>No. of Msg</label>
                                @enderror
                                <input type="text" class="form-control" placeholder="" value="" id="no_of_msg" name="no_of_msg" />
                                
                            </div>
                            <div class="custom-control mb-5">
                                
                                @error('amount')
                                <label style="color: red;">{{ $message }}</label>
                                @else
                                <label>Amount</label>
                                @enderror
                                <input type="text" class="form-control" placeholder="" value="" id="amount" name="amount" />
                                <input type="hidden" class="form-control" placeholder="" value="" id="amount2" name="amount2" />
                                
                            </div>
                            <input type="hidden" name="signup_token" id="signup_token" value="new">
                            <input type="hidden" name="email" id="email" value="{{ Auth::user()->email }}">
                            <input type="hidden" name="contact" id="contact" value="{{ Auth::user()->contactno }}">
                            
                            
                                        
                            <div class="custom-control">
                                <button type="submit" class="btn theme-btn btn-block" id="pay_now">Pay</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
                <div class="col-lg-3 pt-3 pb-3 pt-lg-5 pb-lg-5"></div>
            </div>
        </div>
@endsection

@section('js')
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="{{asset('front2/js/vendor/bootstrap.js')}}"></script>
    
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    

    
   

    <script type="text/javascript">
       $('#smspackageid').change(function() {
            var cID = $(this).val();

            $("#pay_now").removeClass('disabled');
                    $("#pay_now").prop('disabled', false); 
                
            if (cID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-smspackage-list')}}?c_id=" + cID,
                    success: function(res) {
                        console.log(res);
                        console.log(res.length);
                        if (res) {
                            $("#no_of_msg").empty();
                            $("#amount").empty();
                            $("#amount2").empty();
                            if(res.length == "0") {
                            $("#no_of_msg").val("");
                            $("#amount").val("");
                            $("#amount2").val("");
                            
                        }else{
                            $.each(res, function(key, value) {
                                $("#no_of_msg").val(key);
                                $("#amount").val(value);
                                $("#amount2").val(value);
                            });
                        }
                            
                            
                        } else{
                            $("#no_of_msg").val("");
                            $("#amount").val("");
                            $("#amount2").val("");
                        }
                    }

                });
            } else {
                $("#no_of_msg").val("");
                            $("#amount").val("");
                
            }
        });
    </script>
    <script type="text/javascript">

     $(document).ready(function() {   

         $('#walletform')[0].reset()
        
        $('.walletform').validate({

            errorPlacement: function(error, element) {

                if (element.attr("name") == "tnc_accepts") {
                    error.appendTo("#errorToShow");
                } else {
                    // something else if it's not a checkbox

                    element.css('background', '#ffdddd');
                    error.insertAfter(element);
                }
            },
        rules: {
            
                smspackageid: {

                    required: true,
                    
                },
                no_of_msg: {
                    required: true
                },
                amount: {
                    required: true
                },

            },
                messages: {
            

        },
        submitHandler: function() {
               //alert('1');
                
                    
                    $("#pay_now").addClass('disabled');
                    $("#pay_now").prop('disabled', true); 
                    var totalAmount = $('#amount2').val();
                    var formdata = $('#walletform').serialize();
                    var options = {
                        "key": "rzp_test_jmSRPUSopYJQPQ",
                        "amount": (totalAmount * 100), // 2000 paise = INR 20
                        "name": "GSM Gateway",
                        "description": "GSM Gateway SMS Package",
                        "image": "{{asset('front2/images/favicon.png')}}",
                        "handler": function(response) {

                            console.log(response);

                            $.ajax({
                                url: "{{url('wallet')}}",
                                type: 'get',
                                dataType: 'json',
                                data: formdata + "&payment_id=" + response.razorpay_payment_id + "&netamount=" + totalAmount + "&amount=" + totalAmount + "&fees=" + totalAmount,
                                success: function(msg) {
                                    console.log(msg);
                                    $('#walletform')[0].reset();
                                    $('html, body').animate({
                                        scrollTop: 0
                                    }, 0);

                                    if (msg.status === true) {

                                        var htmlmsg = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg.msg + '</div>';

                                        $(htmlmsg).insertBefore('#premium');

                                        @if(Auth::check())
                                        setTimeout(function() {
                                            location.reload();
                                        }, 2000);
                                        @endif

                                        
                                    } else {

                                        var htmlmsg = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg.msg + '</div>';

                                        $(htmlmsg).insertBefore('#premium');
                                        //alert(msg.message);
                                    }
                                    /*   console.log(msg);
                       window.location.href = SITEURL + '/thank-you/'+msg.tempcode;
                       console.log( window.location.href);*/
                                }
                            });

                        },
                        "prefill": {
                            
                            
                            "contact": $('#contact').val(),
                            "email": $('#email').val(),
                        },
                        "theme": {
                            "color": "#528FF0"
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();
            }
            
        

    });
    });
    </script>
    


@endsection