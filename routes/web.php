<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Route::get('/error-500', function () {
	return view('error-500');
})->name('error500');

Route::get('/error-404', function () {
	return view('error-404');
})->name('error404');

Route::get('clean', function () {
	\Artisan::call('config:cache');
	\Artisan::call('route:clear');
	\Artisan::call('optimize:clear');
	return "Done!";
});

Route::get('/client-register', 'UserController@createClientRegisterPage')->name('ClientRegister');
Route::post('client/request', 'UserController@saveClientRegister')->name('client.request.register');
Route::post('/existusername', 'UserController@existusername');
Route::post('/existcontactno', 'UserController@existcontactno');
Route::get('userlogout', 'Auth\LoginController@loggedOut')->name('user.logout');

Route::get('get-state-list1', 'UserController@getStateList');
Route::get('get-city-list1', 'UserController@getCityList');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

	Route::get('/userpanel', 'UserController@userpanelindex')->name('userpanel');
	Route::get('/addressbook', 'UserController@addressindex')->name('addressindex');
	Route::get('/multisms', 'UserController@multismsindex')->name('multismsindex');
	Route::get('/groupsms', 'UserController@groupsmsindex')->name('groupsmsindex');
	Route::get('/singlesms', 'UserController@singlesmsindex')->name('multismsindex');
	Route::get('/bulksms', 'UserController@bulksmsindex')->name('bulksmsindex');
	Route::get('/apikey', 'UserController@apikeyindex')->name('apikeyindex');
	Route::get('/transaction-history', 'MembersController@transaction_history')->name('transaction-history');

	Route::get('/wallet', 'MembersController@storepremiumRegister')->name('wallet.submit');
	Route::get('/Rechargewallet', 'MembersController@index')->name('addmoney');

	Route::post('/payment-success', 'MembersController@payment_success');

	Route::get('/premiumRegister/{token?}', 'PremiumsignupController@index');
	Route::post('/premiumRegister/create', 'PremiumsignupController@store')->name('create.premiumRegister');
	Route::post('/premiumRegister/create_premiumRegister', 'PremiumsignupController@storepremiumRegister');

	Route::post('create/usergroup/', 'ContactController@createusergroup')->name('contact.usergroup.create');

	/**
	 * Roles, Permission and Users Permission Modules
	 */
	Route::resource('roles', 'RoleController');
	Route::resource('users', 'UserController');
	Route::resource('products', 'ProductController');
	Route::get('myprofile/view', 'UserController@amyprofileView')->name('amyprofile.view');
	Route::get('editprofile', 'UserController@aeditProfileForm')->name('aprofile.update');
	Route::post('editprofile/request/{id}', 'UserController@aupdateProfile')->name('aprofile.update.request');

	Route::get('user/change-password', 'ChangePasswordController@index')->name('changepassword');
	Route::post('user/change-password', 'ChangePasswordController@store')->name('change.password');

	Route::get('userpanel/change-password', 'ChangePasswordController@userindex')->name('user.changepassword1');
	Route::post('userpanel/change-password/request', 'ChangePasswordController@userstore')->name('user.change.password');

	Route::get('edituserprofile', 'UserController@usereditProfileForm')->name('userprofile.update');
	Route::post('edituserprofile/request/{id}', 'UserController@userupdatedetails')->name('userupdatedetails.update.request');

	/**
	 * SMS Contacts
	 */
	Route::prefix('contacts')->group(function () {

		Route::get('show', 'ContactController@showContact')->name('contact.show');
		Route::get('create/new/{uuid?}', 'ContactController@createContact')->name('contact.create');
		Route::post('create-request/{uuid?}', 'ContactController@storeData')->name('contact.create.request');
		Route::get('remove/{uuid?}', 'ContactController@deleteContact')->name('contact.remove');

		/**
		 * Group Contact
		 */
		Route::get('group/show', 'ContactController@showGroupContact')->name('contact.group.show');
		Route::get('create/group/{uuid?}', 'ContactController@createGroupContact')->name('contact.group.create');
		Route::get('remove/group/{uuid?}', 'ContactController@deleteGroupContact')->name('contact.group.remove');

		Route::post('create-group-request/{uuid?}', 'ContactController@groupStoreData')->name('contact.group.create.request');

	});

	Route::prefix('user/contacts')->group(function () {

		Route::post('usercontactcreate-request/{uuid?}', 'ContactController@usercontactstoreData')->name('user.contacts.create.request');
		Route::post('usermultiplecontacts-request/{uuid?}', 'ContactController@usermultiplecontactstoreData')->name('user.multiplecontacts.create.request');
		Route::get('/usercontactcreate-request/edit/{id}', 'ContactController@usercontact_edit')->name('user.contacts.edit');
		Route::post('/usercontact/request/update/{id}', 'ContactController@usercontact_update')->name('user.contacts.update');

		Route::post('/usercontact/delete/{id}', 'ContactController@usercontact_delete')->name('user.contacts.delete');
		Route::get('/usergroup/delete/{id}', 'ContactController@usergroup_delete')->name('user.groups.delete');

	});

	Route::get('usercontact/search', 'ContactController@searchcontact')->name('searchcontact');
	Route::get('smsgroupid/select', 'ContactController@smsgroupid')->name('smsgroupid.select');
	Route::get('usercontactgroup/search', 'ContactController@searchcontactgroup')->name('searchcontactgroup');
	Route::get('usercontact/editgroupcontact', 'ContactController@EditGroupContact')->name('editgroupcontact');

	Route::get('phonebook', 'PhoneBookController@index')->name('phonebook');
	Route::get('create-phonebook', 'PhoneBookController@create')->name('phonebook.create');
	Route::post('/store-phonebook', 'PhoneBookController@store')->name('phonebook.store');
	Route::get('/phonebook/edit/{id}', 'PhoneBookController@edit')->name('phonebook.edit');
	Route::get('/phonebook/delete/{id}', 'PhoneBookController@delete1')->name('phonebook.delete');
	Route::post('/phonebook/request/update/{id}', 'PhoneBookController@update')->name('phonebook.update');

	Route::get('allphonebook', 'PhoneBookController@allPhoneBook')->name('allphonebook');
	Route::get('change/phonebook-status/{id}', 'PhoneBookController@changephonebookStatus_datatable')->name('change.phonebook_datatable.status');

	Route::get('sms_package', 'SmsPackageController@index')->name('sms_package');
	Route::get('create-sms_package', 'SmsPackageController@create')->name('sms_package.create');
	Route::post('/store-sms_package', 'SmsPackageController@store')->name('sms_package.store');
	Route::get('/sms_package/edit/{id}', 'SmsPackageController@edit')->name('sms_package.edit');
	Route::get('/sms_package/delete/{id}', 'SmsPackageController@delete1')->name('sms_package.delete');
	Route::post('/sms_package/request/update/{id}', 'SmsPackageController@update')->name('sms_package.update');

	Route::get('allsmspackage', 'SmsPackageController@allSmsPackage')->name('allsmspackage');
	Route::get('change/smspackage-status/{id}', 'SmsPackageController@changeSmsPackageStatus_datatable')->name('change.sms_package_datatable.status');

	Route::get('user_sms_package', 'UserSmsPackageController@index')->name('user_sms_package');
	Route::get('create-user_sms_package', 'UserSmsPackageController@create')->name('user_sms_package.create');
	Route::post('/store-user_sms_package', 'UserSmsPackageController@store')->name('user_sms_package.store');
	Route::get('/user_sms_package/edit/{id}', 'UserSmsPackageController@edit')->name('user_sms_package.edit');
	Route::get('/user_sms_package/delete/{id}', 'UserSmsPackageController@delete1')->name('user_sms_package.delete');
	Route::post('/user_sms_package/request/update/{id}', 'UserSmsPackageController@update')->name('sms_package.update');

	Route::get('allusersmspackage', 'UserSmsPackageController@alluserSmsPackage')->name('allusersmspackage');
	Route::get('change/usersmspackage-status/{id}', 'UserSmsPackageController@changeUserSmsPackageStatus_datatable')->name('change.user_sms_package_datatable.status');
	Route::get('get-smspackage-list', 'UserSmsPackageController@getSmsPackageList');

	/**
	 * External API
	 */
	Route::prefix('third-party-api')->namespace('ExternalAPI')->group(function () {
		Route::get('all/apis', 'ThirdPartyAPIController@showAllAPIS')->name('list.external.apis');
		Route::get('add/new/apis', 'ThirdPartyAPIController@addNewAPIS')->name('add.new.apis');
		Route::post('externalapi/create/apis/request', 'ThirdPartyAPIController@importNewAPIS')->name('externalapi.create.request');
		Route::get('externalapi/remove/{uuid}', 'ThirdPartyAPIController@removeAPIS')->name('externalapi.remove');
		Route::get('externalapi/status/{uuid}', 'ThirdPartyAPIController@changeExternalAPIStatus')->name('externalapi.chnage.status');

		// SEND SMS
		Route::get('sendsms/single/sms', 'SendSMSController@sendSingleSMSForm')->name('sendsms.single.view');
		Route::post('sendsms/single/sms/request', 'SendSMSController@sendSingleSMSRequest')->name('send.singlesms.request');
		Route::post('user/sendsms/single/sms/request', 'SendSMSController@sendUserSingleSMSRequest')->name('send.usersinglesms.request');

		Route::get('sendsms/multi/sms', 'SendSMSController@sendMultiSMS')->name('sendsms.multi.view');
		Route::post('sendsms/multi/sms/request', 'SendSMSController@SendMultiSMSRequest')->name('send.multisms.request');
		Route::post('user/sendsms/multi/sms/request', 'SendSMSController@SendUserMultiSMSRequest')->name('send.usermultisms.request');
		Route::post('user/sendsms/group/sms/request', 'SendSMSController@SendUserGroupSMSRequest')->name('send.usergroupsms.request');

		Route::get('sendsms/bulk/sms', 'SendSMSController@sendBulkSMS')->name('sendsms.bulk.view');
		Route::post('bulk/sms/render/request', 'SendSMSController@renderBulkSMSRequest')->name('load.bulkview.request');

		Route::post('sendsms/bulk/sms/request', 'SendSMSController@SendBulkSMSRequest')->name('send.bulksms.request');
	});

	Route::any('send/testing/sms', 'ExternalAPI\SingleSMSController@testSMS');

	Route::prefix('reports')->namespace('Reports')->group(function () {
		Route::get('sms/deliveries', 'MasterReportController@showSMSHistory')->name('delivery.master.history');
		Route::get('sms/delivery/description/{uuid?}', 'MasterReportController@smsWholeDescription')->name('delivery.smsdescription');

		Route::get('usersms/deliveries', 'UserSmsReportController@showSMSHistory')->name('user.sms.report');
		Route::get('allusersms', 'UserSmsReportController@allUserSms')->name('allusersms');
	});

	/**
	 * Manage Sender IDS
	 */
	Route::prefix('sender/ids/')->group(function () {
		Route::get('show-ids', 'ManageSenderIDs@showSendersId')->name('show.all.senderids');
		Route::get('user/sender/ids/', 'UserManageSenderIDs@showSendersId')->name('user.show.all.senderid');
		Route::get('apply/new-senderids/{uuid?}', 'ManageSenderIDs@applyForSenderID')->name('apply.new.senderids');
		Route::post('apply/senderid/request/{uuid?}', 'ManageSenderIDs@storeSenderIDS')->name('create.sender.request');
		Route::post('user/apply/senderid/request/{uuid?}', 'UserManageSenderIDs@UserstoreSenderIDS')->name('create.user.sender.request');
		//
		Route::get('remove/senderid/{uuid}', 'ManageSenderIDs@removeSenderID')->name('create.sender.remove');
		Route::get('remove/usersenderid/{uuid}', 'UserManageSenderIDs@removeSenderID')->name('create.usersender.remove');
		Route::get('update-status/senderid/{uuid}/{status}', 'ManageSenderIDs@changeSenderIDStatus')->name('change.sender.status');

	});

});
